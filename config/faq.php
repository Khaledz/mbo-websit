<?php

return [
	'Are businesswomen allowed to become MBO members?' => "Women are as welcome as men to become MBO members after fulfilling the membership requirements. Subject to culture's law.",
	'Can non-Muslims join MBO?' => "StNon-Muslims are welcome to join MBO if they fulfill the membership requirements.",
	'Are there any religious or political agendas for MBO?' => "MBO consciously avoids getting involved in any political activity. MBO sole agenda is its declared purpose of developing the Muslim business community.",
	'Does MBO receive any support from any country?' => "MBO does not receive any kind of support from any country.",
	'Is MBO engaged in any charitable activity?' => 'MBO encourages members to participate in community service. However MBO will not be directly involved in any charitable activities.',
	'How is MBO financially supported?' => 'MBO is supported through membership fees and sponsorships.',
	'How can members benefit by joining MBO?' => 'MBO focuses on developing the capabilities of its members. Three particular areas are targeted: knowledge, skills and values. The development of these three areas will enhance our members to become better businessmen. In addition, one of our major objectives is to offer global networking opportunities for our members which would help them better enhance their businesses.',
	'Are there any religious or political agendas for MBO?' => 'MBO consciously avoids getting involved in any political activity. MBO sole agenda is its declared purpose of developing the Muslim business community.',
	'If the purpose of MBO is to develop Muslim business community, why would a non-Muslim join?' => 'MBO believes that higher values is the bridge between businessmen and cultures. Businessmen of all faith are realizing good ethical practices. Good ethics insured sustained business growth make good business sense.',
	'How can I start an MBO chapter in my city?' => "Contact the MBO International Board Chairman's office. His contacts are available in the Organization Link.",
	'What is the relationship between the International Board and the Chapters?' => 'MBO is a chapter-driven Organization. The International Board empowers each chapter to act according to the interest of its own members and community within the parameters set in the Bylaws of the Organization.',
	'Is membership in the MBO open to business owners only?' => 'Membership in the MBO is open to all senior professionals from the business community based on membership criteria.',
];