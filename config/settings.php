<?php

return [
	'language' => config('app.locale'),
	'email_notification' => true,
	'sms_notification' => true,
	'avatar' => '',
	'birthday' => '',
	'sector' => '',
	'job' => '',
	'children' => '',
	'wife' => '',
	'company' => '',
	'linkedin' => '',
	'website' => '',
	'public_profile' => 'on',
	'personal_assistant_email' => '',
	'personal_assistant_name' => '',
	'personal_assistant_phone' => '',
];