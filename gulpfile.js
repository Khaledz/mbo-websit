var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
   
	// Set the paths of folders.
    var npmDir   = './node_modules/';
    var AppAssetDir = './resources/assets/app/';
    var AdminAssetDir = './resources/assets/admin/';

    mix

    /*
        styles
    */
    // .styles(
    //     [
    //         'stack-interface.css',
    //         'socicon.css',
    //         'lightbox.min.css',
    //         'iconsmind.css',
    //         'theme.css',
    //         'xmain-color.css',
    //         'zcustom.css'
    //     ], 
    //     'public/app/css/app.css', 
    //     AppAssetDir + '/css')
   
    
    // /* styles: Vendor CSS, mainly from bower components */
    // .styles(
    //     [
    //         'bootstrap/dist/css/bootstrap.css',
    //         'lightbox2/dist/css/lightbox.min.css',
    //         'flickity/dist/flickity.min.css',
    //         'normalize.css/normalize.css',
    //         'font-awesome/css/font-awesome.min.css',
    //     ],
    //     'public/app/css/vendor.css',
    //     npmDir
    // )
 
    // /* JS: App JavaScript, mainly custom written */
    // .scripts(
    //     [
    //         'parallax.js',
    //         'typed.min.js',
    //         'scripts.js',
    //         'functions.js',
    //         'custom.js'
    //     ], 
    //     'public/app/js/app.js', 
    //     AppAssetDir + '/js')

    // /* JS: Vendor JavaScript, mainly from bower components */
    // .scripts(
    //     [
    //         'jquery/dist/jquery.js',
    //         'flickity/js/flickity.js',
    //         'pickadate/lib/picker.js',
    //         'pickadate/lib/picker.date.js',
    //         'bootstrap/dist/js/bootstrap.js',
    //         'jquery-steps/build/jquery.steps.min.js',
    //         'dropzone/dist/dropzone.js',
    //         'glyphicons/glyphicons.js',
    //         'isotope-layout/dist/isotope.pkgd.min.js',
    //         'jquery.countdown/jquery.countdown.js',
    //         'spectragram/spectragram.js',
    //         'smooth-scroll/dist/js/smooth-scroll.min.js',
    //         'granim/dist/granim.min.js',
    //         'lightbox2/dist/js/lightbox.min.js'
    //     ],
    //     'public/app/js/vendor.js',
    //     npmDir
    // )

    /*
       Admin styles
    */
    .styles(['**/*.css'], 'public/admin/css/app.css', AdminAssetDir + '/css')

    /* styles: Vendor CSS, mainly from bower components */
    .styles(
        [
            'bootstrap/dist/css/bootstrap.css',
            'font-awesome/css/font-awesome.min.css',
            'bootstrap-select/dist/css/bootstrap-select.min.css',
        ],
        'public/admin/css/vendor.css',
        npmDir
    )

    /* JS: App JavaScript, mainly custom written */
    .scripts(['**/*.js'], 'public/admin/js/app.js', AdminAssetDir + '/js')

    /* JS: Vendor JavaScript, mainly from bower components */
    .scripts(
        [
            'jquery/dist/jquery.js',
            'bootstrap/dist/js/bootstrap.js',
            'bootstrap-select/dist/js/bootstrap-select.min.js',
        ],
        'public/admin/js/vendor.js',
        npmDir
    )

   /* Copy: Fonts */
    // .copy(
    //     npmDir + 'font-awesome/fonts',
    //     'public/build/app/fonts'
    // )
    // .copy(
    //     npmDir + 'bootstrap/fonts',
    //     'public/build/app/fonts'
    // )
    // .copy(
    //     npmDir + 'simple-line-icons/fonts',
    //     'public/build/app/fonts'
    // )
    // .copy(
    //     npmDir + 'etlinefont-bower/fonts',
    //     'public/css/fonts'
    // )
    // .copy(
    //     'resources/assets/app/img',
    //     'public/img'
    // )
    // .copy(
    //     'resources/assets/app/fonts',
    //      'public/build/app/fonts'
    // )
    // .copy(
    //     npmDir + 'flag-icon-css/flags',
    //      'public/build/app/flags'
    // )
    

    // Admin
    .copy(
        'resources/assets/admin/img',
        'public/img'
    )
    .copy(
        'resources/assets/admin/fonts',
        'public/build/admin/fonts'
    )
    .copy(
        'resources/assets/admin/fonts',
        'public/build/admin/fonts'
    )
    .copy(
        npmDir + 'font-awesome/fonts',
        'public/build/admin/fonts'
    )

    /* Perform file versioning of compiled files */
    .version([
    	// 'public/app/css/vendor.css',
        // 'public/app/css/app.css',
        // 'public/app/js/vendor.js',
        // 'public/app/js/app.js',
        'public/admin/css/vendor.css',
        'public/admin/css/app.css',
        'public/admin/js/vendor.js',
        'public/admin/js/app.js',
    ])
});