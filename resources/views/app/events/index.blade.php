@extends('layouts.app')
@section('content')

<div class="container mbo-container">
    <div class="row dark-container paddings">
        <div class="col-md-12">
            <h3>Upcoming Events</h3>
            <hr>
        </div>
        <div class="row paddings">
            <div class="col-md-12">
                @if($upcomings->count() == 0)
                    <div class="boxed boxed--border text-center bg--secondary" style="border: 1px solid rgb(0, 0, 0)">
                        <span style="color:#6779b2; font-size: 18px;">There is no upcoming events at this moment.</span>
                    </div>
                @else
                    <table class="table table-hover" style="background-color: white;">
                        <thead>
                            <tr>
                                <th>Event Name</th>
                                <th>Event Date</th>
                                <th>Event Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($upcomings as $event)
                            <tr>
                                <td><a href="{{ route('app.event.show', [$event]) }}">{{ $event->name }}</a></td>
                                <td>{{ $event->date }}</td>
                                <td>{{ $event->time }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-mbo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-custom.css') }}">
<div class="row dark-container paddings">
        <div class="col-md-12">
            <h3>Event Categories</h3>
            <hr>
        </div>
        <div class="row paddings">
            <div class="col-md-12 well">
                    <div class="tabs-container tabs--folder">
                        <ul class="tabs">
                        @if($categories->count() > 0)
                        @foreach($categories as $category)
                            @if($loop->first)
                            <li class="active">
                                <div class="tab__title">
                                    <span class="h5">{{ $category->name }}</span>
                                </div>
                                <div class="tab__content">
                                    <div class="row">
                                        @if($category->events->count() == 0)
                                            <h4 class="text-center">There is no events in {{ $category->name }}</h4>
                                        @else
                                        @foreach($category->events as $event)
                                        <div class="col-sm-4">
                                            <div class="feature feature-1">
                                                <div class="feature__body boxed boxed--border bg--secondary" style="border: 1px solid rgb(0, 0, 0)">
                                                    <h5>{{ $event->name }}</h5>
                                                    <a href="{{ route('app.event.show', $event) }}">
                                                        Learn More
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        @endif
                                    </div>
                                </div>
                            </li>
                            @else
                            <li>
                                <div class="tab__title">
                                    <span class="h5">{{ $category->name }}</span>
                                </div>
                                <div class="tab__content">
                                    <div class="row">
                                        @if($category->events->count() == 0)
                                            <h4 class="text-center">There is no events in {{ $category->name }}</h4>
                                        @else
                                        @foreach($category->events as $event)
                                        <div class="col-sm-4">
                                            <div class="feature feature-1" style="margin-bottom: 0px;">
                                                <div class="feature__body boxed boxed--border bg--secondary" style="border: 1px solid rgb(0, 0, 0)">
                                                    <h5>{{ $event->name }}</h5>
                                                    <a href="{{ route ('app.event.show', $event) }}">
                                                        Learn More
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        @endif
                                    </div>
                                </div>
                            </li>
                            @endif
                        @endforeach
                        @endif
                        </ul>
                    </div>
                    <!--end of tabs container-->
                </div>
                <!--end of tabs container-->
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
<br>
@endsection