@extends('layouts.app')
@section('content')

<div class="container mbo-container">
    <div class="row dark-container paddings">
    <h3><i class="fa fa-arrow-left" aria-hidden="true"></i> <a href="{{ route('app.event.show', $event) }}">{{ $event->name }} Invitees</a></h3>
    <hr>
        @foreach($event->users as $user)
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-top: 15px;">
            <div class="content">
                <a href="{{ route('app.user.show', $user) }}">
                    <div class="content-overlay center-block" style="height: 215px;"></div>
                    <img class="content-image center-block" src="{{ $user->avatar() }}" style="height:215px;">
                    <div class="content-details fadeIn-left" style="background-color: #313131; ">
                            <h3 style="font-size:13px;">{{ $user->fullName }}</h3>
                    </div>
                </a>
            </div>
           <div class="col-md-12">
           <b>
           @if($user->pivot->answered == 0)
            <span class="text-center center-block" style="color: blue;">No Answer Yet</span>
            @elseif($user->pivot->status == 0 && $user->pivot->answered == 1)
            <span class="text-center center-block" style="color: red;">Will NOT Attend</span>
            @elseif($user->pivot->status == 1 && $user->pivot->answered == 1)
                <span class="text-center center-block" style="color: green;">Will Attend</span>
            @endif
            </b>
           </div>
        </div>
        
        @endforeach
    </div>
</div>
<!--end of container-->
@endsection
