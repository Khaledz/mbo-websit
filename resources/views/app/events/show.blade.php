@extends('layouts.app')
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-mbo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-custom.css') }}">
    <div class="container mbo-container">
        <div class="row">
            <div class="col-md-12">
                <!-- Attend or not -->
                @if(isset($user) && $user->answered == 0)
                <div class="boxed boxed--border bg--secondary">
                <form class="text-left" method="POST" action="{{ route('app.event.attend', [$event]) }}">
                    <h3 class="text-center">We will be pleased to see you in this event, will you attend?</h3>
                    <div class="col-sm-6 text-center">
                        <div class="input-radio input-radio--innerlabel">
                            {!! Form::radio('status', 1) !!}
                            <label for="radio-1" style="font-size: 18px; font-weight: bold;">Yes</label>
                        </div>
                    </div>
                    <div class="col-sm-6 text-center">
                        <div class="input-radio input-radio--innerlabel">
                            {!! Form::radio('status', 0) !!}
                            <label for="radio-1" style="font-size: 18px; font-weight: bold;">No</label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <span>Note:</span>
                        {!! Form::textarea('note', null, ['rows' => 3, 'placeholder' => 'You want to say something?']) !!}
                    </div>
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn--primary type--uppercase">SEND</button>
                    </div>
                    {{ csrf_field() }}
                </form>
                </div>
                @endif
                <!-- Attend or not -->
                @if(isset($user) && $user->answered == 1)
                <h4 class="text-left color--primary" style="font-size: 17px;">You're {{ ($user->status == 0) ? 'not attending' : 'attending' }} this event. <a href="{{ route('app.event.change', [$event]) }}">change</a></h4>
                @endif
            </div>
            <div class="col-md-9 col-xs-12 col-sm-12">
            
                <div class="conversation__head boxed boxed--sm bg--secondary">
                    <h4 style="font-weight: bold;">{{ $event->name }}</h4>
                    <span>Speaker: {!! html_entity_decode($event->speaker) !!}</span>
                    <div class="conversation__avatar">
                        <h5>On {{ $event->date }} at {{ $event->time }}</h5>
                    </div>
                    <hr>
                    <p class="lead">{!! html_entity_decode($event->description) !!}</p>
                    @if(!is_null($event->note))
                    <span style="color: red;">Note: {{ $event->note }}</span>
                    @endif
                    
                    @if(! is_null($event->galleries()->first()))
                    @if($event->galleries()->first()->attachments->count() > 0)
                    <br>
                    <h4>Attachments</h4>
                    <ul class="menu-horizontal">
                    <div class="row">
                    @foreach($event->galleries()->first()->attachments as $attachment)
                        @if(! in_array($attachment->extention, ['jpg', 'jpeg', 'gif', 'png']))
                        <div class="masonry__item col-md-4 col-xs-6">
                            <div class="feature boxed boxed--border border--round">
                                <a href="{{ asset('storage/galleries/' .$event->galleries()->first()->id. '/'. $attachment->name) }}">
                                    <img alt="Image" src="{{ asset('app/img/'. $attachment->extention .'.png') }}" class="center-block" width="150" height="100">
                                </a>
                            </div>
                        </div>
                        @else
                        <div class="masonry__item col-md-4 col-xs-6">
                            <div class="feature boxed boxed--border border--round">
                                <a href="{{ asset('storage/galleries/' .$event->galleries()->first()->id. '/'. $attachment->name) }}" data-lightbox="Gallery 1">
                                    <img alt="Image" src="{{ asset('storage/galleries/' .$event->galleries()->first()->id. '/'. $attachment->name) }}" width="200" height="100">
                                </a>
                            </div>
                        </div>
                        @endif
                    @endforeach
                    </div>
                    </ul>
                    @endif
                    @endif
                </div>
                <br>
                <div class="comments conversation__comments">
                    <div class="boxed bg--secondary">
                        <h3 class="text-center">Program</h3>
                        {!! html_entity_decode($event->program) !!}
                    </div>
                </div>
                <div class="comments conversation__comments">
                    <div class="boxed bg--secondary">
                        <h3 class="text-center">Location</h3>
                        @if(is_null($event->location))
                        <p class="text-center">Location is not specified</p>
                        @else
                        {!! $event->location !!}
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="comments conversation__comments">
                    <div class="boxed bg--secondary">
                        <h3 class="text-center">Invitees</h3>
                        @if($event->users->count() == 0)
                        <h4 class="text-center">There is no members.</h4>
                        @else
                            <ul class="sidebar__widget">
                                @foreach($event->users()->take(10)->get() as $user)
                                    <li style="padding-bottom: 9px;">
                                    <div class="row" style="padding-right: 0; padding-left: 0;">
                                        <div class="col-md-3" style="padding-right: 0; padding-left: 0;">
                                        <img src="{{ $user->avatar() }}" width="40" height="40" class="img-circle center-block" />
                                        </div>
                                        <div class="col-md-9" style="padding-right: 0; padding-left: 0;">
                                        <a href="{{ route('app.user.show', [$user])}}">{{ $user->fullName }}</a>
                                        </div>
                                    </div>
                                    </li>
                               @endforeach
                            </ul>
                        @endif
                        <h5 class="text-center"><a href="{{ route('app.event.all_members',[$event]) }}">View More</a></h5>
                    </div>
                </div>
                <div class="comments conversation__comments">
                    <div class="boxed bg--secondary">
                        <h3 class="text-center">Attendees</h3>
                        @if($event->users->count() == 0)
                        <h4 class="text-center">There is no members.</h4>
                        @else
                            <ul class="sidebar__widget">
                                @foreach($event->users()->take(10)->get() as $user)
                                @if($user->pivot->status == 1 && $user->pivot->answered == 1)
                                    <li style="padding-bottom: 9px;">
                                    <div class="row" style="padding-right: 0; padding-left: 0;">
                                        <div class="col-md-3" style="padding-right: 0; padding-left: 0;">
                                        <img src="{{ $user->avatar() }}" width="40" height="40" class="img-circle center-block" />
                                        </div>
                                        <div class="col-md-9" style="padding-right: 0; padding-left: 0;">
                                        <a href="{{ route('app.user.show', [$user])}}">{{ $user->fullName }}</a>
                                        </div>
                                    </div>
                                    </li>
                                @endif
                               @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end of container-->

@endsection
