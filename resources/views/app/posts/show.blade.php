@extends('layouts.app')
@section('content')
    <div class="container mbo-container">
        <div class="row">
            <div class="col-md-9">
            @include('partials.app.breadcrumb')
            </div>
            @if($club->isChampion(auth()->user()))
            <div class="col-md-2 pull-right">
                    <form action="{{ route('app.club.post.destroy', [$club, $post]) }}" method="post">
                        <button type="submit" class="btn btn--primary btn--xs">Remove this post</button>
                        {{ method_field('DELETE') }}
                        {!! csrf_field() !!}
                    </form>
            </div>
            @endif
        </div>

        <div class="row paddings" style="background-color: #eae8e7; border:1px solid rgba(94, 118, 182, 0.19); border-radius: 6px; font-size: 19px;">
            <div class="col-xs-8"><span  style="font-size: 25px; font-weight: bold">{{ $post->title }}</span><br>
            <span  style="font-size: 15px; color: #777">{{ $post->created_at->diffForHumans() }}</span></div>
            <div class="col-xs-4"><img alt="avatar" src="{{ $post->user->avatar() }}" class="img-circle" width="70" height="70"> {{ $post->user->fullName }}</div>
            <div class="col-xs-12"><hr></div>
            <div class="col-xs-12">{{ $post->content }}</div>
                @if($post->attachments->count() > 0)
                <div class="col-xs-12">
                    <br>
                    <h4>Attachments</h4>
                    <div class="row">
                    @foreach($post->attachments as $attachment)
                        @if(! in_array($attachment->extention, ['jpg', 'jpeg', 'gif', 'png']))
                        <div class="masonry__item col-md-4 col-xs-6">
                            <div class="feature boxed boxed--border border--round">
                            <a href="{{ asset('storage/clubs/'.$club->id. '/posts/'. $post->id .'/'. $attachment->name) }}">
                                    <img alt="Image" src="{{ asset('app/img/'. $attachment->extention .'.png') }}" class="center-block" width="150" height="100">
                                </a>
                            </div>
                        </div>
                        @else
                        <div class="col-lg-3">
                        <div class="content">
                            <a href="{{ asset('storage/clubs/'.$club->id. '/posts/'. $post->id .'/'. $attachment->name) }}" data-lightbox="gallery-1" data-title="<span class='caption'>{{ $attachment->orginal_name }}</span>" id="2">
                                <div class="content-overlay"></div>
                                <img class="content-image center-block" src="{{ asset('storage/clubs/'.$club->id. '/posts/'. $post->id .'/'. $attachment->name) }}">
                            </a>
                        </div>
                    </div>
                        @endif
                    @endforeach
                    </div>
                    </div>
                @endif
        </div>
       

        <br>
        <br>
        <link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-mbo.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-custom.css') }}">
        <h3>Comments ({{ $post->comments->count() }})</h3>
        <div class="comments conversation__comments">
                @if($post->comments->count() == 0)
                    <div class="boxed boxed--border bg--secondary">
                        <h4 class="text-center">There is no comments. Post the first comment!</h4>
                    </div>
                @else
                    <ul class="comments__list">
                    @foreach($post->comments()->paginate(10) as $comment)
                        <li style="border-radius: 6px;">
                            <div class="comment bg--secondary">
                                <div class="comment__avatar">
                                    <img alt="Image" class="img-circle" width="60" height="60" src="{{ $comment->user->avatar() }}">
                                </div>
                                <div class="comment__body">
                                    <h5 class="type--fine-print">{{ $comment->user->fullName }}</h5>
                                    <div class="comment__meta">
                                        <span>{{ $comment->created_at->diffForHumans() }}</span>
                                    </div>
                                    <p class="lead">
                                        {{ $comment->content }}
                                    </p>
                                @if($comment->attachments->count() > 0)
                                    <h5>Attachments</h5>
                                    <div class="row">
                                    @foreach($comment->attachments as $attachment)
                                        @if(! in_array($attachment->extention, ['jpg', 'jpeg', 'gif', 'png']))
                                        <div class="masonry__item col-md-4 col-xs-6">
                                            <div class="feature boxed boxed--border border--round">
                                            <a href="{{ asset('storage/clubs/'.$club->id. '/posts/'. $post->id .'/'. $comment->id .'/'. $attachment->name) }}">
                                                    <img alt="Image" src="{{ asset('app/img/'. $attachment->extention .'.png') }}" class="center-block" width="150" height="100">
                                                </a>
                                            </div>
                                        </div>
                                        @else
                                       
                                        <div class="masonry__item col-md-4 col-xs-6">
                                            <div class="feature boxed boxed--border border--round">
                                                <a href="{{ asset('storage/clubs/'.$club->id. '/posts/'. $post->id .'/'. $comment->id .'/'. $attachment->name) }}" data-lightbox="Gallery 1">
                                                    <img alt="Image" src="{{ asset('storage/clubs/'.$club->id. '/posts/'. $post->id .'/'. $comment->id .'/'. $attachment->name) }}" width="100%" height="120">
                                                </a>
                                            </div>
                                        </div>
                                        @endif
                                    @endforeach
                                    </div>
                                    @endif
                                 @if($club->isChampion(auth()->user()))
                                        <form action="{{ route('app.post.comment.destroy', [$post, $comment]) }}" method="post">
                                            <button type="submit" class="btn btn--primary btn--xs">Remove this comment</button>
                                            {{ method_field('DELETE') }}
                                            {!! csrf_field() !!}
                                        </form>
                                    @endif
                                </div>
                                    
                            </div>
                            <!--end comment-->
                        </li>
                        <br>
                    @endforeach
                    </ul>
                @endif
                <!--end comments-->
                <br>
                <div class="boxed boxed--border">
                    <form method="POST" action="{{ route('app.post.comment.store', [$post]) }}" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-xs-12">
                                <span>Reply*:</span>
                                {!! Form::textarea('content', null, ['rows' => 4, 'class' => 'validate-required', 'placeholder' => 'Type reply message here']) !!}
                            </div>
                            <div class="col-xs-12">
                                <span>Attachments:</span>
                                {!! Form::file('files[]', ['multiple']) !!}
                                <span>Press and hold ctrl to select multiple files.</span>
                            </div>
                            <div class="col-sm-3">
                                <button type="submit" class="btn btn--primary type--uppercase">Reply</button>
                            </div>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->


@endsection