@extends('layouts.app')
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-mbo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-custom.css') }}">
    <div class="container mbo-container">
        <div class="row paddings">
        <h3 class="text-center">Start New Discussion</h3>
            <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                <div class="row">
                    <div class="boxed boxed--border bg--secondary">
                    @include('errors.form')
                        <form class="text-left" method="POST" action="{{ route('app.club.post.store', [$club]) }}" enctype="multipart/form-data">
                            <div class="col-sm-12">
                                <span>Title*:</span>
                                {!! Form::text('title', null, ['class' => 'validate-required', 'placeholder' => 'Title']) !!}
                            </div>
                            <div class="col-sm-12">
                                <span>Content*:</span>
                                {!! Form::textarea('content', null, ['rows' => 5, 'class' => 'validate-required', 'placeholder' => 'Type reply message here']) !!}
                            </div>
                            <div class="col-sm-12">
                                <span>Attachments:</span>
                                {!! Form::file('files[]', ['multiple']) !!}
                                <span>Press and hold ctrl to select multiple files.</span>
                            </div>
                            <div class="col-sm-12 boxed">
                                <button type="submit" class="btn btn--primary type--uppercase">post discussion</button>
                            </div>
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
                <!--end of row-->
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->

@endsection