@extends('layouts.app')
@section('content')

<section class="space--sm">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="masonry">
                    <h2 class="head">MBO Gallery</h2>
                    <div class="masonry-filter-container">
                        <span>Filter by Year:</span>
                        <div class="masonry-filter-holder">
                            <div class="masonry__filters" data-filter-all-text="All Years">
                            <ul>
                                <li class="active" data-masonry-filter="*">All Years</li>
                            </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        @if($galleries->count() == 0)
                            <h3 class="text-center">There is no galleries at this moment.</h3>
                        @else
                        <div class="masonry__container masonry--active">
                        @foreach($galleries as $gallery)
                        <!-- Gallery with event -->
                        @if(isset($gallery->event))
                            <div class="masonry__item col-md-4 col-sm-6 filter-{{ \Carbon\Carbon::parse($gallery->event()->first()->date)->year }}" data-masonry-filter="{{ \Carbon\Carbon::parse($gallery->event()->first()->date)->year }}">
                                <article class="feature feature-1">
                                    <a href="{{ route('app.gallery.show', [$gallery->name])}}" class="block">
                                        <img alt="Image" src="{{ $gallery->thumb() }}" width="380" height="250">
                                    </a>
                                    <div class="feature__body boxed boxed--border">
                                        <span>{{ \Carbon\Carbon::parse($gallery->created_at)->toDateString() }}</span>
                                        - <span class="text-right">{{ $gallery->event()->first()->category->name }}</span>
                                        <h5>{{ $gallery->name }}</h5>
                                        <a href="{{ route('app.gallery.show', [$gallery->name])}}">
                                            View Album
                                        </a>
                                    </div>
                                </article>
                            </div>
                            <!--end item-->
                        @else
                        <!-- No event -->
                            <div class="masonry__item col-md-4 col-sm-6 filter-{{ \Carbon\Carbon::parse($gallery->date)->year }}" data-masonry-filter="{{ \Carbon\Carbon::parse($gallery->date)->year }}">
                                <article class="feature feature-1">
                                    <a href="{{ route('app.gallery.show', [$gallery->name])}}" class="block">
                                        <img alt="Image" src="{{ $gallery->thumb() }}" width="380" height="250">
                                    </a>
                                    <div class="feature__body boxed boxed--border">
                                        <span>{{ \Carbon\Carbon::parse($gallery->date)->toDateString() }}</span>
                                       
                                        <h5>{{ $gallery->name }}</h5>
                                        <a href="{{ route('app.gallery.show', [$gallery->name])}}">
                                            View Album
                                        </a>
                                    </div>
                                </article>
                            </div>
                            <!--end item-->
                        @endif
                        @endforeach
                        </div>
                        @endif
                        
                        <!--end of masonry container-->
                    </div>
                    <!--end row-->
                    <center>{{ $galleries->links('vendor.pagination.default') }}</center>
                </div>
                <!--end masonry-->
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
@endsection