@extends('layouts.app')
@section('content')
<div class="container mbo-container">
    
    <div class="row dark-container paddings">
    
        <div class="col-md-12">
            <h2 style="padding: 0; margin: 5px;">{{ $gallery->name }}</h2>
            <hr style="margin: 0;">
        </div>
        @foreach($gallery->attachments as $attachment)
            @if(! in_array($attachment->extention, ['jpg', 'jpeg', 'gif', 'png']))
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-top: 15px;">
                <div class="content">
                    <a href="{{ asset('storage/galleries/' .$gallery->id. '/'. $attachment->name) }}">
                        <div class="content-overlay center-block"></div>
                        <img alt="Image" src="{{ asset('app/img/'. $attachment->extention .'.png') }}" class="content-image center-block">
                        <div class="content-details fadeIn-left" style="background-color: #313131;">
                            <h3 style="font-size: 15px;">{{ substr($attachment->orginal_name, 0, 10) }}</h3>
                        </div>
                    </a>
                </div>
            </div>
            @else
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-top: 15px;">
                <div class="content">
                    <a href="{{ asset('storage/galleries/' .$gallery->id. '/'. $attachment->name) }}" data-lightbox="gallery-1" data-title="<span class='caption'>{{ $attachment->orginal_name }}</span>" id="2">
                        <div class="content-overlay center-block"></div>
                        <img class="content-image center-block" src="{{ asset('storage/galleries/' .$gallery->id. '/'. $attachment->name) }}">
                        <div class="content-details fadeIn-left" style="background-color: #313131;">
                            <h3 style="font-size: 15px;">{{ substr($attachment->orginal_name, 0, 10) }}</h3>
                        </div>
                    </a>
                </div>
            </div>
            @endif
        @endforeach
    </div>
</div>
               
@endsection
