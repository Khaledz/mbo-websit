@extends('layouts.app')
@section('content')

<div class="container mbo-container">
    <div class="row dark-container paddings">
        <div class="col-md-12">
            <h2 style="padding: 0; margin: 5px;">{{ $category->name }} - {{ $year }}</h2>
            <hr style="margin: 0;">
        </div>
        @foreach($filters as $gallery)
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-top: 10px; margin-bottom: 10px;">
                <div class="content">
                    <a href="{{ route('app.gallery.show', $gallery) }}">
                        <div class="content-overlay center-block"></div>
                        <img class="content-image center-block" src="{{ $gallery->thumb() }}">
                        <div class="content-details fadeIn-left" style="background-color: #313131;">
                            <h3 style="font-size: 15px;">{{ $gallery->name }}</h3>
                        </div>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
</div>

@endsection