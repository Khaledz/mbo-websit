@extends('layouts.app')
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-mbo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-custom.css') }}">
@if(!isset($user))
<section class="space--sm">
    <div class="container mbo-container">
        <div class="row">
            <div class="col-md-12 boxed boxed--border bg--secondary">
                <h3 class="text-center">This account is private.</h3>
            </div>
        </div>
    </div>
</section>
@else
<section class="space--sm">
    <div class="container mbo-container">
        <div class="row">
            <div class="col-md-12">
                <div class="bg--secondary boxed boxed--lg boxed--border">
                    <div class="text-block text-center">
                        <img alt="avatar" src="{{ $user->avatar() }}" class="img-circle" width="120" height="120">
                        <span class="h4">{{ $user->fullName }}</span>
                        <span class="h5">{{ $user->settings()->job }}</span>
                    </div>
                    <div class="text-block clearfix text-center">
                        <ul class="row row--list">
                            <li class="col-sm-4">
                                <span class="type--fine-print block">Birthday:</span>
                                <span class="h4">{{ ($user->settings()->birthday == "") ? '-' : $user->settings()->birthday}}</span>
                            </li>
                            <li class="col-sm-4">
                                <span class="type--fine-print block">Member Since:</span>
                                <span class="h4">{{ ($user->joined) ?: $user->created_at->toDateString() }}</span>
                            </li>
                            <li class="col-sm-4">
                                <span class="type--fine-print block">Email:</span>
                                <span class="h4">{{ $user->email }}</span>
                            </li>
                            <li class="col-sm-4">
                                <span class="type--fine-print block">Job:</span>
                                <span class="h4">{{ ($user->settings()->job == "") ? '-' : $user->settings()->job }}</span>
                            </li>
                            <li class="col-sm-4">
                                <span class="type--fine-print block">Children:</span>
                                <span class="h4">{{ ($user->settings()->children == "") ? '-' : $user->settings()->children }}</span>
                            </li>
                            <li class="col-sm-4">
                                <span class="type--fine-print block">Wife:</span>
                                <span class="h4">{{ ($user->settings()->wife == "") ? '-' : $user->settings()->wife }}</span>
                            </li>
                            <li class="col-sm-4">
                                <span class="type--fine-print block">Company:</span>
                                <span class="h4">{{ ($user->settings()->company == "") ? '-' : $user->settings()->company }}</span>
                            </li>
                            <li class="col-sm-4">
                                <span class="type--fine-print block">Website:</span>
                                <span class="h4">
                                    @if($user->settings()->website == '')
                                        -
                                    @else
                                        <a href="{{ $user->settings()->website }}" target="_blank">view</a>
                                    @endif
                                </span>
                            </li>
                            <li class="col-sm-4">
                                <span class="type--fine-print block">Linkedin:</span>
                                <span class="h4">
                                @if($user->settings()->linkedin == '')
                                    -
                                @else
                                    <a href="{{ url($user->settings()->linkedin) }}" target="_blank">view</a>
                                @endif
                                </span>
                            </li>
                            
                    </ul></div>
                    
                </div>
                <div class="bg--secondary boxed boxed--lg boxed--border">
                    <div class="text-block text-center">
                        <span class="h4">Personal Assistant</span>
                    </div>
                    <div class="text-block clearfix text-center">
                        <ul class="row row--list">
                            <li class="col-sm-4">
                                <span class="type--fine-print block">Name:</span>
                                <span class="h4">
                                {{ ($user->settings()->personal_assistant_name == "") ? '-' : $user->settings()->personal_assistant_name }}
                                </span>
                            </li>
                            <li class="col-sm-4">
                                <span class="type--fine-print block">Phone:</span>
                                <span class="h4">
                                {{ ($user->settings()->personal_assistant_phone == "") ? '-' : $user->settings()->personal_assistant_phone }}
                                </span>
                            </li>
                            <li class="col-sm-4">
                                <span class="type--fine-print block">Email:</span>
                                <span class="h4">
                                {{ ($user->settings()->personal_assistant_email == "") ? '-' : $user->settings()->personal_assistant_name }}
                                </span>
                            </li>
                            
                    </ul></div>
                    
                </div>
                
                <div class="bg--secondary boxed boxed--border">
                    <ul class="row row--list clearfix text-center">
                        <li class="col-sm-4 col-xs-6">
                            <span class="h6 type--uppercase type--fade">Posts</span>
                            <span class="h3">{{ $user->posts->count() }}</span>
                        </li>
                        <li class="col-sm-4 col-xs-6">
                            <span class="h6 type--uppercase type--fade">Comments</span>
                            <span class="h3">{{ $user->comments->count() }}</span>
                        </li>
                        <li class="col-sm-4 col-xs-6">
                            <span class="h6 type--uppercase type--fade">Files</span>
                            <span class="h3">{{ $user->attachments->count() }}</span>
                        </li>
                    </ul>
                </div>
                
                <div class="bg--secondary boxed boxed--border">
                    <h4>Events related to you</h4>
                    <ul>
                    @if($user->events->count() == 0)
                        There is no events related to you.
                    @else
                    @foreach($user->events()->latest()->take(5)->get() as $event)
                        <li class="clearfix">
                            <div class="row">
                                <div class="col-md-2 col-xs-3 text-right">
                                    <div class="icon-circle">
                                        <i class="fa fa-comment"></i>
                                    </div>
                                </div>
                                <div class="col-md-10 col-xs-7">
                                    <span class="type--fine-print">{{ $event->created_at->diffForHumans() }}</span>
                                    <a href="{{ route('app.event.show', [$event]) }}" class="block color--primary"><h6 class="block color--primary">{{ $event->name }}</h6></a>
                                </div>
                            </div>
                        </li>
                    @endforeach
                    @endif
                    </ul>
                    <a href="{{ route('app.event.index') }}" class="type--fine-print pull-right">View All</a>
                </div>

                <div class="bg--secondary boxed boxed--border">
                    <h4>Multaqa Related to you</h4>
                    <ul>
                    @if($user->clubs->count() == 0)
                        There is no multaqa related to you.
                    @else
                    @foreach($user->clubs()->latest()->take(5)->get() as $club)
                        <li class="clearfix">
                            <div class="row">
                                <div class="col-md-2 col-xs-3 text-right">
                                    <div class="icon-circle">
                                        <i class="fa fa-comment"></i>
                                    </div>
                                </div>
                                <div class="col-md-10 col-xs-7 text-left">
                                    <span class="type--fine-print">{{ $club->created_at->diffForHumans() }}</span>
                                    <a href="{{ route('app.club.show', [$club]) }}" class="block color--primary"><h6 class="block color--primary">{{ $club->name }}</h6></a>
                                </div>
                            </div>
                        </li>
                    @endforeach
                    @endif
                    </ul>
                    <a href="{{ route('app.club.index') }}" class="type--fine-print pull-right">View All</a>
                </div>

                <div class="bg--secondary boxed boxed--border">
                    <h4>Market places related to you</h4>
                    <ul>
                    @if($user->markets->count() == 0)
                        There is no market places related to you.
                    @else
                    @foreach($user->markets()->latest()->take(5)->get() as $market)
                        <li class="clearfix">
                            <div class="row">
                                <div class="col-md-2 col-xs-3 text-center">
                                    <div class="icon-circle">
                                        <i class="fa fa-comment"></i>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-7">
                                    <span class="type--fine-print">{{ $market->created_at->diffForHumans() }}</span>
                                    <a href="{{ route('app.market.show', [$market]) }}"><h6 class="block color--primary">{{ $market->name }}</h6></a>
                                </div>
                            </div>
                        </li>
                    @endforeach
                    @endif
                    </ul>
                    <a href="{{ route('app.market.index') }}" class="type--fine-print pull-right">View All</a>
                </div>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
@endif
@endsection