@extends('layouts.app')

@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-mbo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-custom.css') }}">
    <div class="container mbo-container">
        <div class="row">
            <div class="col-md-4">
                <div class="boxed boxed--lg boxed--border bg--secondary">
                    <div class="text-block text-center">
                        <img alt="avatar" src="{{ $user->avatar() }}" class="img-circle" width="100" height="100">
                        <span><br><a href="#" id="avatar">Edit avatar</a></span>
                        <span class="h5">{{ $user->fullName }}</span>
                        <span>{{ $user->settings()->job }}</span>
                        <form id="form" method="POST" action="{{ route('app.user.avatar', [$user]) }}" enctype="multipart/form-data">
                        <input id="file" type="file" name="avatar" style="display:none"/>
                        {!! csrf_field() !!}
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="boxed boxed--lg boxed--border bg--secondary">
                    <div id="account-profile" class="account-tab">
                        <h4>Profile</h4>
                        @include('errors.form')
                        {!! Form::model($user, ['route' => ['app.setting.update', $user], 'method' =>'put']) !!}
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>First Name:</label>
                                    {!! Form::text('fname') !!}
                                </div>
                                <div class="col-sm-6">
                                    <label>Last Name:</label>
                                    {!! Form::text('lname') !!}
                                </div>
                                <div class="col-sm-12">
                                    <label>Password:</label>
                                    {!! Form::password('password') !!}
                                    <span>Leave it blank if you don't want to change it.</span>
                                </div>
                                <div class="col-sm-12">
                                    <label>Email Address:</label>
                                    {!! Form::text('email') !!}
                                </div>
                                <div class="col-sm-12">
                                    <label>Birthday:</label>
                                    {!! Form::text('birthday', $user->settings()->birthday, ['class' => 'datepicker']) !!}
                                </div>
                                <div class="col-sm-12">
                                    <label>Sector:</label>
                                    {!! Form::text('sector', $user->settings()->sector) !!}
                                </div>
                                <div class="col-sm-12">
                                    <label>Job:</label>
                                    {!! Form::text('job', $user->settings()->job) !!}
                                </div>
                                <div class="col-sm-6">
                                    <label>Number of children:</label>
                                    {!! Form::text('children', $user->settings()->children) !!}
                                </div>
                                <div class="col-sm-6">
                                    <label>Number of wifes:</label>
                                    {!! Form::text('wife', $user->settings()->wife) !!}
                                </div>
                                <div class="col-sm-12">
                                    <label>Linkedin:</label>
                                    {!! Form::text('linkedin', $user->settings()->linkedin) !!}
                                </div>
                                <div class="col-sm-6">
                                    <label>Company:</label>
                                    {!! Form::text('company', $user->settings()->company) !!}
                                </div>
                                <div class="col-sm-6">
                                    <label>Website:</label>
                                    {!! Form::text('website', $user->settings()->website) !!}
                                </div>
                                <div class="col-sm-12">
                                    <label>Personal assistant name:</label>
                                    {!! Form::text('personal_assistant_name', $user->settings()->personal_assistant_name) !!}
                                </div>
                                <div class="col-sm-6">
                                    <label>Personal assistant email:</label>
                                    {!! Form::text('personal_assistant_email', $user->settings()->personal_assistant_email) !!}
                                </div>
                                <div class="col-sm-6">
                                    <label>Personal assistant phone:</label>
                                    {!! Form::text('personal_assistant_phone', $user->settings()->personal_assistant_phone) !!}
                                </div>
                                <div class="col-sm-12">
                                    <div class="input-checkbox input-checkbox--switch">
                                        {!! Form::checkbox('public_profile', $user->settings()->public_profile, $user->settings()->public_profile) !!}
                                        <label for="checkbox-switch"></label>
                                    </div>
                                    <span style="font-size: 18px;">Public Profile</span>
                                </div>
                                
                                <div class="col-md-3 col-sm-4">
                                    <button type="submit" class="btn btn--primary type--uppercase">Save Profile</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
@endsection

@section('script')
<script>
$(function(){
    $('#avatar').click(function(){
        $('#file').trigger('click');
    });
});

document.getElementById("file").onchange = function() {
    document.getElementById("form").submit();
};

</script>

@endsection