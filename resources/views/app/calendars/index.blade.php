@extends('layouts.app')
@section('style')
<link href="{{ asset('css/fullcalendar.min.css')}}" rel="stylesheet" type="text/css" media="all" />
@endsection

@section('content')
    <section clss="container">
        <div id='calendar' style="margin-right: 40px; margin-left: 40px;"></div>
    </section>

@endsection

@section('script')
<script src="{{ asset('js/moment.js') }}"></script>
<script src="{{ asset('js/fullcalendar.min.js') }}"></script>

<script>
$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $('#calendar').fullCalendar({
        events: '{{ route("app.calendar.show") }}',

        editable: true,
        height: 650,
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        eventRender: function(event, element, view) {
            if (event.allDay === 'true') {
            event.allDay = true;
            } else {
            event.allDay = false;
            }
        },
        selectable: true,
        selectHelper: true,
    });
});
</script>
@endsection