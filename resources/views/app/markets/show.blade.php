@extends('layouts.app')
@section('content')

    <div class="container mbo-container">
        <div class="row">
            <div class="col-md-12">
            @include('partials.app.breadcrumb')
                <div class="row paddings" style="background-color: #eae8e7; border:1px solid rgba(94, 118, 182, 0.19); border-radius: 6px; font-size: 19px;">
            <div class="col-xs-8"><span  style="font-size: 25px; font-weight: bold">{{ $market->name }}</span><br>
            <span  style="font-size: 15px; color: #777">{{ $market->created_at->diffForHumans() }}</span></div>
            <div class="col-xs-4"><img alt="avatar" src="{{ $market->user->avatar() }}" class="img-circle" width="70" height="70"> {{ $market->user->fullName }}</div>
            <div class="col-xs-12"><hr></div>
            <div class="col-xs-12">{{ $market->description }}</div>
                @if($market->attachments->count() > 0)
                <div class="col-xs-12">
                    <br>
                    <h6>Attachments</h6>
                    <div class="row">
                    @foreach($market->attachments as $attachment)
                        @if($attachment->name == $market->logo)
                            @continue
                        @elseif(! in_array($attachment->extention, ['jpg', 'jpeg', 'gif', 'png']))
                        <div class="masonry__item col-md-4 col-xs-6">
                            <div class="feature boxed boxed--border border--round">
                            <a href="{{ asset('storage/market_places/'.$market->id .'/'. $attachment->name) }}">
                                    <img alt="Image" src="{{ asset('app/img/'. $attachment->extention .'.png') }}" class="center-block" width="150" height="100">
                                </a>
                            </div>
                        </div>
                        @else
                        <div class="col-lg-3">
                        <div class="content">
                            <a href="{{ asset('storage/market_places/'.$market->id .'/'. $attachment->name) }}" data-lightbox="gallery-1" data-title="<span class='caption'>{{ $attachment->orginal_name }}</span>" id="2">
                                <div class="content-overlay center-block"></div>
                                <img class="content-image center-block" src="{{ asset('storage/market_places/'.$market->id .'/'. $attachment->name) }}">
                            </a>
                        </div>
                    </div>
                        @endif
                    @endforeach
                    </div>
                @endif
                </div>
        </div>
                
                <link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack.css') }}">
                <link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-mbo.css') }}">
                <link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-custom.css') }}">
                <br>
                <br>
                <h3>Comments ({{ $market->comments->count() }})</h3>
                <div class="comments conversation__comments">
                @if($market->comments->count() == 0)
                    <div class="boxed boxed--border  bg--secondary">
                        <h4 class="text-center">There is no comments. Post the first comment!</h4>
                    </div>
                @else
                    <ul class="comments__list">
                    @foreach($market->comments()->paginate(10) as $comment)
                        <li>
                            <div class="comment">
                                <div class="comment__avatar">
                                    <img alt="Image" src="{{ $comment->user->avatar() }}" class="img-circle">
                                </div>
                                <div class="comment__body">
                                    <h5 class="type--fine-print">{{ $comment->user->fullName }}</h5>
                                    <div class="comment__meta">
                                        <span>{{ $comment->created_at->diffForHumans() }}</span>
                                    </div>
                                    <p class="lead">
                                        {{ $comment->content }}
                                    </p>
                                    @if($comment->attachments->count() > 0)
                                    <h5>Attachments</h5>
                                    <div class="row">
                                    @foreach($comment->attachments as $attachment)
                                        @if(! in_array($attachment->extention, ['jpg', 'jpeg', 'gif', 'png']))
                                        <div class="masonry__item col-md-4 col-xs-6">
                                            <div class="feature boxed boxed--border border--round">
                                            <a href="{{ asset('storage/market_places/'.$market->id. '/comments/'. $comment->id .'/'. $attachment->name) }}">
                                                    <img alt="Image" src="{{ asset('app/img/'. $attachment->extention .'.png') }}" class="center-block" width="150" height="100">
                                                </a>
                                            </div>
                                        </div>
                                        @else
                                       <div class="col-md-4 col-xs-6">
                                            <div class="boxed boxed--border border--round">
                                                <a href="{{ asset('storage/market_places/'.$market->id. '/comments/'. $comment->id .'/'. $attachment->name) }}" data-lightbox="Gallery 1">
                                                    <img alt="Image" src="{{ asset('storage/market_places/'.$market->id. '/comments/'. $comment->id .'/'. $attachment->name) }}" width="100%" height="120">
                                                </a>
                                            </div>
                                        </div>
                                        @endif
                                    @endforeach
                                    </div>
                                  
                                    @endif
                                </div>
                            </div>
                            <!--end comment-->
                        </li>
                        <br>
                    @endforeach
                    </ul>
                @endif
                <center>{{ $market->comments()->paginate(10)->links('vendor.pagination.default') }}</center>
                </div>
                <!--end comments-->
                <br>
                <div class="boxed boxed--border bg--secondary">
                    <form method="POST" action="{{ route('app.market.comment.store', [$market]) }}" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-xs-12">
                                <span>Reply*:</span>
                                {!! Form::textarea('content', null, ['rows' => 4, 'class' => 'validate-required', 'placeholder' => 'Type reply message here']) !!}
                            </div>
                            <div class="col-xs-12">
                                <span>Attachments:</span>
                                {!! Form::file('files[]', ['multiple']) !!}
                                <span>Press and hold ctrl to select multiple files.</span>
                            </div>
                            <div class="col-sm-3">
                                <button type="submit" class="btn btn--primary type--uppercase">Reply</button>
                            </div>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->

@endsection