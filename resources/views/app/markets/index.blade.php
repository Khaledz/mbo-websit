@extends('layouts.app')
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-mbo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-custom.css') }}">
<div class="container mbo-container">
    <div class="row">
        <div class="col-md-12">
            <div class="boxed">
                <h2 class="head">Market Places</h2>
                    <div class="row">
                        <div class="col-md-6">
                        @include('partials.app.breadcrumb')
                        </div>
                        <div class="col-md-3 pull-right">
                            <a href="{{ route('app.market.create') }}"><button type="submit" class="btn btn--primary type--uppercase">New Market place</button></a>
                        </div>
                    </div>
                    <br>
                    
                    @if($markets->count() == 0)
                    <div class="feature feature-2 boxed boxed--border bg--secondary">
                        <h4 class="text-center">There is no market places at this moment.</h4>
                    </div>
                    @else
                    <div class="row">
                    @foreach($markets as $market)
                        <div class="feature feature-2 boxed boxed--border bg--secondary">
                            <div class="row">
                                <div class="col-md-1">
                                @if(is_null($market->logo))
                                    <i class="icon-Dollar-Sign" style="font-size: 52px;"></i>
                                @else
                                    <img src="{{ asset('storage/market_places/logos/' . $market->id .'/'. $market->logo) }}" width="200" height="50">
                                @endif
                                </div>
                                <div class="col-md-3" style="padding-top: 10px;">
                                    <h5><a href="{{ route('app.market.show', [$market]) }}">{{ $market->name }}</a></h5>
                                </div>
                                <div class="col-md-4 text-center">
                                    <h5>Posted By</h5>
                                    <p style="font-size: 15px;">
                                        <img src="{{ $market->user->avatar() }}" width="30" height="30" class="img-circle"> {{ $market->user->fullName }}
                                    </p>
                                </div>
                                <div class="col-md-4 text-center">
                                    <h5>Posted on</h5>
                                    <p style="font-size: 15px;">
                                        <b>{{ $market->created_at->diffForHumans() }}</b>
                                    </p>
                                </div>
                                
                            </div>
                        
                    </div>
                @endforeach
                
                @endif
                </div>
                <center>{{ $markets->links('vendor.pagination.default') }}</center>
            </div>
        </div>
    </div>
</div>
@endsection

