@extends('layouts.app')
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-mbo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-custom.css') }}">
    <div class="container">
        <div class="row">
        <h3 class="text-center">Start New Market Place</h3>
            <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                <div class="row">
                    <div class="boxed boxed--border bg--secondary">
                    @include('errors.form')
                        <form class="text-left" method="POST" action="{{ route('app.market.store') }}" enctype="multipart/form-data">
                            <div class="col-sm-12">
                                <span>Name*:</span>
                                {!! Form::text('name', null, ['class' => 'validate-required', 'placeholder' => 'Name']) !!}
                            </div>
                            <div class="col-sm-12">
                                <span>Description*:</span>
                                {!! Form::textarea('description', null, ['rows' => 5, 'class' => 'validate-required', 'placeholder' => 'Descripe']) !!}
                            </div>
                            <div class="col-sm-12">
                                <span>Logo:</span>
                                {!! Form::file('logo') !!}
                            </div>
                            <div class="col-sm-12">
                                <span>Attachments:</span>
                                {!! Form::file('files[]', ['multiple']) !!}
                                <span>Press and hold ctrl to select multiple files.</span>
                            </div>
                            <div class="col-sm-12 boxed">
                                <button type="submit" class="btn btn--primary type--uppercase">post market place</button>
                            </div>
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
                <!--end of row-->
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->

@endsection