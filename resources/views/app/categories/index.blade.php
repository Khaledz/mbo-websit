@extends('layouts.app')
@section('content')
    <div class="container mbo-container">
        <div class="row dark-container paddings">
            <div class="col-md-12">
            <h3>{{ $category->name }}</h3>
            <hr>
            </div>
            @if(count($category->events) == 0)
                <div class="col-md-12">
                    <div class="bg--secondary boxed boxed--border text-center">
                        <h4 class="text-center">There is no events in {{ $category->name }}</h4>
                    </div>
                </div>
            @else
                @foreach($dates as $date)
                <!-- Show year , take it from gallery mbo.bz-->
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-top: 10px;">
                        <div class="content">
                            <a href="{{ route('app.gallery.filter', [$category, $date]) }}">
                                <div class="content-overlay"></div>
                                <img class="content-image center-block" src="{{ asset('app/img/events.png') }}">
                                <div class="content-details fadeIn-left" style="background-color: #313131;">
                                    <h3 style="font-size: 15px;">{{ $date }}</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>

@endsection