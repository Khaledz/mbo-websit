@extends('layouts.app')
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-mbo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-custom.css') }}">
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="boxed">
                <h3 class="text-center">{{ $club->name }}</h3>
                <p class="lead text-center">{!! html_entity_decode($club->description) !!}</p>
                    <div class="col-md-12">
                        @include('partials.app.breadcrumb')
                    </div>
                <div class="row">
                   
                    <div class="col-sm-9">
                    @if($club->posts->where('status', 1)->count() == 0)
                    
                        <div class="feature feature-2 boxed boxed--border bg--secondary">
                            <h4 class="text-center">There is no posts at this moment.</h4>
                        </div>
                    </div>
                    @else
                    @foreach($club->posts()->where('status', 1)->latest()->get() as $post)
                    <div class="feature feature-2 boxed boxed--border bg--secondary">
                        <div class="row">
                            <div class="col-md-1">
                                <i class="icon-Speach-Bubbles" style="font-size: 50px;"></i>
                            </div>
                            <div class="col-md-4" style="padding-top: 10px;">
                                <h6><a href="{{ route('app.club.post.show', [$club, $post]) }}">{{ $post->title }}</a></h6>
                            </div>
                            <div class="col-md-3 text-center">
                                <h5>Posted by</h5>
                                <p style="font-size: 16px;">
                                    <b>
                                    <img src="{{ $post->user->avatar() }}" width="20" height="20" class="img-circle"> {{ $post->user->username }}
                                    </b>
                                </p>
                            </div>
                            <div class="col-md-2 text-center">
                                <h5>Views</h5>
                                <p style="font-size: 16px;">
                                    <b>{{ $post->views }}</b>
                                </p>
                            </div>
                            <div class="col-md-2 text-center">
                                <h5>Comments</h5>
                                <p style="font-size: 16px;">
                                    <b>{{ $post->comments->count() }}</b>
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>
                @endif
                    <div class="col-md-3">
                        <div class="boxed boxed--lg boxed--border bg--secondary" data-scroll-class="180px:pos-fixed" style="max-width: 323.325px;">
                            <div class="text-block text-center">
                                <p>
                                    <a href="{{ route('app.club.post.create', [$club]) }}"><button type="submit" class="btn btn--primary">New Discussion</button></a>
                                </p>
                            </div>
                            <hr>
                            <div class="text-block text-center">
                                <h4 class="text-center">Champion</h4>
                            @if($club->champions->count() > 0)
                                <img alt="avatar" src="{{ $club->champions->first()->avatar() }}" class="image--sm img-circle">
                                <span class="h5">{{ $club->champions->first()->fullName }}</span>
                            @else
                                -
                            @endif
                            </div>
                            <hr>
                            <div class="text-block text-center">
                                <h4 class="text-center">Launched</h4>
                                <span class="h5">{{ $club->launched }}</span>
                            </div>
                            <hr>
                                <div class="text-block text-center">
                                    <h4 class="text-center">Goals</h4>
                                    <span class="h5">{!! html_entity_decode($club->goals) !!}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--  end row  --}}
            </div>
        </div>
    </div>
</div>
@endsection
