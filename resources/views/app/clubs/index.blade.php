@extends('layouts.app')
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-mbo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-custom.css') }}">
<div class="container mbo-container">
    <div class="row">
        <div class="col-md-12">
            <div class="boxed">
                <h2 class="head">Multaqa of MBO</h2>

                    @include('partials.app.breadcrumb')
                    
                    @if($clubs->count() == 0)
                    <div class="feature feature-2 boxed boxed--border bg--secondary">
                        <h4 class="text-center">There is no multaqa at this moment.</h4>
                    </div>
                    @else
                    <div class="row">
                    @foreach($clubs as $club)
                        <div class="feature feature-2 boxed boxed--border bg--secondary">
                            <div class="row">
                            <div class="col-md-1">
                                <i class="icon-Speach-Bubbles" style="font-size: 52px;"></i>
                            </div>
                                <div class="col-md-3" style="padding-top: 10px;">
                                    <h6><a href="{{ route('app.club.show', [$club]) }}">{{ $club->name }}</a></h6>
                                </div>
                                <div class="col-md-2 text-center">
                                    <h5>Last topic</h5>
                                    <p style="font-size: 15px;">
                                        <b>
                                            @if(isset($club->getLastPost()->first()->title) && ! is_null($club->getLastPost()->first()->title))
                                                <a href="{{ route('app.club.post.show', [$club, $club->getLastPost()->first()]) }}">{{ $club->getLastPost()->first()->title }}</a>
                                            @else
                                                -
                                            @endif
                                        </b>
                                    </p>
                                </div>
                                <div class="col-md-2 text-center">
                                    <h5>Memebrs</h5>
                                    <p style="font-size: 15px;">
                                        <b>{{ $club->members->count() }}</b>
                                    </p>
                                </div>
                                <div class="col-md-2 text-center">
                                    <h5>Posts</h5>
                                    <p style="font-size: 15px;">
                                        <b>{{ $club->posts->count() }}</b>
                                    </p>
                                </div>
                                <div class="col-md-2 text-center">
                                    <h5>Join</h5>
                                    @if(auth()->user()->isJoinedClub($club) == 0)
                                        <p class="color--primary" style="font-size: 15px;">You are a member</p>
                                    @elseif(auth()->user()->isJoinedClub($club) == 1)
                                        <p class="color--primary" style="font-size: 15px;">Request sent!</p>
                                    @elseif(auth()->user()->isJoinedClub($club) == 2)
                                    <p id="join-{{ $club->id }}" style="font-size: 15px;">
                                        <a class="btn btn--xs btn--primary" href="#join" id="{{ $club->id }}">
                                            <span class="btn__text" style="color: white;">Send Request</span>
                                        </a>
                                    </p>
                                    @endif
                                </div>
                            </div>
                        
                    </div>
                @endforeach
                
                @endif
                </div>
                <center>{{ $clubs->links('vendor.pagination.default') }}</center>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$(function(){
    $("a[href$='#join']").click(function(){
        var club_id = $(this).attr('id');
        $.ajax({
            type: "GET",
            url: "{{ route('app.club.join') }}",
            data:{club_id:club_id},
            success: function(data) {
                $('#join-'+ club_id).html('<p class="color--primary">' + data.message + '</p>');
            },
            error: function (data) {
                $.each(data.responseJSON, function(key, value) {
                    $('#message').css('display', '').find("ul").append('<li>'+value+'</li>');
                });
            }
        });
    });
});
</script>
@endsection