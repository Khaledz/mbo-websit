@extends('layouts.app')

@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-mbo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-custom.css') }}">
<br>
<div class="container mbo-container">
    <div class="row dark-container paddings">
        <div class="col-md-12">
            <h3>Answers to frequently asked question</h3>
            <hr>
        </div>
<style></style>
<div class="container ">
    <div class="row paddings">
        <div class="col-sm-12 switchable__text">
            <ul class="accordion accordion-1" style="min-height: 0px;">
                @foreach($faqs as $faq)
                <li class="">
                    <div class="accordion__title">
                        <i class="fa fa-arrow-down" aria-hidden="true" style="font-size: 17px;color: #252525;padding-right: 5px;"></i><span class="h5">{{ $faq->question }}</span>
                    </div>
                    <div class="accordion__content">
                        <p class="lead">
                        {{ $faq->answer }}  
                        </p>
                    </div>
                </li>
            @endforeach
            
            </ul>
            <!--end accordion-->
        </div>
    </div>
    <!--end of row-->
</div>
</div></div>


@endsection