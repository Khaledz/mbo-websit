@extends('layouts.app')
@section('content')

<div class="container mbo-container" style="margin-top: 10px;">
    <div class="row dark-container paddings">
        <div class="col-md-12">
            <h3>Browse Members</h3>
            <hr>
        </div>
        @if(count($users) == 0)
            <div class="col-lg-12 col-xs-12">
                <h3 class="text-center">There is no members at this moment.</h3>
            </div>
        @else
            @foreach($users as $user)
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-top: 10px; margin-bottom: 10px;">
                <div class="content">
                    <a href="{{ route('app.user.show', $user) }}">
                        <div class="content-overlay center-block"></div>
                        <img class="content-image center-block" src="{{ $user->avatar() }}" style="height:210px;">
                        <div class="content-details fadeIn-left" style="background-color: #313131; ">
                                <h3 style="font-size:13px;">{{ $user->fullName }}</h3>
                        </div>
                    </a>
                </div>
            </div>
            @endforeach
        @endif
    </div>
</div>
@endsection