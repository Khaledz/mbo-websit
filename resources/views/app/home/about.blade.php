@extends('layouts.app')
@section('content')

<div class="container mbo-container">
    <div class="row dark-container">
        <div class="col-md-12 paddings">
            <h3>MBO Values</h3>
            <hr>
            <div class="col-md-4 mb-40 col-sm-4 col-xs-12 col-md-4">
                <div class="single-process bg-gray">
                    <span class="service-bar"></span>
                    <div class="process-icon">
                        <i class="fa fa-bar-chart"></i>
                    </div>
                    <div class="process-content">
                        <h3 style="font-size: 23px;">Purpose</h3>
                        <p style="font-size:18px;">{!! html_entity_decode($info->settings()->get('purpose')) !!}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-40 col-sm-4 col-xs-12 col-md-4">
                <div class="single-process bg-gray">
                    <span class="service-bar"></span>
                     <div class="process-icon">
                        <i class="fa fa-bar-chart"></i>
                    </div>
                    <div class="process-content">
                        <h3 style="font-size: 23px;">Vision</h3>
                        <p style="font-size: 18px;">{!! html_entity_decode($info->settings()->get('vision')) !!}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-40 col-sm-4 col-xs-12 col-md-4">
                <div class="single-process bg-gray">
                    <span class="service-bar"></span>
                    <div class="process-icon">
                        <i class="fa fa-bar-chart"></i>
                    </div>
                    <div class="process-content">
                        <h3 style="font-size: 23px;">Mission Statement</h3>
                        <p style="font-size: 18px;">{!! html_entity_decode($info->settings()->get('mission')) !!}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12 mb-40 col-sm-12 col-xs-12">
                <div class="single-process bg-gray">
                    <span class="service-bar"></span>
                    <div class="process-icon">
                        <i class="fa fa-paper-plane-o"></i>
                    </div>
                    <div class="process-content">
                        <h3 style="font-size: 23px;">Objectives</h3>
                        <p style="font-size: 18px;">{!! html_entity_decode($info->settings()->get('objectives')) !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>

@endsection