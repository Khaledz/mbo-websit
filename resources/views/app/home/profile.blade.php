@extends('layouts.app')

@section('content')

<section class="space--sm">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="bg--secondary boxed boxed--lg boxed--border">
                    <div class="text-block text-center">
                        <img alt="avatar" src="{{ $user->avatar() }}" class="image--md" width="80" height="120">
                        <span class="h5">{{ $user->fullName }}</span>
                        <span>{{ $user->settings()->job }}</span>
                    </div>
                    <div class="text-block clearfix text-center">
                        <ul class="row row--list">
                            <li class="col-sm-4">
                                <span class="type--fine-print block">Birthday:</span>
                                <span>{{ ($user->settings()->birthday == "") ? '-' : $user->settings()->birthday}}</span>
                            </li>
                            <li class="col-sm-4">
                                <span class="type--fine-print block">Member Since:</span>
                                <span>{{ $user->created_at->toDateString() }}</span>
                            </li>
                            <li class="col-sm-4">
                                <span class="type--fine-print block">Email:</span>
                                <a href="#">{{ $user->email }}</a>
                            </li>
                    </ul></div>
                    
                </div>
                <div class="bg--secondary boxed boxed--border">
                    <ul class="row row--list clearfix text-center">
                        <li class="col-sm-4 col-xs-6">
                            <span class="h6 type--uppercase type--fade">Posts</span>
                            <span class="h3">{{ $user->posts->count() }}</span>
                        </li>
                        <li class="col-sm-4 col-xs-6">
                            <span class="h6 type--uppercase type--fade">Comments</span>
                            <span class="h3">{{ $user->comments->count() }}</span>
                        </li>
                        <li class="col-sm-4 col-xs-6">
                            <span class="h6 type--uppercase type--fade">Files</span>
                            <span class="h3">{{ $user->attachments->count() }}</span>
                        </li>
                    </ul>
                </div>
                
                <div class="bg--secondary boxed boxed--border">
                    <h4>Events Related to you</h4>
                    <ul>
                    @if($user->events->count() == 0)
                        There is no events related to you.
                    @else
                    @foreach($user->events as $event)
                        <li class="clearfix">
                            <div class="row">
                                <div class="col-md-2 col-xs-3 text-center">
                                    <div class="icon-circle">
                                        <i class="icon icon--lg material-icons">comment</i>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-7">
                                    <span class="type--fine-print">{{ $event->created_at->diffForHumans() }}</span>
                                    <a href="{{ route('app.event.show', [$event]) }}" class="block color--primary">{{ $event->name }}</a>
                                </div>
                            </div>
                        </li>
                    @endforeach
                    @endif
                    </ul>
                    <a href="{{ route('app.event.index') }}" class="type--fine-print pull-right">View All</a>
                </div>

                <div class="bg--secondary boxed boxed--border">
                    <h4>Multaqa Related to you</h4>
                    <ul>
                    @if($user->events->count() == 0)
                        There is no events related to you.
                    @else
                    @foreach($user->clubs as $club)
                        <li class="clearfix">
                            <div class="row">
                                <div class="col-md-2 col-xs-3 text-center">
                                    <div class="icon-circle">
                                        <i class="icon icon--lg material-icons">comment</i>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-7">
                                    <span class="type--fine-print">{{ $club->created_at->diffForHumans() }}</span>
                                    <a href="{{ route('app.club.show', [$club]) }}" class="block color--primary">{{ $club->name }}</a>
                                </div>
                            </div>
                        </li>
                    @endforeach
                    @endif
                    </ul>
                    <a href="{{ route('app.club.index') }}" class="type--fine-print pull-right">View All</a>
                </div>

                <div class="bg--secondary boxed boxed--border">
                    <h4>Market Places Related to you</h4>
                    <ul>
                    @if($user->events->count() == 0)
                        There is no events related to you.
                    @else
                    @foreach($user->clubs as $club)
                        <li class="clearfix">
                            <div class="row">
                                <div class="col-md-2 col-xs-3 text-center">
                                    <div class="icon-circle">
                                        <i class="icon icon--lg material-icons">comment</i>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-7">
                                    <span class="type--fine-print">{{ $club->created_at->diffForHumans() }}</span>
                                    <a href="{{ route('app.club.show', [$club]) }}" class="block color--primary">{{ $club->name }}</a>
                                </div>
                            </div>
                        </li>
                    @endforeach
                    @endif
                    </ul>
                    <a href="{{ route('app.club.index') }}" class="type--fine-print pull-right">View All</a>
                </div>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
@endsection