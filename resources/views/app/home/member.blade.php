@extends('layouts.app')
@section('content')
<div class="container mbo-container">
    <div class="row dark-container paddings">
        <div class="col-md-12">
            <h3>Upcoming Events</h3>
            <hr>
        </div>
        <div class="row paddings">
            <div class="col-md-12">
                @if($upcomings->count() == 0)
                    <div class="boxed boxed--border text-center bg--secondary">
                        <span style="color:#6779b2; font-size: 18px;">There is no upcoming events at this moment.</span>
                    </div>
                @else
                    <table class="table table-hover" style="background-color: white;">
                        <thead>
                            <tr>
                                <th>Event Name</th>
                                <th>Event Date</th>
                                <th>Event Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($upcomings as $event)
                            <tr>
                                <td><a href="{{ route('app.event.show', [$event]) }}">{{ $event->name }}</a></td>
                                <td>{{ $event->date }}</td>
                                <td>{{ $event->time }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
    <div class="row dark-container paddings">
        <div class="col-md-12">
            <h3>Members Section</h3>
            <hr>
        </div>
        @foreach($activities as $activity)
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-top: 15px;">
            <div class="content">
               <a href="{{ url($activity->url) }}">
                    <div class="content-overlay center-block"
                    @if(! is_null($activity->background_color))
                     style="background-color: {{ $activity->background_color }}"
                    @endif
                    ></div>
                    <img class="content-image center-block" src="{{ $activity->icon() }}">
                    <div class="content-details fadeIn-left"
                    @if(! is_null($activity->background_text_color))
                        style="background-color: {{ $activity->background_text_color }}"
                    @else
                        style="background-color: #313131;"
                    @endif
                    >
                        <h3 
                        @if(! is_null($activity->text_color))
                            style="font-size: 13px; color: {{ $activity->text_color }}"
                        @else
                            style="font-size: 13px;" 
                        @endif
                        >
                        {{ $activity->title }}</h3>
                    </div>
                </a>
            </div>
        </div>
        @endforeach
        <!-- <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-top: 15px;">
            <div class="content">
                <a href="events.php">
                    <div class="content-overlay"></div>
                    <img class="content-image" src="{{ asset('app/img/events.png') }}">
                    <div class="content-details fadeIn-left" style="background-color: #313131;">
                        <h3 style="font-size: 15px;">International Events</h3>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-top: 15px;">
            <div class="content">
                <a href="events.php">
                    <div class="content-overlay"></div>
                    <img class="content-image" src="{{ asset('app/img/events.png') }}">
                    <div class="content-details fadeIn-left" style="background-color: #313131;">
                        <h3 style="font-size: 15px;">Educational Events</h3>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-top: 15px;">
            <div class="content">
                <a href="events.php">
                    <div class="content-overlay"></div>
                    <img class="content-image" src="{{ asset('app/img/events.png') }}">
                    <div class="content-details fadeIn-left" style="background-color: #313131;">
                        <h3 style="font-size: 15px;">Networking Events</h3>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-top: 15px;">
            <div class="content">
                <a href="events.php">
                    <div class="content-overlay"></div>
                    <img class="content-image" src="{{ asset('app/img/events.png') }}">
                    <div class="content-details fadeIn-left" style="background-color: #313131;">
                        <h3 style="font-size: 15px;">CSR Events</h3>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-top: 15px;">
            <div class="content">
                <a href="events.php">
                    <div class="content-overlay"></div>
                    <img class="content-image" src="{{ asset('app/img/moltaqa.png') }}">
                    <div class="content-details fadeIn-left" style="background-color: #313131;">
                        <h3 style="font-size: 15px;">Multaqa</h3>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-top: 15px;">
            <div class="content">
                <a href="events.php">
                    <div class="content-overlay"></div>
                    <img class="content-image" src="{{ asset('app/img/marketplace.png') }}">
                    <div class="content-details fadeIn-left" style="background-color: #313131;">
                        <h3 style="font-size: 15px;">Market Place</h3>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-top: 15px;">
            <div class="content">
                <a href="events.php">
                    <div class="content-overlay"></div>
                    <img class="content-image" src="{{ asset('app/img/calender.png') }}">
                    <div class="content-details fadeIn-left" style="background-color: #313131;">
                        <h3 style="font-size: 15px;">Market Place</h3>
                    </div>
                </a>
            </div>
        </div> -->
    </div>
</div>
@endsection