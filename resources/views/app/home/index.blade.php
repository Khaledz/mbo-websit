@extends('layouts.app')
@section('content')

<div class="container mbo-container">
    <div class="row dark-container paddings">
        <div class="col-md-12">
            <h3>Members Section</h3>
            <hr>
        </div>
        @foreach(\App\Models\Activity::paginate(4) as $activity)
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-top: 15px;">
            <div class="content">
               <a href="{{ url($activity->url) }}">
                    <div class="content-overlay center-block"
                    @if(! is_null($activity->background_color))
                     style="background-color: {{ $activity->background_color }}"
                    @endif
                    ></div>
                    <img class="content-image center-block" src="{{ $activity->icon() }}">
                    <div class="content-details fadeIn-left"
                    @if(! is_null($activity->background_text_color))
                        style="background-color: {{ $activity->background_text_color }}"
                    @else
                        style="background-color: #313131;"
                    @endif
                    >
                        <h3 
                        @if(! is_null($activity->text_color))
                            style="font-size: 13px; color: {{ $activity->text_color }}"
                        @else
                            style="font-size: 13px;" 
                        @endif
                        >
                        {{ $activity->title }}</h3>
                    </div>
                </a>
            </div>
        </div>
        @endforeach
    </div>

    <div class="row dark-container paddings">
        <div class="col-md-12">
            <h3>Welcome to MBO</h3>
            <hr>
        </div>
        <div class="col-lg-6">
            <div class="col-md-12 paddings" style="background-color: white; border-radius: 6px;">
                <h3>Introduction</h3>
                {!! html_entity_decode($info->settings()->get('introduction')) !!}
            </div>
        </div>
        <div class="col-lg-6">
            <div class="col-md-12 paddings" style="background-color: white; border-radius: 6px;">
                <h3>Activites:</h3>
                <div class="col-md-6">
                    {!! html_entity_decode($info->settings()->get('activites')) !!}
                </div>
                <div class="col-md-6">
                    <b>Social</b><br>
                    Events and Trips<br>
                    Family and Health<br>
                    Cultural Self Improvement
                </div>
            </div>
        </div>
    </div>


</div>
@endsection