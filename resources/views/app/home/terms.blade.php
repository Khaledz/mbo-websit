@extends('layouts.app')
@section('content')

    <div class="container mbo-container">
    	<div class="row dark-container paddings">
        <div class="col-md-12">
            <h3>Conditions for Membership</h3>
            <hr>
        </div>
        <div class="row paddings">
            <div class="col-md-12">
				<p class="text-justify" style="font-size: 23px;">
					Membership to MBO is by invitation only. Three existing members of MBO have to recommend a potential member before it can be reviewed by the Chapter Board. In addition each member must fill the application form (which is available for download from our website) and include a check to cover the chapter fees. Three endorsing MBO members must be knowledgeable of the character and background of the applicant for membership.  As a business organization we require that all applicants be senior managers in their organizations or owners of their business. Professionals related to the business community are also welcome for membership. As an organization that has a strong focus on the value system of members, we require that all applicants be of the highest ethical conduct.  The Chapter Board has the right to accept or reject any application for membership without giving any justification.  Applicants from all religions, ethnic background, color or sex are welcome to join.  Membership to MBO is by invitation only. The process starts when three MBO members recommend a potential member and sends it to the chapter's administrator. The three recommending members must be knowledgeable of the character and background of the potential member. The potential member recommendation is subject to approval by the board. After approval the new member will be asked to fill up the application form and issue a cheque / transfer for the membership fee.
				</p>
				<br>
				<button type="submit" class="btn center-block" style="color: white;background: #5e76b6;
    border-color: #5e76b6"><a href="{{ url('/register') }}" style="color: white;">I ACCEPT AND AGREE THE TERMS OF MEMBERSHIP</a></button>
			</div>
    	</div>
    </div>
   	</div>

@endsection