@extends('layouts.app')

@section('content')

<section class="text-center bg--secondary">
<div class="container">
    <div class="row">
        <div class="col-sm-10 col-md-8">
            <h1>MBO Philosophy</h1>
        </div>
    </div>
    <!--end of row-->
</div>
<!--end of container-->
</section>

<section class="feature-large switchable ">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <img alt="Image" class="" src="{{ asset('img/logo.png') }}">
            </div>
            <div class="col-md-5 col-sm-6">
                <div class="heading-block">
                    <h4>We will strive to improve the global business community by promoting higher ethical practices through education and teamwork.</h4>
                </div>
                <div class="text-block">
                    <h4>The Purpose</h4>
                    <p>
                    To develop the Muslim Business Community.
                    </p>
                </div>
                <div class="text-block">
                    <h4>The Vision</h4>
                    <p>
                    Better Business leaders through higher values.
                    </p>
                </div>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
<section>
    <div class="container">
    <h2 class="head">Objectves</h2>
    <div class="row">
        <div class="col-sm-10 col-md-8 col-md-offset-2">
            <div class="row">
                <div class="col-sm-6">
                    <div class="feature feature-3 boxed boxed--lg boxed--border bg--secondary">
                        <h4><small class="h1 h1--large" style="font-weight: 600;">.</small> Instill higher values in MBO members</h4>
                        <br>
                    </div>
                </div>
                    <!--end boxed-->
                <div class="col-sm-6">
                    <div class="feature feature-3 boxed boxed--lg boxed--border bg--secondary">
                        <h4><small class="h1 h1--large" style="font-weight: 600;">.</small> Develop members to become world-class businessmen and managers</h4>
                    </div>
                </div>
                    <!--end boxed-->
                <div class="col-sm-6">
                    <div class="feature feature-3 boxed boxed--lg boxed--border bg--secondary">
                        <h4><small class="h1 h1--large" style="font-weight: 600;">.</small> Develop the sprit of teamwork and Shoura among members</h4>
                    </div>
                </div>
                    <!--end boxed-->
                <div class="col-sm-6">
                    <div class="feature feature-3 boxed boxed--lg boxed--border bg--secondary">
                        <h4><small class="h1 h1--large" style="font-weight: 600;">.</small> Promote networking among members</h4>
                    </div>
                    <!--end boxed-->
                </div>
                </div>
            </div>
            <!--end of row-->
        </div>
    </div>
    </div>
</section>
@endsection