@extends('layouts.app')

@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-mbo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-custom.css') }}">
    <div class="container mbo-container dark-container">

        <div class="row paddings">
        <h2 class="head">Contact Us</h2>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3709.93259121045!2d39.14157021494215!3d21.58855418569778!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjHCsDM1JzE4LjgiTiAzOcKwMDgnMzcuNSJF!5e0!3m2!1sen!2s!4v1505817281848" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

        </div>
        <!--end of row-->
    </div>

    <div class="container mbo-container dark-container paddings">
    <h3 class="text-center">Minaret Business Organization</h3>
    <p class="lead text-center">{!! html_entity_decode($info->settings()->get('address')) !!}</p>
    
        <div class="row">
            <div class="col-sm-8 col-md-12">
                <div class="row">
                    <form class="text-left" action="{{ route('app.home.contact') }}" method="POST">
                        <div class="col-sm-6">
                            <label>Your Name:</label>
                            <input type="text" name="name" class="validate-required">
                        </div>
                        <div class="col-sm-6">
                            <label>Email Address:</label>
                            <input type="email" name="email" class="validate-required validate-email">
                        </div>
                        <div class="col-sm-12">
                            <label>Message:</label>
                            <textarea rows="6" name="message" class="validate-required"></textarea>
                        </div>
                        <div class="col-sm-5 col-md-4">
                            <button type="submit" class="btn btn--primary type--uppercase">Send Enquiry</button>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
                <!--end of row-->
            </div>
        </div>
        <!--end of row-->
    </div>
</div>
@endsection