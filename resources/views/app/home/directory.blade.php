@extends('layouts.app')

@section('content')
<div class="container mbo-container">
    <div class="row dark-container">
        <div class="col-md-12">
            <h3>Board Members</h3>
            <hr>
            @if(! isset($board))
                <center><span style="font-weight:bold; font-size: 16px;">There is no boards at this moment.</center><br>
            @else
            <center><span style="font-weight:bold; font-size: 35px;">International Board Chairman </span><br><span style="font-size:25px;">Dr. Ghassan A. AlSulaiman</center><br>
            <h3 class="text-center" style="border: 2px solid #000; font-size: 25px;">Current Board ({{ \Carbon\Carbon::parse($board->from)->year }} - {{ \Carbon\Carbon::parse($board->to)->year }})</h3>
            <table class="table table-striped text-center">
                @foreach($board->users as $user)
                <tr>
                    <td>{{ $user->pivot->position }}</td>
                    <td>{{ $user->fullName }}</td>
                </tr>
                @endforeach
            </table>
            @endif

            @if($boards->count() > 0)
            @foreach($boards as $board)
            <div class="row" style="background-color: #FFF"><br></div>
            <h3 class="text-center">Pervious Board ({{ \Carbon\Carbon::parse($board->from)->year }} - {{ \Carbon\Carbon::parse($board->to)->year }})</h3>
            <hr>
            <table class="table table-striped text-center">
                @foreach($board->users as $user)
                <tr>
                    <td>{{ $user->pivot->position }}</td>
                    <td>{{ $user->fullName }}</td>
                </tr>
                @endforeach
            </table>
            @endforeach
            @endif
        </div>
    </div>
</div>
@endsection