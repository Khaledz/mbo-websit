@extends('layouts.app')

@section('content')
<section class="container text-center">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
            <h2 class="head">Membership Home Page</h2>
        </div>
    </div>
</section>

<section class="container text-center">
    <div class="row">
        <div class="col-sm-3">
            <div class="feature feature-1">
                <img alt="Image" src="{{ asset('img/1.png') }}">
                <div class="feature__body boxed boxed--lg boxed--border">
                    <h4>Social Events</h4>
                    <a href="#">
                        Learn More
                    </a>
                </div>
            </div>
            <!--end feature-->
        </div>
        <div class="col-sm-3">
            <div class="feature feature-1">
                <img alt="Image" src="{{ asset('img/2.png') }}">
                <div class="feature__body boxed boxed--lg boxed--border">
                    <h4>International Events</h4>
                    <a href="#">
                        Learn More
                    </a>
                </div>
            </div>
            <!--end feature-->
        </div>
        <div class="col-sm-3">
            <div class="feature feature-1">
                <img alt="Image" src="{{ asset('img/3.png') }}">
                <div class="feature__body boxed boxed--lg boxed--border">
                    <h4>Eductional Events</h4>
                    <a href="#">
                        Learn More
                    </a>
                </div>
            </div>
            <!--end feature-->
        </div>
        <div class="col-sm-3">
            <div class="feature feature-1">
                <img alt="Image" src="{{ asset('img/4.png') }}">
                <div class="feature__body boxed boxed--lg boxed--border">
                    <h4>Networking Events</h4>
                    <a href="#">
                        Learn More
                    </a>
                </div>
            </div>
            <!--end feature-->
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <div class="feature feature-1">
                <img alt="Image" src="{{ asset('img/5.png') }}">
                <div class="feature__body boxed boxed--lg boxed--border">
                    <h4>CSR Events</h4>
                    <a href="#">
                        Learn More
                    </a>
                </div>
            </div>
            <!--end feature-->
        </div>
        <div class="col-sm-3">
            <div class="feature feature-1">
                <img alt="Image" src="{{ asset('img/6.png') }}">
                <div class="feature__body boxed boxed--lg boxed--border">
                    <h4>Multaqa</h4>
                    <a href="#">
                        Learn More
                    </a>
                </div>
            </div>
            <!--end feature-->
        </div>
        
        <div class="col-sm-3">
            <div class="feature feature-1">
                <img alt="Image" src="{{ asset('img/7.png') }}">
                <div class="feature__body boxed boxed--lg boxed--border">
                    <h4>Market Place</h4>
                    <a href="#">
                        Learn More
                    </a>
                </div>
            </div>
            <!--end feature-->
        </div>
        <div class="col-sm-3">
            <div class="feature feature-1">
                <img alt="Image" src="{{ asset('img/8.png') }}">
                <div class="feature__body boxed boxed--lg boxed--border">
                    <h4>Events Calendar</h4>
                    <a href="#">
                        Learn More
                    </a>
                </div>
            </div>
            <!--end feature-->
        </div>
    </div>
</section>
@endsection