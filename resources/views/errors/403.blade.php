<!DOCTYPE html>
<html>
    <head>
        <title>MBO - No permission</title>

        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/earlyaccess/notokufiarabic.css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 50px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                    <img class="logo" alt="logo" src="{{ asset('app/img/logo.png') }}" width="380" height="250">
                    <br>
                    You don't have permission to access this page.
                </div>
            </div>
        </div>
    </body>
</html>
