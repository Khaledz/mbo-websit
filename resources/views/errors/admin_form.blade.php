@if(session('message'))
<div class="alert alert-success">{{ session('message') }}</div>

@elseif(count($errors) > 0)
<div class="well">
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</div>
@endif