@if(count($errors) > 0)
<div class="boxed boxed--border bg--secondary boxed--lg box-shadow">
	<ul>
	    @foreach($errors->all() as $error)
	        <span class="h5" style="margin-bottom: 0;"><li class="" style="color: red;">{{ $error }}</li></span>
	    @endforeach
	</ul>
</div>
@endif