<!DOCTYPE html>
<html lang="en" class="">
<head>
  <meta charset="utf-8" />
  <title>MBO admin - @yield('title', '')</title>
  <link rel="stylesheet" href="{{ asset('admin/animate.css/animate.css')}}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('admin/font-awesome/css/font-awesome.min.css')}}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('admin/simple-line-icons/css/simple-line-icons.css')}}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('admin/jquery/bootstrap/dist/css/bootstrap.css')}}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('admin/css/font.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('admin/css/app.css') }}" type="text/css" />
  @yield('header')
  <script src="{{ asset('admin/jquery/jquery/dist/jquery.js') }}"></script>
  <script src="{{ asset('admin/jquery/bootstrap/dist/js/bootstrap.js') }}"></script>
  <script src="{{ asset('admin/js/ui-load.js') }}"></script>

</head>
<body>
<div class="app app-header-fixed ">
  

  <!-- header -->
  @include('partials.admin.header')
  <!-- / header -->

  <!-- aside -->
  @include('partials.admin.sibebar')
  <!-- / aside -->


  <!-- content -->
  <div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
      <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">@yield('title', '')</h1>
      </div>
	    @yield('content')
	   </div>
  </div>
  <!-- /content -->
  
  <!-- footer -->
  <footer id="footer" class="app-footer" role="footer">
    <div class="wrapper b-t bg-light">
      <span class="pull-right"><a href ui-scroll="app" class="m-l-sm text-muted"><i class="fa fa-long-arrow-up"></i></a></span>
      &copy; {{ date('Y')}} MBO Website.
    </div>
  </footer>
  <!-- / footer -->

</div>

<script src="{{ asset('admin/js/ui-load.js') }}"></script>
<script src="{{ asset('admin/js/ui-jp.config.js') }}"></script>
<script src="{{ asset('admin/js/ui-jp.js') }}"></script>
<script src="{{ asset('admin/js/ui-nav.js') }}"></script>
<script src="{{ asset('admin/js/ui-toggle.js') }}"></script>
<script src="{{ asset('admin/js/ui-client.js') }}"></script>
<script src="{{ asset('admin/js/bootbox.min.js') }}"></script>
<script src="{{ asset('admin/libs/jquery/chosen/chosen.jquery.min.js') }}"></script>
<script type="text/javascript">var csrfToken="{!! csrf_token() !!}";</script>
<script>
$(function () {
$(".w-md").chosen();
});
</script>
@yield('script')
</body>
</html>
