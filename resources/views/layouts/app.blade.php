<!DOCTYPE html>
<html lang="en">
<head>
	<title>@lang('title.name') - {{ $title or '' }}</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('app/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('app/css/fullcalendar.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app/css/iconsmind.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app/css/fullcalendar.print.css') }}" media="print">
	<link rel="stylesheet" type="text/css" href="{{ asset('app/css/lightbox.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app/css/custom.css') }}">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<script src="{{ asset('app/js/moment.min.js') }}"></script>
  	<script src="{{ asset('app/js/jquery.js') }}"></script>
  	<script src="{{ asset('app/js/bootstrap.min.js') }}"></script>
  	<script src="{{ asset('app/js/fullcalendar.min.js') }}"></script>
  	<script src="{{ asset('app/js/lightbox.min.js') }}"></script>
</head>

<body class="js">
	<div id="preloader"></div>

		@include('partials.app.header')
		@include('partials.app.sliders')
        @yield('content')

        @include('partials.app.footer')
        @include('partials.app.form.notification')
        <script src="{{ asset('app/js/datepicker.js') }}"></script>
        <script src="{{ asset('app/js/smooth-scroll.min.js') }}"></script>
        <script src="{{ asset('app/js/scripts.js') }}"></script>
        @yield('script')
</body>
</html>