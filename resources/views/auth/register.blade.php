@extends('layouts.auth')
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-mbo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-custom.css') }}">
<div class="container mbo-container" style="margin-top: 10px;">
    <div class="row dark-container paddings">
        <div class="col-md-12">
            <h3>Register</h3>
            <hr>
        </div>
        <div class="col-lg-12 col-xs-12" style="margin-top: 15px;">
            @include('errors.form')

            <form method="POST" action="{{ route('register') }}">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <label>First Name:</label>
                        <input type="text" name="fname" placeholder="First Name" />
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <label>Last Name:</label>
                        <input type="text" name="lname" placeholder="Last Name" />
                    </div>
                    <div class="col-xs-12">
                        <label>Username:</label>
                        <input type="text" name="username" placeholder="username" />
                    </div>
                    <div class="col-xs-12">
                        <label>Email:</label>
                        <input type="email" name="email" placeholder="E-mail" />
                    </div>
                    <div class="col-xs-12">
                        <label>Phone:</label>
                        <input type="text" name="phone" value="" placeholder="+966"/>
                        <span class="help-block">Example:(+966)xxxxxxxxx</span>
                    </div>
                    <div class="col-xs-12">
                        <label>Password:</label>
                        <input type="password" name="password" placeholder="Password" />
                    </div>
                    <div class="col-xs-12">
                        <label>How did hear about MBO?</label>
                        <input type="text" name="invite" placeholder="How did hear about MBO" />
                    </div>
                    <!-- <div class="col-xs-12">
                        <div class="input-checkbox">
                        <input id="checkbox" type="checkbox" name="agree" />
                        <label for="checkbox"></label>
                        </div>
                    <span style="font-size: 16px;">I have read and agree to the <a href="{{ route('app.home.term') }}" target="_blank">terms and conditions</a></span>
                    </div> -->

                    <div class="col-xs-12">
                        <button type="submit" class="btn btn--primary type--uppercase">Send membership request</button>
                    </div>
                    <div class="col-xs-12 text-center">
                        <span class="type--fine-print">You have an account?  
                            <a href="{{ url('login') }}">log in.</a>
                        </span>
                    </div>
                </div>
                <!--end row-->
                {{ csrf_field() }}
            </form>
        </div>
    </div>
</div>
@endsection