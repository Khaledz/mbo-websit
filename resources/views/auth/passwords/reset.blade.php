@extends('layouts.auth')

@section('content')
<section class="height-100 text-center">
    <div class="container pos-vertical-center">
    <img src="{{ asset('img/logo.png') }}" class="img-responsive center-block" width="300" height="180">
        <div class="row">
            <div class="col-sm-7 col-md-5">
                <h2>Recover your account</h2>
                <p class="lead">
                    Enter email address to send a recovery email.
                </p>
                <form>
                    <input type="email" placeholder="Email Address">
                    <button class="btn btn--primary type--uppercase" type="submit">Recover Account</button>
                </form>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
@endsection
