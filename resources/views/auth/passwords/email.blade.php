@extends('layouts.auth')
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-mbo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-custom.css') }}">

<div class="container mbo-container">
    <div class="row dark-container paddings">
        <div class="col-md-12">
            <h2 style="padding: 0; margin: 5px;">Recover your account</h2>
            <hr style="margin: 0;">
        </div>
            <form style="margin-top: 10px;">
                <br>
                <input type="email" placeholder="Email Address">
                <button class="btn btn--primary type--uppercase" type="submit">Recover Account</button>
            </form>
        </div>
    </div>
    <!--end of row-->
</div>
<!--end of container-->

@endsection