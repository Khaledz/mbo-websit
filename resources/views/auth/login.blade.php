@extends('layouts.auth')
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-mbo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/css/stack-custom.css') }}">
<div class="container mbo-container" style="margin-top: 10px;">
    <div class="row dark-container paddings">
        <div class="col-md-12">
            <h3>Login</h3>
            <hr>
        </div>
        <div class="col-lg-12 col-xs-12" style="margin-top: 15px;">
            @include('errors.form')

            <form method="POST" action="{{ route('login') }}">
                <div class="row">
                    <div class="col-xs-12">
                        <input type="email" name="email" placeholder="E-mail" />
                    </div>
                    <div class="col-xs-12">
                        <input type="password" name="password" placeholder="Password" />
                    </div>
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn--primary type--uppercase">Login</button>
                    </div>
                    <div class="col-xs-12 text-center">
                        <span class="type--fine-print">Forgot your username or password? 
                            <a href="{{ url('/password/reset') }}">Recover account</a>
                        </span>
                        <br>
                        <span class="type--fine-print">You don't have account with us? 
                            <a href="{{ route('app.home.term') }}">Become a membership</a>
                        </span>
                    </div>
                </div>
                <!--end row-->
                {{ csrf_field() }}
            </form>
        </div>
    </div>
</div>
@endsection