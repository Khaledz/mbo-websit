<header id="header" class="app-header navbar" role="menu">
      <!-- navbar header -->
      <div class="navbar-header bg-dark">
        <button class="pull-right visible-xs dk" ui-toggle-class="show" target=".navbar-collapse">
          <i class="glyphicon glyphicon-cog"></i>
        </button>
        <button class="pull-right visible-xs" ui-toggle-class="off-screen" target=".app-aside" ui-scroll="app">
          <i class="glyphicon glyphicon-align-justify"></i>
        </button>
        <!-- brand -->
        <a href="{{ route('app.home.index') }}" class="navbar-brand text-lt">
          <i class="fa fa-btc"></i>
          <img src="app/img/logo.png" alt="." class="hide">
          <span class="hidden-folded m-l-xs">MBO</span>
        </a>
        <!-- / brand -->
      </div>
      <!-- / navbar header -->

      <!-- navbar collapse -->
      <div class="collapse pos-rlt navbar-collapse box-shadow bg-white-only">
        
        <!-- link and dropdown -->
        <ul class="nav navbar-nav hidden-sm">
          <li>
            <a>
              <span>{{ date("F j, Y") }}</span>
            </a>
          </li>
        </ul>
        <!-- / link and dropdown -->

        <!-- nabar right -->
        <ul class="nav navbar-nav navbar-right">
        <li>
          <a href="{{ route('app.home.index') }}">Home Page</a>
        </li>
        
          <li class="dropdown">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle clear" data-toggle="dropdown">
              <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
                <img src="{{ asset(auth()->user()->avatar())}}" alt="...">
                <i class="on md b-white bottom"></i>
              </span>
              <span class="hidden-sm hidden-md">Hi, {{ Auth::user()->username }}</span>
            </a>
            <!-- dropdown -->
            <ul class="dropdown-menu">
              <li>
                <a href="{{ route('app.setting.index', auth()->user()) }}">
                  <span>Settings</span>
                </a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="{{ url('/logout') }}"
                    onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                    Logout
                </a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                </form>
            </li> 
            </ul>
            <!-- / dropdown -->
          </li>
        </ul>
        <!-- / navbar right -->
      </div>
      <!-- / navbar collapse -->
  </header>
