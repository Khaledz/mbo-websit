<aside id="aside" class="app-aside hidden-xs bg-dark">
  <div class="aside-wrap">
    <div class="navi-wrap">
      <!-- user -->
      <div class="clearfix hidden-xs text-center hide" id="aside-user">
        <div class="dropdown wrapper">
          <a href="app.page.profile">
            <span class="thumb-lg w-auto-folded avatar m-t-sm">
              <img src="img/a0.jpg" class="img-full" alt="...">
            </span>
          </a>
          
          <!-- dropdown -->
          <ul class="dropdown-menu animated fadeInRight w hidden-folded">
            <li>
              <a href>Settings</a>
            </li>
            <li>
              <a href="page_profile.html">Profile</a>
            </li>
            <li>
              <a href>
                <span class="badge bg-danger pull-right">3</span>
                Notifications
              </a>
            </li>
            <li class="divider"></li>
            <li>
              <a href="page_signin.html">Logout</a>
            </li>
          </ul>
          <!-- / dropdown -->
        </div>
        <div class="line dk hidden-folded"></div>
      </div>
      <!-- / user -->

      <!-- nav -->
      <nav ui-nav class="navi clearfix">
        <ul class="nav">
          <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
          </li>
          <li>
            <a href="{{ route('admin.home.index') }}">      
              <i class="glyphicon glyphicon-stats icon"></i>
              Dashboard
            </a>
          </li>
          <li>
            <a href="{{ url('/') }}">      
              <i class="glyphicon glyphicon-stats icon"></i>
              Home Page
            </a>
          </li>
          <li class="line dk"></li>

          <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
            <span>Components</span>
          </li>
          <li>
            <a href="{{ route('admin.user.index') }}">  
            <i class="glyphicon glyphicon-user"></i>
              Users
            </a>
          </li>
          <!-- <li>
            <a href="{{ route('admin.role.index') }}">  
            <i class="glyphicon glyphicon-stats icon"></i>
              Roles
            </a>
          </li>
          <li>
            <a href="">  
            <i class="glyphicon glyphicon-stats icon"></i>
              Permissions
            </a>
          </li> -->
          <li>
            <a href="{{ route('admin.club.index') }}">  
            <i class="glyphicon glyphicon-stats icon"></i>
              Multaqa
            </a>
          </li>
          <li>
            <a href="{{ route('admin.category.index') }}">  
            <i class="glyphicon glyphicon-stats icon"></i>
              Event Categories
            </a>
          </li>
          <li>
            <a href="{{ route('admin.event.index') }}">  
            <i class="glyphicon glyphicon-stats icon"></i>
              Events
            </a>
          </li>
          <li>
            <a href="{{ route('admin.market.index') }}">  
            <i class="glyphicon glyphicon-stats icon"></i>
              Market Places
            </a>
          </li>
          <li>
            <a href="{{ route('admin.announcement.index') }}">  
            <i class="glyphicon glyphicon-stats icon"></i>
              Announcements
            </a>
          </li>
           <li>
            <a href="{{ route('admin.gallery.index') }}">  
            <i class="glyphicon glyphicon-stats icon"></i>
              Gallery
            </a>
          </li>
          <li>
            <a href="{{ route('admin.slider.index') }}">  
            <i class="glyphicon glyphicon-stats icon"></i>
              Home Sliders
            </a>
          </li>
          <li>
            <a href="{{ route('admin.activity.index') }}">  
            <i class="glyphicon glyphicon-stats icon"></i>
              Home Activities
            </a>
          </li>
          <li>
            <a href="{{ route('admin.renwal.index') }}">  
            <i class="glyphicon glyphicon-stats icon"></i>
              Renwals
            </a>
          </li>
          <li>
            <a href="{{ route('admin.board.index') }}">  
            <i class="glyphicon glyphicon-stats icon"></i>
              Board Members
            </a>
          </li>
          <li>
            <a href="{{ route('admin.setting.index') }}">  
            <i class="glyphicon glyphicon-stats icon"></i>
              MBO Texts
            </a>
          </li>
          <li>
            <a href="{{ route('admin.faq.index') }}">  
            <i class="glyphicon glyphicon-stats icon"></i>
              MBO Faq
            </a>
          </li>
          <li>
            <a href="{{ route('admin.post.index') }}">  
            <i class="glyphicon glyphicon-stats icon"></i>
              MBO Posts
            </a>
          </li>
          <!-- <li>
            <a href="{{ route('admin.comment.index') }}">  
            <i class="glyphicon glyphicon-stats icon"></i>
              MBO Comments
            </a>
          </li> -->
        </ul>
      </nav>
      <!-- nav -->
    </div>
  </div>
</aside>