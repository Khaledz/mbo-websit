@if($sliders->count() > 0)
<div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="bs-carousel" style="margin-bottom:20px;">
    <!-- Overlay -->
    <div class="overlay"></div>
    <!-- Indicators -->
    <ol class="carousel-indicators">
        @for($i = 0; $i < count($sliders); $i++)
        <li data-target="#bs-carousel" data-slide-to="{{ $i }}" class="{{ ($i == 0) ? 'active' : ''}}"></li>
        @endfor
    </ol>
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        @foreach($sliders as $slider)
        <div class="item slides {{ ($loop->first) ? 'active' : ''}}" style="height: 450px">
            <img src="{{ asset('storage/sliders/' . $slider->id .'/'. $slider->attachments->first()->name) }}" style="width: 100%; height: 450px;">
        </div>
        @endforeach            
    </div> 
</div>
@endif