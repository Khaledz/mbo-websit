<div class="container-fluid" style="background: url({{ asset('app/img/footar.jpg') }}); background-size:cover;  min-height: 150px; margin-top: 20px;">
<div class="container mbo-container">
<div class="row" style="margin-top: 35px;">
    <div class="col-md-5">
      <a href="{{ url('/') }}" class="footer_link">Home</a>
      <a href="{{ route('app.home.about') }}" class="footer_link">About Us</a>
      <a href="{{ route('app.home.about') }}" class="footer_link">Memberships</a>
      <a href="{{ route('app.home.contact') }}" class="footer_link">Contact Us</a><br>
    
      
     <ul class="social-icons icon-circle icon-zoom list-unstyled list-inline "> 
      <br>
      <span class="footer_link">Join Us On</span>
      <li> <a href="{{ $info->settings()->get('facebook') }}"><i class="fa fa-facebook"></i></a></li>  
      <li> <a href="{{ $info->settings()->get('twitter') }}"><i class="fa fa-twitter"></i></a></li>  
      <li> <a href="{{ $info->settings()->get('youtube') }}"><i class="fa fa-youtube"></i></a></li>  
      <li> <a href="{{ $info->settings()->get('linkedin') }}"><i class="fa fa-linkedin"></i></a></li>  
    </ul><BR>
    </div>
    <div class="col-md-3 text-center">
      <img src="{{ asset('app/img/phone.png') }}" width="50">
      <br>
      <span class="" style="padding-right: 0; color: white;">Contact Us</span>
    </div>
    <div class="col-md-4">
      <div class="row">
      <div class="col-md-6">
        <span class="footer_link">{!! html_entity_decode($info->settings()->get('tel1')) !!}</span>
      </div>
      <div class="col-md-6">
        <span class="footer_link">{!! html_entity_decode($info->settings()->get('tel2')) !!}</span>
      </div>
      <br>
    <div class="col-md-12">
      <span class="footer_link"><a href="mailto:info@mbo.bz" style="color: white;">Email: {!! html_entity_decode( $info->settings()->get('email')) !!}</a></span><br>
    </div>
    <div class="col-md-12">
      <span class="footer_link">© {{ date('Y') }} MBO, all Rights Reserved</span><br>
    </div>
    </div>
    </div>
</div>
</div>



<script>
jQuery(document).ready(function($) {  

// site preloader -- also uncomment the div in the header and the css style for #preloader
$(window).load(function(){
  $('#preloader').fadeOut('slow',function(){$(this).remove();});
});

});
</script>
