<ul class="breadcrumb" style="font-size: 18px;">
	<li><a href="{{ route('app.home.index') }}">Home</a></li>
	@if(! is_null(Request::segment(1)) && is_null(Request::segment(2)) && is_null(Request::segment(3)))
	<li>{{ (Request::segment(1) == 'club') ? ucfirst('multaqa') : str_replace('-', ' ', Request::segment(1)) }}</li>
	@endif
	@if(! is_null(Request::segment(1)) && ! is_null(Request::segment(2)) && is_null(Request::segment(3)))
	<li><a href="{{ route('app.'. Request::segment(1) .'.index') }}">{{ (Request::segment(1) == 'club') ? ucfirst('multaqa') :str_replace('-', ' ', Request::segment(1)) }}</a></li>
	<li>{{ str_replace('-', ' ', Request::segment(2)) }}</li>
	@endif
	@if(! is_null(Request::segment(1)) && ! is_null(Request::segment(2)) && ! is_null(Request::segment(3)))
	<li><a href="{{ route('app.'. Request::segment(1) .'.index') }}">{{ (Request::segment(1) == 'club') ? ucfirst('multaqa') : str_replace('-', ' ', Request::segment(1)) }}</a></li>
	<li><a href="{{ route('app.'. Request::segment(1) .'.show', [Request::segment(2)]) }}">{{  str_replace('-', ' ', Request::segment(2)) }}</a></li>
	<li>{{ str_replace('-', ' ', Request::segment(4)) }}</li>
	@endif
</ul>