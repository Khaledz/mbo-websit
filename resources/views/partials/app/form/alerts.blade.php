@if(session()->has('payment_success'))
<div class="notification pos-right pos-top col-sm-4 col-md-3 notification--reveal" data-animation="from-top" data-autoshow="500">
    <div class="boxed boxed--border border--round box-shadow">
        <div class="text-block">
            <h5><i class="fa fa-check fa-lg"></i> تم إرسال طلبك بنجاح</h5>
            <p>
                سنقوم بمبراجعة طلبك وتأكيده خلال 24 ساعة.
            </p>
        </div>
    </div>
    <div class="notification-close-cross notification-close"></div>
</div>
@endif


@if(session()->has('payment_error'))
<div class="notification pos-right pos-top col-sm-4 col-md-3 notification--reveal" data-animation="from-top" data-autoshow="500">
    <div class="boxed boxed--border border--round box-shadow">
        <div class="text-block">
            <h5><i class="fa fa-times fa-lg"></i> حدث خطأ أثناء عملية الدفع</h5>
            <p>
                لم تتكتمل عملية الدفع بنجاح، من فضلك عاود المحاولة أو أستخدم حساب آخر.
            </p>
        </div>
    </div>
    <div class="notification-close-cross notification-close"></div>
</div>
@endif

@if(session()->has('register_success'))
<div class="notification pos-right pos-top col-sm-4 col-md-4 notification--reveal" data-animation="from-top" data-autoshow="500">
    <div class="boxed boxed--border border--round box-shadow">
        <div class="text-block">
            <h5><i class="fa fa-check fa-lg"></i> تم تسجيل حسابك بنجاح</h5>
            <p>
                لديك الآن لوحة تحكم خاصة لاستخدام خدماتنا.
            </p>
        </div>
    </div>
    <div class="notification-close-cross notification-close"></div>
</div>
@endif

@if(session()->has('ticket_success'))
<div class="notification pos-right pos-top col-sm-4 col-md-4 notification--reveal" data-animation="from-top" data-autoshow="500">
    <div class="boxed boxed--border border--round box-shadow">
        <div class="text-block">
            <h5><i class="fa fa-check fa-lg"></i> تم إنشاء تذكرتك بنجاح</h5>
            <p>
                بإمكانك متابعة التذكرة في صفحة الدعم الفني.
            </p>
        </div>
    </div>
    <div class="notification-close-cross notification-close"></div>
</div>
@endif