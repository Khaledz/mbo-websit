@if (session('message_header'))
<div class="notification pos-right pos-top col-sm-4 col-md-3 notification--reveal" data-animation="from-bottom" data-autohide="2000">
	<div class="boxed boxed--border border--round box-shadow">
        <div class="text-block">
            <h5>{{ session('message_header') }}</h5>
            <p>
                {{ session('message_body') ?? session('message_body')}}
            </p>
        </div>
    </div>
</div>


<script>
setTimeout( function(){ 
    $('.notification').fadeOut(1000);
 }, 3000);        
</script>
@endif