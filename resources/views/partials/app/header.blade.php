<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span> 
            </button>
            <a href="{{ url('/') }}"><img src="{{ asset('app/img/logo.jpg') }}" class="logo" style="height: 95px;width: 422px;margin-top: 15px;"> </a>
        </div>
        
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="divider nav navbar-nav pull-right col-md-7 col-xs-12" style="margin-top: 19px;">
              @if(auth::guest())
                <li><a href="#" data-toggle="modal" data-target="#login">Login</a></li>
                <li><a href="{{ route('app.home.term') }}">Register</a></li>
              @else
                @if(auth()->user()->hasRole(['admin', 'board admin']))
                <li><a href="{{ route('admin.home.index') }}"><i class="fa fa-globe" style="color: #7685ac"></i> Admin</a></li>
                @endif
                <li><a href="{{ route('app.user.show', auth()->user()) }}"><i class="fa fa-user" style="color: #7685ac"></i> User Profile</a></li>
                <li><a href="{{ route('app.setting.index', auth()->user()) }}"><i class="fa fa-cogs" style="color: #7685ac"></i> User Settings</a></li>
                <li style="border-right: none;"><a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" style="color: #7685ac"></i> Logout</a></li>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
              @endif
            </ul>
            <ul class="space nav navbar-nav pull-right col-md-7 col-xs-12">
                <li><a href="{{ route('app.home.index') }}" style="font-size: 17px;">Home</a></li>
                <li class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" id="menu1">
                  <a href="#">About Us <span class="caret"></span></a>
                  </li>
                  <ul class="dropdown-menu" role="menu" aria-labelledby="menu1" style="left: 15%;right: 56%;">
                    <li><a href="{{ route('app.home.about') }}">Value</a></li>
                    <li><a href="{{ route('app.home.directory') }}">Board Members</a></li>
                    <li><a href="{{ route('app.home.faq') }}">FAQ</a></li>
                  </ul>
                <!-- <li><a href="{{ route('app.home.about') }}" style="font-size: 17px;">About us</a></li> -->
                <li><a href="{{ route('app.home.member') }}" style="font-size: 17px;">Browse Members</a></li>
                <li><a href="{{ route('app.home.contact') }}" style="font-size: 17px;">Contact Us</a></li> 
            </ul>
            <!-- <ul class="nav navbar-nav pull-right col-md-6 col-xs-12">
                <li><a href="{{ route('app.home.member') }}"><i class="fa fa-users" style="color: #7685ac"></i> Browse Members</a></li>
                <li><a href="{{ route('app.user.show', auth()->user()) }}"><i class="fa fa-user" style="color: #7685ac"></i> User Profile</a></li>
                <li><a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a></li>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </ul> -->
        </div>
    </nav>

    <!-- Login -->
    <div id="login" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="padding: 10px;">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center">Membership Login</h4>
          </div>
          <div class="modal-body">
            <p class="text-center" id="message-login" style="color: red;"></p>
          <form method="post" action="{{ route('login') }}" id="login-form">
            {{ csrf_field() }}
            <input type="email" class="form-control" placeholder="Email" name="email" style="padding: 20px 10px; margin-bottom: 10px;">
            <input type="password" class="form-control" placeholder="Password" name="password" style="padding: 20px 10px; margin-bottom: 10px;">
            <button type="submit" class="btn btn-primary btn-block">Login</button>
            <p class="text-center">Forgot your username or password? <a href="{{ url('password/reset') }}">Recover account</a> <br>
            You don't have account with us? <a href="{{ url('register') }}">Join MBO</a></p>
          </form>
            
          </div>
        </div>

        </div>
    </div>

    <!-- Register -->
    <div id="register" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="padding: 10px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center">MBO Membership Form</h4>
      </div>
      <div class="modal-body">
       <p class="lead" id="message-register"></p>
      <form method="post" action="{{ route('register') }}" id="register-form">
        {{ csrf_field() }}
        <div class="row">
          <div class="col-md-6">
            <label>First Name:</label>
            <input name="fname" type="text" class="form-control" placeholder="First Name">
          </div>
          <div class="col-md-6">
            <label>Last Name:</label>
            <input name="lname" type="text" class="form-control" placeholder="Last Name">
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
          <label>UserName:</label>
            <input name="username" type="text" class="form-control" placeholder="Username">
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
          <label>Email:</label>
            <input name="email" type="email" class="form-control" placeholder="Email">
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
          <label>Phone:</label>
            <input name="phone" type="text" class="form-control" placeholder="Phone">
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
          <label>Password:</label>
            <input name="password" type="text" class="form-control" placeholder="Password">
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
          <label>How did you hear about MBO:</label>
            <input name="username" type="text" class="form-control" placeholder="How did you hear about MBO">
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">
          <input type="checkbox"> I have read and agree to the terms and conditions
          </div>
        </div>
        <div class="row">
        <div class="col-md-12">
        <button type="submit" class="btn btn-primary btn-block">Send Membership Request</button>
        </div>
        </div>  
      </form>
        
      </div>
    </div>

  </div>
</div>

@section('script')
<script>
$(function() {

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $('#login-form').on('submit', function(e){
    e.preventDefault(); 

    var formData = $('#login-form').serialize();
    console.log(formData);
    $.ajax({
        type: 'POST',
        url: '/login',
        data: formData,
        success: function (response) {
            location.reload();
        },
        error: function (jqXHR) {
          var response = $.parseJSON(jqXHR.responseText);
          $('#message-login').html(response.error);
        }
    });
  });

  $('#register-form').on('submit', function(e){
    e.preventDefault(); 

    var formData = $('#register-form').serialize();
    $.ajax({
        type: 'POST',
        url: '/register',
        data: formData,
        success: function (response) {
            location.reload();
        },
        error: function (jqXHR) {
            var errors = $.parseJSON(jqXHR.responseText);
            errorHtml='<div class="errors"><ul>';
            $.each( errors, function( key, value ) {
                 errorHtml += '<li>' + value[0] + '</li>';
           });
            errorHtml += '</ul></div>';
            $( '#message-register' ).html( errorHtml );
        }
    });
  });

});
</script>
@endsection
    