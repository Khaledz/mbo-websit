@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            {{ config('app.name') }} <br>
            <img src="{{ asset('img/logo.png') }}" width="250" height="200" />
        @endcomponent
    @endslot

    {{-- Body --}}
    
    <h2>Hello, {{ $user->Fullname }}</h2> <br>
    {{ $content }}

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            &copy; {{ date('Y') }} {{ config('app.name') }}. All rights reserved.
        @endcomponent
    @endslot
@endcomponent
