
@if ($paginator->hasPages())
    <nav>
        <ul class="pagination pagination-rounded">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="disabled page-item"><a href="#" class="page-link"><span aria-hidden="true" class="fa fa-left-arrow"></span>&laquo;</a></li>
        @else
            <li class="page-item"><a href="{{ $paginator->previousPageUrl() }}" aria-label="Previous" class="page-link"><span aria-hidden="true" class="fa fa-left-arrow"></span>&laquo;</a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="disabled"><span>{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        {{-- <li class="active"><span>{{ $page }}</span></li> --}}
                        <li class="page-item active"><a href="#" class="page-link">{{ $page }}</a></li>
                    @else
                        {{-- <li><a href="{{ $url }}">{{ $page }}</a></li> --}}
                        <li class="page-item"><a href="{{ $url }}" class="page-link">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="page-item"><a href="{{ $paginator->nextPageUrl() }}" aria-label="Previous" class="page-link"><span aria-hidden="true" class="fa fa-left-arrow"></span>&raquo;</a></li>
        @else
            <li class="disabled page-item"><a href="#" class="page-link"><span aria-hidden="true" class="fa fa-left-arrow"></span>&raquo;</a></li>
        @endif
    </ul>
    </nav>
@endif
