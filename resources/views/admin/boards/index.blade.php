@extends('layouts.admin')

@section('title', 'List board members')

@section('content')
<div class="wrapper-md">
@include('errors.admin_form')
    <a href="{{ route('admin.board.create') }}"><button class="btn m-b-xs btn-sm btn-primary btn-addon center-block"><i class="fa fa-plus"></i>Create new board member</button></a>
    <br>
    
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
          <br>
          @if(count($boards) == 0)
            <div class="alert alert-warning">There is no board member at this moment</div>
          @else
          <table class="table table-striped m-b-none">
          <thead>
            <tr>
              <th>#</th>
              <th>From</th>
              <th>To</th>
              <th>Members</th>
              <th>Status</th>             
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($boards as $board)
            <tr>                 
              <td>#{{ $board->id }}</td>
              <td>{{ $board->from }}</td>
              <td>{{ $board->to }}</td>
              <td>{{ ($board->users->count()) }} {{ str_plural('member',$board->users->count()) }}</td>
              <td>{{ ($board->status == 0) ? 'Inactive' : 'Active' }}</td>
              <td>
                  <a href="{{ route('admin.board.show', $board) }}" class="btn btn-info btn-xs">View</a> -
                  {!! Form::open(['method' => 'DELETE', 'route' => ['admin.board.destroy', $board] ,'style' => 'display:inline']) !!}
                      {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                  {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        <br>
        <br>
        <center>{{ $boards->links('vendor.pagination.default') }}</center>
        @endif
      </div>
  </div>
  <p>Note: Once you add a new board members, the pervious board will move to archive.</p>
</div>

@endsection