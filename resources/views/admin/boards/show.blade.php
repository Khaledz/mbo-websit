@extends('layouts.admin')

@section('title', 'Board Member')
@section('content')
<div class="wrapper-md">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
          <p class="pull-right">{{ $board->from }} - {{ $board->to }}</p>
        </div>
        <div class="panel-body">
        <table class="table table-striped m-b-none">
          <thead>
            <tr>
              <th>Position</th>
              <th>Member</th>                    
            </tr>
          </thead>
          <tbody>
            @if($board->users->count() == 0)
              There is no members
            @else
            @foreach($board->users as $user)
            <tr>                    
              <td>
                {{ $user->pivot->position }}
              </td>
              <td>
              {{ $user->fullName }}
              </td>
            </tr>
            @endforeach
            @endif
            
          </tbody>
        </table>
        </div>
      </div>
    </div>
    
    </div>
  </div>
</div>
  
@endsection