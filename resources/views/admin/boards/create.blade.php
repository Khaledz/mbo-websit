@extends('layouts.admin')

@section('title', 'Create new board member')
@section('style')
  <link rel="stylesheet" href="{{ asset('admin/libs/jquery/chosen/bootstrap-chosen.css') }}">
@endsection
@section('content')
@include('errors.admin_form')
            {!! Form::open(['route' => ['admin.board.store'], 'class' => 'bs-example form-horizontal']) !!}
<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
            

            <div class="form-group {{ $errors->has('from') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">From</label>
              <div class="col-lg-4">
                <div class="input-group date" data-provide="datepicker">
                  <input type="text" class="form-control"  name="from">
                  <div class="input-group-addon">
                      <span class="glyphicon glyphicon-th"></span>
                  </div>
              </div>

              </div>
                @if ($errors->has('date'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('to') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">To</label>
              <div class="col-lg-4">
                <div class="input-group date" data-provide="datepicker">
                  <input type="text" class="form-control" name="to">
                  <div class="input-group-addon">
                      <span class="glyphicon glyphicon-th"></span>
                  </div>
              </div>

              </div>
                @if ($errors->has('to'))
                    <span class="help-block">
                        <strong>{{ $errors->first('to') }}</strong>
                    </span>
                @endif
            </div>
</div>
  </div>
</div>

<div class="wrapper-md">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
            <div class="panel-heading font-bold">                  
              Board Members
              <button class="add_field_button pull-right btn m-b-xs btn-sm btn-primary btn-addon"><i class="fa fa-plus"></i>Add more member</button>
            </div>
            <div class="input_fields_wrap panel-body">
              <div class="row">
              <div class="form-group">
                  <label class="col-sm-1 control-label">Position</label>
                  <div class="col-sm-4">
                    <input type="text" name="positions[]" class="form-control">
                  </div>
                  <label class="col-sm-1 control-label">Member</label>
                  <div class="col-sm-4">
                  {!! Form::select('users[]', $users, null, ['ui-jq' => 'chosen', 'class' => 'w-md', 'multiple']) !!}
                  </div>
                </div>
              </div>
        
              
            </div>
            

      </div>
            <div class="form-group btn-group-justified">
              <div class="col-lg-offset-4 col-lg-4">
                <button class="btn m-b-xs btn-md btn-info btn-addon center-block"><i class="fa fa-check"></i> Save</button>
              </div>
            </div>
      </div>
    </div>
    


{!! Form::close() !!}
          
@endsection
@section('script')
  <script src="{{ asset('admin/libs/jquery/chosen/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('admin/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('admin/js/moment.min.js') }}"></script>
    <script src="{{ asset('admin/js/bootstrap-datetimepicker.min.js') }}"></script>

   <script type="text/javascript">
      $(function () {
          $('#datetimepicker3').datetimepicker({
              format: 'LT'
          });
          
           var max_fields      = 15; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).on("click",wrapper, function(e){ //user click on remove text
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment

            $(wrapper).append('<div class="row"> <div class="form-group"> <label class="col-sm-1 control-label">Position</label> <div class="col-sm-4"> <input type="text" name="positions[]" class="form-control"> </div><label class="col-sm-1 control-label">Member</label> <div class="col-sm-4">{!! Form::select('users[]', $users, null, ["class"=> "w-md", "multiple"]) !!}</div><button class="remove_field btn m-b-xs btn-xs btn-danger"><i class="fa fa-trash-o"></i> Delete</button></div></div>'); //add input box

            $(".w-md").chosen();
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
      });
  </script>
  
@endsection
