@extends('layouts.admin')

@section('title', 'Edit a user')
@section('content')
<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
        {!! Form::model($user, ['method' => 'PATCH', 'route' => ['admin.user.update', $user], 'method' => 'PUT', 'class' => 'bs-example form-horizontal']) !!}
            <div class="form-group {{ $errors->has('role_id') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Role</label>
              <div class="col-lg-4">
                {!! Form::select('role_id', $roles, null, ['class' => 'selectpicker form-control', 'data-live-search' => true]) !!}
              </div>
               @if ($errors->has('role_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('role_id') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('fname') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">First Name</label>
              <div class="col-lg-4">
                {!! Form::text('fname', null, ['class' => 'form-control', 'placeholder' => 'First Name']) !!}
              </div>
                @if ($errors->has('fname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('fname') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('lname') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Last Name</label>
              <div class="col-lg-4">
                {!! Form::text('lname', null, ['class' => 'form-control', 'placeholder' => 'Last Name']) !!}
              </div>
                @if ($errors->has('lname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('lname') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Email</label>
              <div class="col-lg-4">
                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
              </div>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Phone</label>
              <div class="col-lg-4">
                {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => '+966']) !!}
              </div>
              @if ($errors->has('phone'))
                    <span class="help-block">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('username') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Username</label>
              <div class="col-lg-4">
                {!! Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'Username']) !!}
              </div>
              @if ($errors->has('username'))
                    <span class="help-block">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Active</label>
              <div class="col-lg-4">
                {!! Form::select('status', ['1' => 'Active', '0' => 'Deactivate'], null, ['class' => 'selectpicker form-control', 'data-live-search' => true]) !!}
              </div>
               @if ($errors->has('status'))
                    <span class="help-block">
                        <strong>{{ $errors->first('status') }}</strong>
                    </span>
                @endif
            </div>
            
            <div class="form-group btn-group-justified">
              <div class="col-lg-offset-4 col-lg-4">
                <button class="btn m-b-xs btn-md btn-info btn-addon center-block"><i class="fa fa-check"></i> Save</button>
              </div>
            </div>
          {!! Form::close() !!}
        </div>
  </div>
</div>

@endsection