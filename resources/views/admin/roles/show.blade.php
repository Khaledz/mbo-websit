@extends('layouts.admin')

@section('title', $user->name . "'s profile")
@section('content')
<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
        User Information
        </div>
  </div>
</div>

@endsection