@extends('layouts.admin')

@section('title', 'List roles')

@section('content')
<div class="wrapper-md">
@include('errors.admin_form')
    <a href="{{ route('admin.role.create') }}"><button class="btn m-b-xs btn-sm btn-primary btn-addon center-block"><i class="fa fa-plus"></i>Create new role</button></a>
    <br>
    
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
          <br>
          @if(count($roles) == 0)
            <div class="alert alert-warning">There is no roles at this moment</div>
          @else
          <table class="table table-striped m-b-none">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Users Number</th>                      
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($roles as $role)
            <tr>                    
              <td>#{{ $role->id }}</td>
              <td>{{ $role->name }}</td>
              <td>({{ $role->users()->count() }}) {{ str_plural('user', $role->users()->count()) }}</td>
              <td>
                  <a href="{{ route('admin.role.edit', $role->id) }}" class="btn btn-info btn-xs">Update</a> - 
                  {!! Form::open(['method' => 'DELETE', 'route' => ['admin.role.destroy', $role->id] ,'style' => 'display:inline']) !!}
                      {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                  {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        <br>
        <br>
        <center>{{ $roles->links('vendor.pagination.default') }}</center>
        @endif
      </div>
  </div>
</div>

@endsection