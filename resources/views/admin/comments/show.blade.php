@extends('layouts.admin')

@section('title', 'Comments')
@section('content')
<div class="wrapper-md">
  <div class="row">
    <div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">
          Comment for post {{ $comment->post->title }}
        </div>
        <div class="panel-body">
        <h5 class="text-left">{!! html_entity_decode($comment->content) !!}</h5>
        </div>
    </div>
  </div>
  
  </div>
</div>

<div class="wrapper-md">
  <div class="row">
    <div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          Comment Files
        </div>
        <div class="panel-body">
        @if($comment->attachments->count() == 0)
          There is no files attach to this post.
        @else
        <div class="row">
        <div class="col-md-3" style="margin-bottom: 10px;">
          @foreach($comment->attachments as $attachment)
            <a href="{{ asset('storage/clubs/'.$comment->post->club->id. '/posts/'. $comment->post->id .'/'.  $comment->id .'/'. $attachment->name) }}">{{ $attachment->orginal_name }}</a><br>
          @endforeach
        </div>
      </div>
        @endif
        </div>
    </div>
  </div>

  
  
  </div>
</div>


@endsection