@extends('layouts.admin')

@section('title', 'Create new gallery')
@section('content')
<link href="{{ asset('admin/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" media="all" />
<link href="{{ asset('admin/css/summernote.css') }}" rel="stylesheet" type="text/css" media="all" />
<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
             @include('errors.admin_form')
            {!! Form::open(['route' => ['admin.gallery.store'], 'class' => 'bs-example form-horizontal', 'enctype' => 'multipart/form-data']) !!}

            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Gallery Name</label>
              <div class="col-lg-4">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'gallery Title']) !!}
              </div>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('date') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Gallery Date</label>
              <div class="col-lg-4">
                <div class="input-group date" data-provide="datepicker">
                  <input type="text" class="form-control" id="datepicker" name="date">
                  <div class="input-group-addon">
                      <span class="glyphicon glyphicon-th"></span>
                  </div>
              </div>

              </div>
                @if ($errors->has('date'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Gallery Description</label>
              <div class="col-lg-9">
                {!! Form::textarea('description', null, ['rows' => '4', 'class' => 'form-control summernote', 'id' => 'summernote', 'placeholder' => 'Gallery Description']) !!}
              </div>
                @if ($errors->has('description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
              <label class="col-lg-2 control-label">Images</label>
              <div class="col-lg-4">
                <input type="file" name="files[]" multiple="">
                <span class="help-block m-b-none">Press and hold ctrl to select multiple files.</span>
              </div>
            </div>

            <div class="form-group btn-group-justified">
              <div class="col-lg-offset-4 col-lg-4">
                <button class="btn m-b-xs btn-md btn-info btn-addon center-block"><i class="fa fa-check"></i> Save</button>
              </div>
            </div>
          {!! Form::close() !!}
        </div>
  </div>
</div>
<script src="{{ asset('admin/js/summernote.min.js') }}"></script>
<script src="{{ asset('admin/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('admin/js/moment.min.js') }}"></script>
    <script src="{{ asset('admin/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
    $(document).ready(function() {
      $('.datepicker').datepicker({
            setDate: '12/03/2017'
          });
      $('#datetimepicker3').datetimepicker({
              format: 'LT',
          });
      $('.summernote').summernote({
        placeholder: 'Description',
        tabsize: 2,
        height: 300
      });
    });
    </script>

@endsection

