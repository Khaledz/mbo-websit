@extends('layouts.admin')
@section('header')
  <link href="{{ asset('admin/libs/jquery/chosen/bootstrap-chosen.css') }}" rel="stylesheet" type="text/css" media="all" />
  <link href="{{ asset('admin/css/summernote.css') }}" rel="stylesheet" type="text/css" media="all" />
@endsection
@section('title', 'Edit a comment')
@section('content')
@include('errors.admin_form')
<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
        {!! Form::model($comment, ['method' => 'PUT', 'route' => ['admin.comment.update', $comment], 'method' => 'PUT', 'class' => 'bs-example form-horizontal', 'files' => true]) !!}
          
            <div class="form-group {{ $errors->has('content') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Content</label>
              <div class="col-lg-9">
                {!! Form::textarea('content', null, ['rows' => '4', 'class' => 'form-control', 'id' => 'summernote', 'placeholder' => 'content']) !!}
              </div>
                @if ($errors->has('content'))
                    <span class="help-block">
                        <strong>{{ $errors->first('content') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
              <label class="col-lg-2 control-label">Images</label>
              <div class="col-lg-4">
                <input type="file" name="files[]" multiple="">
              </div>
              @if($comment->attachments->count() > 0)
              <div class="col-lg-4">
              <ul>
                @foreach($comment->attachments as $attachment)
                <li>
                  <a href="{{ asset('storage/clubs/'.$comment->post->club->id. '/posts/'. $comment->post->id .'/'. $comment->id .'/'. $attachment->name) }}">{{ $attachment->orginal_name }}</a>
                   - <a href="{{ route('admin.attachment.destroy', [$comment, $attachment]) }}" style="color: red;"> Remove</a>
                </li>
                @endforeach
              </ul>
              </div>
              @endif
            </div>

            <div class="form-group btn-group-justified">
              <div class="col-lg-offset-4 col-lg-4">
                <button class="btn m-b-xs btn-md btn-info btn-addon center-block"><i class="fa fa-check"></i> Save</button>
              </div>
            </div>
          {!! Form::close() !!}
        </div>
  </div>
</div>
@endsection
@section('script')
<script src="{{ asset('admin/js/summernote.min.js') }}"></script>
    <script>
    $(document).ready(function() {
      $('.summernote').summernote({
        placeholder: 'Description',
        tabsize: 2,
        height: 300
      });
    });
    </script>
    <script src="{{ asset('admin/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('admin/js/moment.min.js') }}"></script>
    <script src="{{ asset('admin/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('admin/libs/jquery/chosen/chosen.jquery.min.js') }}"></script>
   <script type="text/javascript">

      
     
  </script>
  
@endsection