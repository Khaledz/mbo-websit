@extends('layouts.admin')

@section('title', 'Edit ' . $event->name)

@section('header')
  <link href="{{ asset('admin/libs/jquery/chosen/bootstrap-chosen.css') }}" rel="stylesheet" type="text/css" media="all" />
  <link href="{{ asset('admin/css/summernote.css') }}" rel="stylesheet" type="text/css" media="all" />
@endsection
@section('content')
<style></style>
<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
             @include('errors.admin_form')
            {!! Form::model($event, ['method' => 'PUT', 'route' => ['admin.event.update', $event], 'class' => 'bs-example form-horizontal', 'enctype' => 'multipart/form-data']) !!}
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Status</label>
              <div class="col-lg-4">
                {!! Form::select('status', ['0' => 'Inactive', '1' => 'Active'], $event->status, ['class' => 'selectpicker form-control', 'data-live-search' => true]) !!}
              </div>
               @if ($errors->has('status'))
                    <span class="help-block">
                        <strong>{{ $errors->first('status') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('event_category_id') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Category</label>
              <div class="col-lg-4">
                {!! Form::select('event_category_id', $categories, $event->event_category_id, ['class' => 'selectpicker form-control', 'data-live-search' => true]) !!}
              </div>
               @if ($errors->has('event_category_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('event_category_id') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Event Name</label>
              <div class="col-lg-4">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'event Name']) !!}
              </div>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
            <label class="col-lg-2 control-label">Event Description</label>
            <div class="col-lg-9">
            {!! Form::textarea('description', null, ['rows' => '4', 'class' => 'form-control summernote', 'id' => 'summernote', 'placeholder' => 'Event Description']) !!}
            </div>
              @if ($errors->has('description'))
                  <span class="help-block">
                      <strong>{{ $errors->first('description') }}</strong>
                  </span>
              @endif
          </div>

          <div class="form-group {{ $errors->has('program') ? 'has-error' : ''}}">
          <label class="col-lg-2 control-label">Event Program</label>
          <div class="col-lg-9">
          {!! Form::textarea('program', null, ['rows' => '4', 'class' => 'form-control summernote', 'id' => 'summernote', 'placeholder' => 'Event Program']) !!}
          </div>
            @if ($errors->has('program'))
                <span class="help-block">
                    <strong>{{ $errors->first('program') }}</strong>
                </span>
            @endif
        </div>

            <div class="form-group {{ $errors->has('speaker') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Event Speaker</label>
              <div class="col-lg-9">
                {!! Form::textarea('speaker', null, ['rows' => '4', 'class' => 'form-control summernote', 'id' => 'summernote', 'placeholder' => 'Event Speaker']) !!}
              </div>
                @if ($errors->has('speaker'))
                    <span class="help-block">
                        <strong>{{ $errors->first('speaker') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('date') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Event Date</label>
              <div class="col-lg-4">
                <div class="input-group date" data-provide="datepicker">
                  <input type="text" class="form-control" id="datepicker" name="date">
                  <div class="input-group-addon">
                      <span class="glyphicon glyphicon-th"></span>
                  </div>
              </div>

              </div>
                @if ($errors->has('date'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('time') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Event Time</label>
              <div class="col-lg-4">
                <div class='input-group date' id='datetimepicker3'>
                    <input type='text' class="form-control" name="time" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div>
              </div>
                @if ($errors->has('speaker'))
                    <span class="help-block">
                        <strong>{{ $errors->first('speaker') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('location') ? 'has-error' : ''}}">
            <label class="col-lg-2 control-label">Google Location Url(Embed Map)</label>
            <div class="col-lg-4">
              {!! Form::text('location', null, ['class' => 'form-control', 'placeholder' => 'Google Location Url(Embed Map)']) !!}
              <span class="help-block">Example: <?= htmlspecialchars('<iframe src="url"></iframe>'); ?></span>
            </div>
                @if ($errors->has('location'))
                    <span class="help-block">
                        <strong>{{ $errors->first('location') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('note') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">note (optional)</label>
              <div class="col-lg-4">
                {!! Form::text('note', null, ['class' => 'form-control', 'placeholder' => 'note']) !!}
              </div>
                @if ($errors->has('note'))
                    <span class="help-block">
                        <strong>{{ $errors->first('note') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
              <label class="col-lg-2 control-label">Files (optional)</label>
              <div class="col-lg-4">
                <input type="file" name="files[]" multiple="">
                <span class="help-block m-b-none">Press and hold ctrl to select multiple files.</span>
              </div>
              @if(! is_null($event->galleries()->first()))
              @if($event->galleries()->first()->attachments()->count() > 0)
              <div class="col-lg-4">
              <ul>
                @foreach($event->galleries()->first()->attachments()->get() as $attachment)
                <li>
                  <a href="{{ asset('storage/galleries/' . $event->galleries()->first()->id .'/'. $attachment->name) }}"/>{{ $attachment->orginal_name }}</a>
                   - <a href="{{ route('admin.attachment.destroy', [$event, $attachment]) }}" style="color: red;"> Remove</a>
                </li>
                @endforeach
              </ul>
              </div>
              @endif
              @endif
            </div>

             <div class="line line-dashed b-b line-lg pull-in"></div>
            <h3>Invitations</h3>

            <div class="form-group">
              <label class="col-lg-2 control-label">Send invitation to</label>
              <div class="col-lg-4">
              <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="invite" value="custom" checked>
                    <i></i>
                    Custom Members
                  </label>
                </div>
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="invite" value="all" >
                    <i></i>
                    All Members
                  </label>
                </div>
                
              </div>
            </div>

            <div id="users-list">
            <div class="form-group {{ $errors->has('users[]') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Select Members</label>
              <div class="col-lg-4">
                {!! Form::select('users[]', $users, null, ['ui-jq' => 'chosen', 'class' => 'w-md', 'multiple', 'id' => 'users']) !!}
                <span class="help-block m-b-none">Start typing the member name.</span>
              </div>
                @if ($errors->has('users[]'))
                    <span class="help-block">
                        <strong>{{ $errors->first('users[]') }}</strong>
                    </span>
                @endif
            </div>
            </div>

            <div class="line line-dashed b-b line-lg pull-in"></div>
            <h3>Notifications</h3>
              <div class="form-group">
              <label class="col-lg-2 control-label"></label>
              <div class="col-lg-4">
              <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="via" value="none" checked>
                    <i></i>
                    None
                  </label>
                </div>
              <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="via" value="all">
                    <i></i>
                    All
                  </label>
                </div>
                
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="via" value="sms" >
                    <i></i>
                    SMS only
                  </label>
                </div>

                 <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="via" value="email" >
                    <i></i>
                    Email only
                  </label>
                </div>
                
              </div>
            </div>
            
            <div class="form-group btn-group-justified">
              <div class="col-lg-offset-4 col-lg-4">
                <button id="submit" class="btn m-b-xs btn-md btn-info btn-addon center-block"><i class="fa fa-check"></i> Save</button>
                <input type="submit" name="" style="display: none;" id="theConfirm">
              </div>
            </div>
          {!! Form::close() !!}
        </div>
  </div>
</div>

<!-- set up the modal to start hidden and fade in and out -->
<div id="myModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- dialog body -->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4>Are you sure you want to save this event without members?</h4>
      </div>
      <!-- dialog buttons -->
      <div class="modal-footer" style="text-align: inherit;"><button type="submit" id="confirm" class="btn btn-primary">Yes I'm sure</button>
      <button type="button"  data-dismiss="modal" class="btn btn-primary pull-right">No, add members</button></div>
    </div>
  </div>
</div>
{!! Form::close() !!}

@endsection

@section('script')
<script src="{{ asset('admin/js/summernote.min.js') }}"></script>
    <script>
    $(document).ready(function() {
      $('.summernote').summernote({
        placeholder: 'Description',
        tabsize: 2,
        height: 300
      });
    });
    </script>
    <script src="{{ asset('admin/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('admin/js/moment.min.js') }}"></script>
    <script src="{{ asset('admin/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('admin/libs/jquery/chosen/chosen.jquery.min.js') }}"></script>
   <script type="text/javascript">

      $(function () {
        $.fn.datepicker.defaults.format = "yyyy-mm-dd";
        var currentDate = new Date();
          $("#datepicker").datepicker("setDate" , '{{ $event->date }}');

          $(".w-md").chosen();

        
        var currentTime = new Date('{{ $event->date }} {{ $event->time }}');
          $('#datetimepicker3').datetimepicker({
              format: 'LT',
              defaultDate: currentTime 
          });


          {{--  Invite  --}}
          $('form input').on('change', function() {
            if($('input[name=invite]:checked', 'form').val() == 'all')
            {
              $('#users-list').css('display', 'none');
              
            } 
            if($('input[name=invite]:checked', 'form').val() == 'custom')
            {
              $('#users-list').css('display', '');
            } 
          });

      });

      $('#submit').click(function(e){
        var users = $("#users").val();
        var value = $("input[name='invite']:checked").val();
        
          if(value == 'custom' && users == null)
          {
            e.preventDefault();

            $("#myModal").on("show", function() {    // wire up the OK button to dismiss the modal when shown
              $("#myModal button.exit").on("click", function(e) {
                  $("#myModal").modal('hide');     // dismiss the dialog
              });
          });
          $("#myModal").on("hide", function() {    // remove the event listeners when the dialog is dismissed
        $("#myModal a.btn").off("click");
    });
    
    $("#myModal").on("hidden", function() {  // remove the actual elements from the DOM when fully hidden
        $("#myModal").remove();
    });
    
    $("#myModal").modal({                    // wire up the actual modal functionality and show the dialog
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true                     // ensure the modal is shown immediately
    });

          }
      });

      $('#confirm').click(function(){
        $('#theConfirm').click();
      });
  </script>
  
@endsection