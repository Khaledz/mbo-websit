@extends('layouts.admin')

@section('title', 'List event')

@section('content')
<div class="wrapper-md">
@include('errors.admin_form')
    <a href="{{ route('admin.event.create') }}"><button class="btn m-b-xs btn-sm btn-primary btn-addon center-block"><i class="fa fa-plus"></i>Create new event</button></a>
    <br>
    
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
          <br>
          @if(count($events) == 0)
            <div class="alert alert-warning">There is no event at this moment</div>
          @else
          <table class="table table-striped m-b-none">
          <thead>
            <tr>
              <th>#</th>
              <th>Category</th>
              <th>Name</th>
              <th>Date - Time</th> 
              <th>Members</th>                   
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($events as $event)
            <tr>                    
              <td>#{{ $event->id }}</td>
              <td>{{ $event->category->name }}</td>
              <td>{{ $event->name }}</td>
              <td>{{ $event->date }} - {{ $event->time }}</td>
              <td>({{ $event->users->count() }}) {{ str_plural('member', $event->users->count()) }}</td>
              <td>
                  <a href="{{ route('admin.event.show', $event) }}" class="btn btn-info btn-xs">View</a> -
                  <a href="{{ route('admin.event.edit', $event) }}" class="btn btn-info btn-xs">Update</a> - 
                  {!! Form::open(['method' => 'DELETE', 'route' => ['admin.event.destroy', $event] ,'style' => 'display:inline']) !!}
                      {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                  {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        <br>
        <br>
        <center>{{ $events->links('vendor.pagination.default') }}</center>
        @endif
      </div>
  </div>
</div>

@endsection