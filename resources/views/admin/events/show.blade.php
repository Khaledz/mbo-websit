@extends('layouts.admin')

@section('title', $event->name .' event')
@section('content')
<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
          <div class="pull-right">Date on {{ $event->date }} at {{ $event->time }}</div>
        </div>
        <div class="panel-body">
        <h5 class="text-left">{!! html_entity_decode($event->description) !!}</h5>
        </div>
  </div>
  <div class="row">
    <div class="col-md-4">
      <div class="panel panel-default">
            <div class="panel-heading font-bold">                  
              Invitation
            </div>
            <div class="panel-body">
             @if($users->count() == 0)
                No data
              @else
              <div class="col-md-12">
                <div class="list-group list-group-lg list-group-sp">
                @foreach($users as $user)
                
                  <a herf="" class="list-group-item clearfix">
                  <span class="clear">
                    <span>{{ $user->fname }} {{ $user->lname }}</span>
                    @if($user->answered == 0)
                    <small class="text-right pull-right" style="color: blue;">Pending</small>
                    @elseif($user->status == 0 && $user->answered == 1)
                    <small class="text-right pull-right" style="color: red;">NOT Attend</small>
                    @elseif($user->status == 1 && $user->answered == 1)
                      <small class="text-right pull-right" style="color: green;">Will Attend</small>
                    @endif
                    </small>
                    {{ ($user->note) ?: '' }}
                  </span>
                </a>
                @endforeach
                </div>
              </div>
              @endif
            </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="panel panel-default">
            <div class="panel-heading font-bold">                  
              Program
            </div>
            <div class="panel-body">
           
            {!! html_entity_decode($event->program) !!}
            
            </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="panel panel-default">
            <div class="panel-heading font-bold">                  
              Speaker
            </div>
            <div class="panel-body">
              {!! html_entity_decode($event->speaker) !!}
            </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
            <div class="panel-heading font-bold">                  
              Location
            </div>
            <div class="panel-body">
            {!! $event->location !!}
            </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
            <div class="panel-heading font-bold">                  
              Attachments
            </div>
            <div class="panel-body">
            @if(! is_null($event->galleries()->first()))
              @if($event->galleries()->first()->attachments()->count() == 0)
                -
              @else
              <ul style="list-style: none;">
              @foreach($event->galleries()->first()->attachments()->get() as $attachment)
                <li><a href="{{ asset('storage/galleries/'.$event->galleries()->first()->id. '/'.$attachment->name) }}">{{ $attachment->orginal_name }}</a></li>
              @endforeach
              </ul>
              @endif
            </div>
          @endif
      </div>
    </div>
  </div>

  {{--  more data  --}}

@endsection