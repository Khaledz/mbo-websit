@extends('layouts.admin')

@section('title', 'Dashboard')

@section('content')

<!-- main -->
<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">

  <!-- stats -->
  <div class="row">
    <div class="col-md-12">
      <div class="row row-sm text-center">
        <div class="col-xs-3">
          <a class="block panel padder-v bg-info item">
            <div class="text-white font-thin h1 block">{{ $admin }}</div>
            <span class="text-muted text-xs">Admin</span>
          </a>
        </div>
        <div class="col-xs-3">
          <a href class="block panel padder-v bg-primary item">
            <span class="text-white font-thin h1 block">{{ $champion }}</span>
            <span class="text-muted text-xs">Champions</span>
          </a>
        </div>
        <div class="col-xs-3">
          <a href class="block panel padder-v bg-danger item">
            <span class="text-white font-thin h1 block">{{ $membership }}</span>
            <span class="text-muted text-xs">Memberships</span>
          </a>
        </div>
        <div class="col-xs-3">
          <a href class="block panel padder-v bg-dark item">
            <span class="text-white font-thin h1 block">{{ $clubs }}</span>
            <span class="text-muted text-xs">Multaqa</span>
          </a>
        </div>
        <div class="col-xs-3">
          <div class="panel padder-v item">
            <div class="h1 text-info font-thin h1">{{ $marketPlaces }}</div>
            <span class="text-muted text-xs">Market Places</span>
          </div>
        </div>
        <div class="row row-sm text-center">
            <div class="col-xs-3">
              <div class="panel padder-v item">
                <div class="h1 text-primary font-thin h1">{{ $event }}</div>
                <span class="text-muted text-xs">Events</span>
              </div>
            </div>
            <div class="row row-sm text-center">
                <div class="col-xs-3">
                  <div class="panel padder-v item">
                    <div class="h1 text-danger font-thin h1">{{ $post }}</div>
                    <span class="text-muted text-xs">Posts</span>
                  </div>
                </div>
                <div class="row row-sm text-center">
                    <div class="col-xs-3">
                      <div class="panel padder-v item">
                        <div class="h1 text-dark font-thin h1">{{ $files }}</div>
                        <span class="text-muted text-xs">Files</span>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
     </div>
    <!-- / col-md-12 -->
</div>
  <div class="row">
    <!-- start new users -->
    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading">
          Latest Users
          <a class="pull-right btn btn-default btn-xs" href="{{ route('admin.user.index') }}"><i class="fa fa-bars"></i> List More</a>
        </div>
        <ul class="list-group alt">
        @if($users->count() == 0)
          <div class="text-center">There is no users at this moment.</div>
        @else
        @foreach($users as $user)
          <li class="list-group-item">
            <div class="media">
              <span class="pull-left thumb-sm"><img src="{{ $user->avatar()}}" alt="..." class="img-circle"></span>
              <div class="pull-right text-success m-t-sm">
              {{ ($user->status == 0) ? 'Inactive' : 'Activated'}}
              </div>
              <div class="media-body">
                <div><a href>{{ $user->fname }} {{ $user->lname }}</a></div>
                <small class="text-muted">{{ ($user->created_at) ?? '' }}</small>
              </div>
            </div>
          </li>
          @endforeach
        @endif
        </ul>
      </div>
    </div>
    <!-- end new users. -->
    <!-- start new Attendance -->
    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading">
          Multaqa Membership Requests
          <a class="pull-right btn btn-default btn-xs" href="{{ route('admin.club.index') }}"><i class="fa fa-bars"></i> List More</a>
        </div>
        <ul class="list-group alt">
        @if($pendingMemberships->count() == 0)
          <div class="text-center">There is no pending memberships at this moment.</div>
        @else
        @foreach($pendingMemberships as $club)
        @foreach($club->members as $user)
          <li class="list-group-item">
            <div class="media">
              <span class="pull-left thumb-sm"><img src="{{ $club->user->avatar()}}" alt="..." class="img-circle"></span>
              <div class="pull-right text-success m-t-sm">
              {{ ($user->status == 0) ? 'Inactive' : 'Activated'}}
              </div>
              <div class="media-body">
                <div><a href>{{ $user->fname }} {{ $user->lname }}</a></div>
                <small class="text-muted">{{ $user->created_at->diffForHumans() }}</small>
              </div>
            </div>
          </li>
          @endforeach
          @endforeach
        @endif
        </ul>
      </div>
    </div>
    <!-- start end CMS articles. -->
    </div>

    <div class="row">

    <!-- start new Attendance -->
    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading">
          Latest Posts
          <a class="pull-right btn btn-default btn-xs" href=""><i class="fa fa-bars"></i> List More</a>
        </div>
        <ul class="list-group alt">
        @if($posts->count() == 0)
          <div class="text-center">There is no posts at this moment.</div>
        @else
        @foreach($posts as $post)
          <li class="list-group-item">
            <div class="media">
              <div class="pull-right text-success m-t-sm">
              <a href="{{ route('admin.club.show', [$post->club]) }}">{{ $post->club->name }}</a>
              </div>
              <div class="media-body">
                <div><a href>{{ $post->content }}</a></div>
                <small class="text-muted">{{ $user->created_at->diffForHumans() }}</small>
              </div>
            </div>
          </li>
          @endforeach
        @endif
        </ul>
      </div>
    </div>
    <!-- start end CMS articles. -->

    <!-- start new Attendance -->
    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading">
          Market Places
          <a class="pull-right btn btn-default btn-xs" href=""><i class="fa fa-bars"></i> List More</a>
        </div>
        <ul class="list-group alt">
        @if($pendingMarketPlaces->count() == 0)
          <div class="text-center">There is no posts at this moment.</div>
        @else
        @foreach($pendingMarketPlaces as $market)
          <li class="list-group-item">
            <div class="media">
              <div class="pull-right text-success m-t-sm">
              {{ ($market->status == 0) ? 'Inactive' : 'Activated'}}
              </div>
              <div class="media-body">
                <div><a href="{{ route('admin.market.show', [$market]) }}">{{ $market->name }}</a></div>
                <small class="text-muted">{{ $market->created_at->diffForHumans() }}</small>
              </div>
            </div>
          </li>
          @endforeach
        @endif
        </ul>
      </div>
    </div>
    <!-- start end CMS articles. -->
</div>
<!-- / main -->
</div>
@endsection

