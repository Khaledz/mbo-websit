@extends('layouts.admin')

@section('title', $club->name)
@section('content')
<div class="wrapper-md">
  <div class="row">
    <div class="col-md-9">
      <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
        <h5 class="text-left">{!! html_entity_decode($club->description) !!}</h5>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          Multaqa Information
        </div>
        <div class="panel-body">
        <div class="text-block text-center">
          <h4 class="text-center">Champion</h4>
          @if($club->champions->count() > 0)
              <img alt="avatar" src="{{ $club->champions->first()->avatar() }}" width="110" height="110">
              <br>
              <br>
              <span class="h5">{{ $club->champions->first()->fullName }}</span>
          @else
              -
          @endif
          </div>
          <hr>
          <div class="text-block text-center">
              <h4 class="text-center">Launched</h4>
              <span class="h5">{{ $club->launched }}</span>
          </div>
          <hr>
              <div class="text-block text-center">
                  <h4 class="text-center">Goals</h4>
                  <span class="h5">{!! html_entity_decode($club->goals) !!}</span>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
            <div class="panel-heading font-bold">                  
              Posts
            </div>
            <div class="panel-body">
              @if(count($club->posts) == 0)
                <div class="alert alert-warning">There is no posts at this moment</div>
              @else
              <table class="table table-striped m-b-none">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Title</th>
                  <th>Content</th>
                  <th>Posted By</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($club->posts as $post)
                <tr>                    
                  <td>#{{ $post->id }}</td>
                  <td>{{ $post->title }}</td>
                  <td>{{ substr($post->content, 0, 20) }}</td>
                  <td>{{ $post->user->fullName }}</td>
                  <td>
                      <a href="{{ route('admin.post.show', $post) }}" class="btn btn-info btn-xs">View</a> -
                      <a href="{{ route('admin.post.edit', $post) }}" class="btn btn-info btn-xs">Update</a> - 
                      {!! Form::open(['method' => 'DELETE', 'route' => ['admin.post.destroy', $post] ,'style' => 'display:inline']) !!}
                          {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                      {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
              </tbody>
            </table>
            @endif
            </div>
      </div>  
    </div>
  </div>
  </div>

</div>


  
  
@endsection