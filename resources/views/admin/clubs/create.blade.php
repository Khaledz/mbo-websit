@extends('layouts.admin')
@section('header')
  <link href="{{ asset('admin/css/summernote.css') }}" rel="stylesheet" type="text/css" media="all" />
@endsection
@section('title', 'Create new multaqa')
@section('style')
  <link rel="stylesheet" href="{{ asset('admin/libs/jquery/chosen/bootstrap-chosen.css') }}">
@endsection
@section('content')
<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
             @include('errors.admin_form')
            {!! Form::open(['route' => ['admin.club.store'], 'class' => 'bs-example form-horizontal']) !!}

            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Multaqa Name</label>
              <div class="col-lg-4">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Multaqa Name']) !!}
              </div>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Multaqa Description</label>
              <div class="col-lg-10">
              {!! Form::textarea('description', null, ['rows' => '4', 'class' => 'form-control summernote', 'id' => 'summernote', 'placeholder' => 'event Description']) !!}
              </div>
                @if ($errors->has('description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('goals') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Multaqa Goals (optional)</label>
              <div class="col-lg-10">
              {!! Form::textarea('goals', null, ['rows' => '4', 'class' => 'form-control summernote', 'id' => 'summernote', 'placeholder' => 'Multaqa Goals']) !!}
              </div>
                @if ($errors->has('goals'))
                    <span class="help-block">
                        <strong>{{ $errors->first('goals') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('launched') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Launched</label>
              <div class="col-lg-4">
                {!! Form::text('launched', null, ['class' => 'form-control', 'placeholder' => 'launched']) !!}
              </div>
                @if ($errors->has('launched'))
                    <span class="help-block">
                        <strong>{{ $errors->first('launched') }}</strong>
                    </span>
                @endif
            </div>
</div>
  </div>
</div>

<div class="wrapper-md">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
            <div class="panel-heading font-bold">                  
              Members
            </div>
            <div class="panel-body">
              <div class="form-group {{ $errors->has('members[]') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label text-right">Members</label>
              <div class="col-lg-4">
                {!! Form::select('members[]', $users, null, ['ui-jq' => 'chosen', 'class' => 'w-md', 'multiple']) !!}
                <span class="help-block m-b-none">Start typing the member name.</span>
              </div>
                @if ($errors->has('members[]'))
                    <span class="help-block">
                        <strong>{{ $errors->first('members[]') }}</strong>
                    </span>
                @endif
            </div>
            </div>
      </div>
    </div>
  </div>
</div>

<div class="wrapper-md">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
            <div class="panel-heading font-bold">                  
              Champion
            </div>
            <div class="panel-body">
              <div class="form-group {{ $errors->has('champions[]') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label text-right">Select champion for this Multaqa</label>
              <div class="col-lg-4">
                {!! Form::select('champions[]', $users, null, ['ui-jq' => 'chosen', 'class' => 'w-md', 'multiple']) !!}
                <span class="help-block m-b-none">Start typing the member name.</span>
              </div>
                @if ($errors->has('champions[]'))
                    <span class="help-block">
                        <strong>{{ $errors->first('champions[]') }}</strong>
                    </span>
                @endif
            </div>
            </div>
            <div class="form-group btn-group-justified">
              <div class="col-lg-offset-4 col-lg-4">
                <button class="btn m-b-xs btn-md btn-info btn-addon center-block"><i class="fa fa-check"></i> Save</button>
              </div>
            </div>
      </div>
    </div>
  </div>
</div>


          {!! Form::close() !!}
@endsection

@section('script')
  <script src="{{ asset('admin/libs/jquery/chosen/chosen.jquery.min.js') }}"></script>
  <script src="{{ asset('admin/js/summernote.min.js') }}"></script>
    <script>
    $(document).ready(function() {
      $('.summernote').summernote({
        placeholder: 'Description',
        tabsize: 2,
        height: 300
      });
    });
    </script>
@endsection