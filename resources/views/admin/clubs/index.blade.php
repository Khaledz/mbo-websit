@extends('layouts.admin')

@section('title', 'List multaqa')

@section('content')
<div class="wrapper-md">
@include('errors.admin_form')
    <a href="{{ route('admin.club.create') }}"><button class="btn m-b-xs btn-sm btn-primary btn-addon center-block"><i class="fa fa-plus"></i>Create new multaqa</button></a>
    <br>
    
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
          <br>
          @if(count($clubs) == 0)
            <div class="alert alert-warning">There is no multaqa at this moment</div>
          @else
          <table class="table table-striped m-b-none">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Launched</th>
              <th>Champion</th>
              <th>Members</th>                
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($clubs as $club)
            <tr>                 
              <td>#{{ $club->id }}</td>
              <td>{{ $club->name }}</td>
              <td>{{ $club->launched }}</td>
              <td>{{ $club->champions->first()->fullName }}</td>
              <td>({{ $club->members->count() }}) {{ str_plural('member', $club->members->count()) }}</td>
              <td>
                  <a href="{{ route('admin.club.pending', $club) }}" class="btn btn-info btn-xs">({{ $club->members()->where('club_members.status', 0)->count() }}) Pending</a> -
                  <a href="{{ route('admin.club.show', $club) }}" class="btn btn-info btn-xs">View</a> -
                  <a href="{{ route('admin.club.edit', $club) }}" class="btn btn-info btn-xs">Update</a> - 
                  {!! Form::open(['method' => 'DELETE', 'route' => ['admin.club.destroy', $club] ,'style' => 'display:inline']) !!}
                      {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                  {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        <br>
        <br>
        <center>{{ $clubs->links('vendor.pagination.default') }}</center>
        @endif
      </div>
  </div>
</div>

@endsection