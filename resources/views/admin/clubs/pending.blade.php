@extends('layouts.admin')
@section('header')
  <link href="{{ asset('admin/libs/jquery/chosen/bootstrap-chosen.css') }}" rel="stylesheet" type="text/css" media="all" />
@endsection
@section('title', 'Pending memberships')
@section('content')
<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
         @include('errors.admin_form')
        {!! Form::open(['route' => ['admin.club.pending', $club], 'method' => 'post', 'class' => 'bs-example form-horizontal']) !!}
            
            <div class="form-group {{ $errors->has('pendings') ? 'has-error' : ''}}">
              
              @if($pendings->count() == 0)
                    <div class="col-lg-12">
                    <h4 class="text-center">There is no pending membership for this multaqa.</h4>
                    </div>
              @else
              
              <div class="col-lg-12">
                    <h4 class="text-center">Please select the users that you want to join this Multaqa.</h4>
                    </div>
              <div class="col-lg-4">
                @foreach($pendings as $pending)
                    <div class="checkbox">
                    <label class="i-checks">
                    <input type="checkbox" value="{{ $pending->id }}" name="pendings[]">
                    <i></i>
                    <img src="{{ $pending->avatar() }}" width="60" height"60"> {{ $pending->fullName }}
                    </div>
                @endforeach
             
              </label>
            </div>
              </div>
                @if ($errors->has('pendings'))
                    <span class="help-block">
                        <strong>{{ $errors->first('pendings') }}</strong>
                    </span>
                @endif
            </div>

            
            <div class="form-group btn-group-justified">
              <div class="col-lg-offset-4 col-lg-4">
                <button class="btn m-b-xs btn-md btn-info btn-addon center-block"><i class="fa fa-check"></i> Save</button>
              </div>
            </div>
            @endif
      </div>
    </div>
  </div>
</div>
@endsection

