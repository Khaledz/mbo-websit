@extends('layouts.admin')

@section('title', 'Edit a market place')
@section('content')

<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
        {!! Form::model($market, ['method' => 'PUT', 'route' => ['admin.market.update', $market], 'method' => 'PUT', 'class' => 'bs-example form-horizontal', 'files' => true]) !!}
           <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Market Name</label>
              <div class="col-lg-4">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Market Name']) !!}
              </div>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Market Description</label>
              <div class="col-lg-4">
                {!! Form::textarea('description', null, ['rows' => '4', 'class' => 'form-control', 'placeholder' => 'Market Description']) !!}
              </div>
                @if ($errors->has('description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
              <label class="col-lg-4 control-label">logo</label>
              <div class="col-lg-4">
                <input type="file" name="logo">
              </div>
              
              @if(! is_null($market->logo))
              <div class="col-lg-4">
              <ul>
                <li>
                  <a href="{{ asset('storage/market_places/logos/' . $market->id .'/'. $market->logo) }}"/>{{ $market->logo }}</a>
                </li>
              </ul>
              </div>
              @endif
            </div>

            <div class="form-group">
              <label class="col-lg-4 control-label">Files (optional)</label>
              <div class="col-lg-4">
                <input type="file" name="files[]" multiple="">
                <span class="help-block m-b-none">Press and hold ctrl to select multiple files.</span>
              </div>
              @if($market->attachments->count() > 0)
              <div class="col-lg-4">
              <ul>
                @foreach($market->attachments as $attachment)
                <li>
                  <a href="{{ asset('storage/market_places/' . $market->id .'/'. $attachment->name) }}"/>{{ $attachment->orginal_name }}</a>
                   - <a href="{{ route('admin.attachment.destroy', [$market, $attachment]) }}" style="color: red;"> Remove</a>
                </li>
                @endforeach
              </ul>
              </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('description_of_file') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Description of File (optional)</label>
              <div class="col-lg-4">
                {!! Form::text('description_of_file', (is_null($market->attachments->first())) ? '' : $market->attachments->first()->description, ['class' => 'form-control', 'placeholder' => 'Description of File']) !!}
              </div>
                @if ($errors->has('description_of_file'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description_of_file') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Status</label>
              <div class="col-lg-4">
                {!! Form::select('status', ['0' => 'Inactive', '1' => 'Active'], $market->status, ['class' => 'selectpicker form-control', 'data-live-search' => true]) !!}
              </div>
               @if ($errors->has('status'))
                    <span class="help-block">
                        <strong>{{ $errors->first('status') }}</strong>
                    </span>
                @endif
            </div>
            
            <div class="form-group btn-group-justified">
              <div class="col-lg-offset-4 col-lg-4">
                <button class="btn m-b-xs btn-md btn-info btn-addon center-block"><i class="fa fa-check"></i> Save</button>
              </div>
            </div>
          {!! Form::close() !!}
        </div>
  </div>
</div>

@endsection