@extends('layouts.admin')

@section('title', $market->name)
@section('content')
<div class="wrapper-md">
  <div class="row">
    <div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
        <h5 class="text-left">{!! html_entity_decode($market->description) !!}</h5>
        </div>
    </div>
  </div>
   <div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          Attachments
        </div>
        <div class="panel-body">
        @if($market->attachments()->count() == 0)
          No data
        @else
        <ul style="list-style: none;">
        @foreach($market->attachments()->get() as $attachment)
        @if($attachment->name == $market->logo)
          @continue
        @else
          <li><a href="{{ asset('storage/market_places/'.$market->id. '/'.$attachment->name) }}">{{ $attachment->orginal_name }}</a></li>
        @endif
        @endforeach
        </ul>
        @endif
        </div>
    </div>
  
  </div>
</div>

@endsection