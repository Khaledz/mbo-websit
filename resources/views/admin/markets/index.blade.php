@extends('layouts.admin')

@section('title', 'List markets')

@section('content')
<div class="wrapper-md">
@include('errors.admin_form')
    <a href="{{ route('admin.market.create') }}"><button class="btn m-b-xs btn-sm btn-primary btn-addon center-block"><i class="fa fa-plus"></i>Create new market</button></a>
    <br>
    
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
          <br>
          @if(count($markets) == 0)
            <div class="alert alert-warning">There is no markets at this moment</div>
          @else
          <table class="table table-striped m-b-none">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Description</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($markets as $market)
            <tr>                    
              <td>#{{ $market->id }}</td>
              <td>{{ $market->name }}</td>
              <td>{{ substr($market->description, 0 ,20) }} ..</td>
              <td>{{ ($market->status == 0) ? 'Inactive' : 'Active' }}</td>
              <td>
                  <a href="{{ route('admin.market.show', $market) }}" class="btn btn-info btn-xs">View</a> -
                  <a href="{{ route('admin.market.edit', $market) }}" class="btn btn-info btn-xs">Update</a> - 
                  {!! Form::open(['method' => 'DELETE', 'route' => ['admin.market.destroy', $market] ,'style' => 'display:inline']) !!}
                      {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                  {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        <br>
        <br>
        <center>{{ $markets->links('vendor.pagination.default') }}</center>
        @endif
      </div>
  </div>
</div>

@endsection