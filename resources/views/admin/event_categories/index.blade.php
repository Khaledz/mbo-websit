@extends('layouts.admin')

@section('title', 'List event categories')

@section('content')
<div class="wrapper-md">
@include('errors.admin_form')
    <a href="{{ route('admin.category.create') }}"><button class="btn m-b-xs btn-sm btn-primary btn-addon center-block"><i class="fa fa-plus"></i>Create new event category</button></a>
    <br>
    
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
          <br>
          @if(count($categories) == 0)
            <div class="alert alert-warning">There is no categories at this moment</div>
          @else
          <table class="table table-striped m-b-none">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Events</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($categories as $category)
            <tr>                    
              <td>#{{ $category->id }}</td>
              <td>{{ $category->name }}</td>
              <td>({{ $category->events->count() }}) {{ str_plural('event', $category->events->count()) }}</td>
              <td>
                  <a href="{{ route('admin.category.show', $category) }}" class="btn btn-info btn-xs">View</a> -
                  <a href="{{ route('admin.category.edit', $category) }}" class="btn btn-info btn-xs">Update</a> - 
                  {!! Form::open(['method' => 'DELETE', 'route' => ['admin.category.destroy', $category] ,'style' => 'display:inline']) !!}
                      {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                  {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        <br>
        <br>
        <center>{{ $categories->links('vendor.pagination.default') }}</center>
        @endif
      </div>
  </div>
</div>

@endsection