@extends('layouts.admin')

@section('title', $category->name)
@section('content')
<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
          <div class="pull-right">Contains: {{ $category->events->count() }} {{ str_plural('event', $category->events->count()) }}</div>
        </div>
        <div class="panel-body">
        
          @if($category->events()->count() == 0)
              No events available in this category
            @else
            <div class="col-md-12">
              <ul style="list-style-type: none;">
              @foreach($category->events as $event)
                <li class="text-left">{{ $event->name }}</li>
              @endforeach
              </ul>
            </div>
            @endif

        </div>
  </div>
 
@endsection