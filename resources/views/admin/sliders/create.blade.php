@extends('layouts.admin')

@section('title', 'Create new slider')
@section('content')
<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
             @include('errors.admin_form')
            {!! Form::open(['route' => ['admin.slider.store'], 'class' => 'bs-example form-horizontal', 'enctype' => 'multipart/form-data']) !!}

            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Slider Name</label>
              <div class="col-lg-4">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Slider Title']) !!}
              </div>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Status</label>
              <div class="col-lg-4">
                {!! Form::select('status', [1 => 'Active', 0 => 'Inactive'], null, ['class' => 'form-control']) !!}
              </div>
                @if ($errors->has('status'))
                    <span class="help-block">
                        <strong>{{ $errors->first('status') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
              <label class="col-lg-4 control-label">Slider</label>
              <div class="col-lg-4">
                <input type="file" name="file">
                <br>
                <span>Max slide size: {{ ini_get('upload_max_filesize') }} <br> Slider size (width: > 500px, height: 450px;)</span>
              </div>
            </div>

            <div class="form-group btn-group-justified">
              <div class="col-lg-offset-4 col-lg-4">
                <button class="btn m-b-xs btn-md btn-info btn-addon center-block"><i class="fa fa-check"></i> Save</button>
              </div>
            </div>
          {!! Form::close() !!}
        </div>
  </div>
</div>

@endsection

