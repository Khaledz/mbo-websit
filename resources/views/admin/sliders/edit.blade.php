@extends('layouts.admin')

@section('title', 'Edit a event')
@section('content')

<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
        {!! Form::model($slider, ['method' => 'PUT', 'route' => ['admin.slider.update', $slider], 'method' => 'PUT', 'class' => 'bs-example form-horizontal', 'files' => true]) !!}
           <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Slider Name</label>
              <div class="col-lg-4">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Slider Title']) !!}
              </div>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Status</label>
              <div class="col-lg-4">
                {!! Form::select('status', [1 => 'Active', 0 => 'Inactive'], null, ['class' => 'form-control']) !!}
              </div>
                @if ($errors->has('status'))
                    <span class="help-block">
                        <strong>{{ $errors->first('status') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
              <label class="col-lg-4 control-label">Slider</label>
              @if($slider->attachments->count() > 0)
                <img src="{{ asset('storage/sliders/' . $slider->id .'/'. $slider->attachments->first()->name) }}" width="100" height="50" />
                <a href="{{ route('admin.attachment.destroy', [$slider, $slider->attachments->first()]) }}">Remove</a>
              @endif
              <div class="col-lg-4">
                <input type="file" name="file">
              </div>
            </div>
            
            <div class="form-group btn-group-justified">
              <div class="col-lg-offset-4 col-lg-4">
                <button class="btn m-b-xs btn-md btn-info btn-addon center-block"><i class="fa fa-check"></i> Save</button>
              </div>
            </div>
          {!! Form::close() !!}
        </div>
  </div>
</div>

@endsection