@extends('layouts.admin')

@section('title', $market->name)
@section('content')
<div class="wrapper-md">
  <div class="row">
    <div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
        <h5 class="text-left">{{ $market->description }}</h5>
        </div>
    </div>
  </div>
   <div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          Attachments
        </div>
        <div class="panel-body">
        @if($market->attachments()->count() == 0)
          No data
        @else
        <ul style="list-style: none;">
        @foreach($market->attachments()->get() as $attachment)
          <li><a href="{{ asset('storage/markets/'.$market->id. '/'.$attachment->name) }}">{{ $attachment->orginal_name }}</a></li>
        @endforeach
        </ul>
        @endif
        </div>
    </div>
  
  </div>
</div>

@endsection