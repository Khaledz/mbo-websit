@extends('layouts.admin')

@section('title', 'List sliders')

@section('content')
<div class="wrapper-md">
@include('errors.admin_form')
    <a href="{{ route('admin.slider.create') }}"><button class="btn m-b-xs btn-sm btn-primary btn-addon center-block"><i class="fa fa-plus"></i>Create new slider</button></a>
    <br>
    
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
          <br>
          @if(count($sliders) == 0)
            <div class="alert alert-warning">There is no sliders at this moment</div>
          @else
          <table class="table table-striped m-b-none">
          <thead>
            <tr>
              <th>#</th>
              <th>Image</th>
              <th>Name</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($sliders as $slider)
            <tr>                    
              <td>#{{ $slider->id }}</td>
              @if($slider->attachments->count() == 0)
              <td>-</td>
              @else
              <td><img src="{{ asset('storage/sliders/' . $slider->id .'/'. $slider->attachments->first()->name) }}" width="100" height="50" /></td>
              @endif
              <td>{{ $slider->name }}</td>
              <td>{{ ($slider->status == 0) ? 'Inactive' : 'Active' }}</td>
              <td>
                  <a href="{{ route('admin.slider.edit', $slider) }}" class="btn btn-info btn-xs">Update</a> - 
                  {!! Form::open(['method' => 'DELETE', 'route' => ['admin.slider.destroy', $slider] ,'style' => 'display:inline']) !!}
                      {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                  {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        <br>
        <br>
        <center>{{ $sliders->links('vendor.pagination.default') }}</center>
        @endif
      </div>
  </div>
</div>

@endsection