@extends('layouts.admin')

@section('title', 'List users')

@section('content')

<link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" type="text/css" />
<div class="wrapper-md">
@include('errors.admin_form')
    <a href="{{ route('admin.user.create') }}"><button class="btn m-b-xs btn-sm btn-primary btn-addon center-block"><i class="fa fa-plus"></i>Create new user</button></a>
    <br>
    
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
          <ul class="nav nav-pills nav-sm">
            <li class="{{ (Request::segment(4) == '') ? 'active' : ''}}"><a href="{{ route('admin.user.index') }}">All</a></li>
          @foreach($roles as $role)
            <li class="{{ (Request::segment(4) == $role->name) ? 'active' : ''}}"><a href="{{ route('admin.user.filter', ['role' => $role->name]) }}">{{ ucfirst($role->name) }}</a></li>
          @endforeach
          </ul>
          <br>
          @if(count($users) == 0)
            <div class="alert alert-warning">There is no students at this moment</div>
          @else
          <div class="table-responsive">
          <table class="table table-striped b-t b-b"  ui-jq="dataTable">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>                    
              <th>Username</th>
              <th>Email</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($users as $user)
            <tr>                    
              <td>#{{ $user->id }}</td>
              <td><a href="{{ route('app.user.show', $user) }}" class="text-info">{{ $user->fname }} {{ $user->lname }}</a></td>
              <td>{{ $user->username }}</td>
              <td>{{ $user->email }}</td>
              <td>{{ ($user->status == 1) ? 'Active' : 'Inactive' }}</td>
              <td>
                  <a href="{{ route('admin.user.edit', $user) }}" class="btn btn-info btn-xs">Update</a> - 
                  {!! Form::open(['method' => 'DELETE', 'route' => ['admin.user.destroy', $user] ,'style' => 'display:inline']) !!}
                      {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                  {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        </div>
        <br>
        <br>
        @endif
      </div>
  </div>
</div>

@endsection

@section('script')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function(){
    $('table').DataTable();
});
</script>
@endsection