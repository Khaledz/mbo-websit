@extends('layouts.admin')

@section('title', 'Edit a user')
@section('content')
<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
        {!! Form::model($user, ['method' => 'PATCH', 'route' => ['admin.user.update', $user], 'method' => 'PUT', 'class' => 'bs-example form-horizontal']) !!}
            <div class="form-group {{ $errors->has('role_id') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Role</label>
              <div class="col-lg-4">
                {!! Form::select('role_id', $roles, $user->roles, ['class' => 'selectpicker form-control', 'data-live-search' => true]) !!}
              </div>
               @if ($errors->has('role_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('role_id') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('fname') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">First Name</label>
              <div class="col-lg-4">
                {!! Form::text('fname', null, ['class' => 'form-control', 'placeholder' => 'First Name']) !!}
              </div>
                @if ($errors->has('fname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('fname') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('lname') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Last Name</label>
              <div class="col-lg-4">
                {!! Form::text('lname', null, ['class' => 'form-control', 'placeholder' => 'Last Name']) !!}
              </div>
                @if ($errors->has('lname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('lname') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Email</label>
              <div class="col-lg-4">
                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
              </div>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Phone</label>
              <div class="col-lg-4">
                {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => '+966']) !!}
              </div>
              @if ($errors->has('phone'))
                    <span class="help-block">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('username') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Username</label>
              <div class="col-lg-4">
                {!! Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'Username']) !!}
              </div>
              @if ($errors->has('username'))
                    <span class="help-block">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                <label class="col-lg-4 control-label">Password</label>
                <div class="col-lg-4">
                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
                <span>Leave it blank if you don't want to change it.</span>
                </div>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Active</label>
              <div class="col-lg-4">
                {!! Form::select('status', ['1' => 'Activate', '0' => 'Deactivate'], null, ['class' => 'selectpicker form-control', 'data-live-search' => true]) !!}
              </div>
               @if ($errors->has('status'))
                    <span class="help-block">
                        <strong>{{ $errors->first('status') }}</strong>
                    </span>
                @endif
            </div>
            
        </div>
  </div>
</div>

<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          Extra Inforamtion (optional)
        </div>
        <div class="panel-body bs-example form-horizontal">
            <div class="form-group {{ $errors->has('sector') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Sector</label>
              <div class="col-lg-4">
              {!! Form::text('sector', $user->settings()->sector, ['class' => 'form-control', 'placeholder' => 'Sector']) !!}
              </div>
               @if ($errors->has('sector'))
                    <span class="help-block">
                        <strong>{{ $errors->first('sector') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('job') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Job title</label>
              <div class="col-lg-4">
                {!! Form::text('job', $user->settings()->job, ['class' => 'form-control', 'placeholder' => 'Job title']) !!}
              </div>
                @if ($errors->has('job'))
                    <span class="help-block">
                        <strong>{{ $errors->first('job') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('children') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">number of children</label>
              <div class="col-lg-4">
                {!! Form::text('children', $user->settings()->children, ['class' => 'form-control', 'placeholder' => 'Children']) !!}
              </div>
                @if ($errors->has('children'))
                    <span class="help-block">
                        <strong>{{ $errors->first('children') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('wife') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Number of wife</label>
              <div class="col-lg-4">
                {!! Form::text('wife', $user->settings()->wife, ['class' => 'form-control', 'placeholder' => 'Wife']) !!}
              </div>
                @if ($errors->has('wife'))
                    <span class="help-block">
                        <strong>{{ $errors->first('wife') }}</strong>
                    </span>
                @endif
            </div>
            
            <div class="form-group {{ $errors->has('company') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Company</label>
              <div class="col-lg-4">
                {!! Form::text('company', $user->settings()->company, ['class' => 'form-control', 'placeholder' => 'Company']) !!}
              </div>
              @if ($errors->has('company'))
                    <span class="help-block">
                        <strong>{{ $errors->first('company') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('linkedin') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Linkedin</label>
              <div class="col-lg-4">
                {!! Form::text('linkedin', $user->settings()->linkedin, ['class' => 'form-control', 'placeholder' => 'Linkedin']) !!}
              </div>
              @if ($errors->has('linkedin'))
                    <span class="help-block">
                        <strong>{{ $errors->first('linkedin') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('website') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Website</label>
              <div class="col-lg-4">
                {!! Form::text('website', $user->settings()->website, ['class' => 'form-control', 'placeholder' => 'Website']) !!}
              </div>
                @if ($errors->has('website'))
                    <span class="help-block">
                        <strong>{{ $errors->first('website') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group btn-group-justified">
              <div class="col-lg-offset-4 col-lg-4">
                <button class="btn m-b-xs btn-md btn-info btn-addon center-block"><i class="fa fa-check"></i> Save</button>
              </div>
            </div>
            
        </div>
  </div>
</div>

{!! Form::close() !!}
@endsection