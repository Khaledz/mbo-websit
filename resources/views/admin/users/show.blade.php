@extends('layouts.admin')

@section('title', $user->fullName . "'s profile")
@section('content')
<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        
  </div>
</div>

@endsection