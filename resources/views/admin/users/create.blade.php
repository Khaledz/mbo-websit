@extends('layouts.admin')

@section('title', 'Create new user')
@section('header')
  <link href="{{ asset('admin/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" media="all" />
  <link href="{{ asset('admin/css/summernote.css') }}" rel="stylesheet" type="text/css" media="all" />
@endsection
@section('content')
<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
             @include('errors.admin_form')
            {!! Form::open(['route' => ['admin.user.store'], 'class' => 'bs-example form-horizontal']) !!}
            
            <div class="form-group {{ $errors->has('role_id') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Role</label>
              <div class="col-lg-4">
                {!! Form::select('role_id', $roles, null, ['class' => 'selectpicker form-control', 'data-live-search' => true]) !!}
              </div>
               @if ($errors->has('role_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('role_id') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('fname') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">First Name</label>
              <div class="col-lg-4">
                {!! Form::text('fname', null, ['class' => 'form-control', 'placeholder' => 'First Name']) !!}
              </div>
                @if ($errors->has('fname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('fname') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('lname') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Last Name</label>
              <div class="col-lg-4">
                {!! Form::text('lname', null, ['class' => 'form-control', 'placeholder' => 'Last Name']) !!}
              </div>
                @if ($errors->has('lname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('lname') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Email</label>
              <div class="col-lg-4">
                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
              </div>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            
            <div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Phone</label>
              <div class="col-lg-4">
                {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => '+966']) !!}
              </div>
              @if ($errors->has('phone'))
                    <span class="help-block">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('username') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Username</label>
              <div class="col-lg-4">
                {!! Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'Username']) !!}
              </div>
              @if ($errors->has('username'))
                    <span class="help-block">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Password</label>
              <div class="col-lg-4">
                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
              </div>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Active</label>
              <div class="col-lg-4">
                {!! Form::select('status', ['1' => 'Activate', '0' => 'Deactivate'], null, ['class' => 'selectpicker form-control', 'data-live-search' => true]) !!}
              </div>
               @if ($errors->has('status'))
                    <span class="help-block">
                        <strong>{{ $errors->first('status') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('joined') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">User joind since</label>
              <div class="col-lg-4">
                <div class="input-group date" data-provide="datepicker">
                  <input type="text" class="form-control" name="joined">
                  <div class="input-group-addon">
                      <span class="glyphicon glyphicon-th"></span>
                  </div>
              </div>

              </div>
                @if ($errors->has('joined'))
                    <span class="help-block">
                        <strong>{{ $errors->first('joined') }}</strong>
                    </span>
                @endif
            </div>
            
        </div>
  </div>
</div>

<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          Extra Inforamtion (optional)
        </div>
        <div class="panel-body bs-example form-horizontal">
            <div class="form-group {{ $errors->has('sector') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Sector</label>
              <div class="col-lg-4">
              {!! Form::text('sector', null, ['class' => 'form-control', 'placeholder' => 'Sector']) !!}
              </div>
               @if ($errors->has('sector'))
                    <span class="help-block">
                        <strong>{{ $errors->first('sector') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('job') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Job title</label>
              <div class="col-lg-4">
                {!! Form::text('job', null, ['class' => 'form-control', 'placeholder' => 'Job title']) !!}
              </div>
                @if ($errors->has('job'))
                    <span class="help-block">
                        <strong>{{ $errors->first('job') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('children') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">number of children</label>
              <div class="col-lg-4">
                {!! Form::text('children', null, ['class' => 'form-control', 'placeholder' => 'Children']) !!}
              </div>
                @if ($errors->has('children'))
                    <span class="help-block">
                        <strong>{{ $errors->first('children') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('wife') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Number of wife</label>
              <div class="col-lg-4">
                {!! Form::text('wife', null, ['class' => 'form-control', 'placeholder' => 'Wife']) !!}
              </div>
                @if ($errors->has('wife'))
                    <span class="help-block">
                        <strong>{{ $errors->first('wife') }}</strong>
                    </span>
                @endif
            </div>
            
            <div class="form-group {{ $errors->has('company') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Company</label>
              <div class="col-lg-4">
                {!! Form::text('company', null, ['class' => 'form-control', 'placeholder' => 'Company']) !!}
              </div>
              @if ($errors->has('company'))
                    <span class="help-block">
                        <strong>{{ $errors->first('company') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('linkedin') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Linkedin</label>
              <div class="col-lg-4">
                {!! Form::text('linkedin', null, ['class' => 'form-control', 'placeholder' => 'Linkedin']) !!}
              </div>
              @if ($errors->has('linkedin'))
                    <span class="help-block">
                        <strong>{{ $errors->first('linkedin') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('website') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Website</label>
              <div class="col-lg-4">
                {!! Form::text('website', null, ['class' => 'form-control', 'placeholder' => 'Website']) !!}
              </div>
                @if ($errors->has('website'))
                    <span class="help-block">
                        <strong>{{ $errors->first('website') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group btn-group-justified">
              <div class="col-lg-offset-4 col-lg-4">
                <button class="btn m-b-xs btn-md btn-info btn-addon center-block"><i class="fa fa-check"></i> Save</button>
              </div>
            </div>
            
        </div>
  </div>
</div>

{!! Form::close() !!}

@endsection

@section('script')
<script src="{{ asset('admin/js/summernote.min.js') }}"></script>
    <script>
    $(document).ready(function() {
      $('.summernote').summernote({
        placeholder: 'Description',
        tabsize: 2,
        height: 300
      });
    });
    </script>
    <script src="{{ asset('admin/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('admin/js/moment.min.js') }}"></script>
    <script src="{{ asset('admin/js/bootstrap-datetimepicker.min.js') }}"></script>

   <script type="text/javascript">
      $(function () {
        var time = "{{ session('time') }}";
        var date = "{{ session('date') }}";

          $('.datepicker').datepicker({
            setDate: '12/03/2017'
          });

          $('#datetimepicker3').datetimepicker({
              format: 'LT',
          });
          
          {{--  Invite  --}}
          $('form input').on('change', function() {
            if($('input[name=invite]:checked', 'form').val() == 'all')
            {
              $('#users-list').css('display', 'none');
              
            } 
            if($('input[name=invite]:checked', 'form').val() == 'custom')
            {
              $('#users-list').css('display', '');
            } 
          });

      });

      $('#submit').click(function(e){
        var users = $("#users").val();
        var value = $("input[name='invite']:checked").val();
        
          if(value == 'custom' && users == null)
          {
            e.preventDefault();

            $("#myModal").on("show", function() {    // wire up the OK button to dismiss the modal when shown
              $("#myModal button.exit").on("click", function(e) {
                  $("#myModal").modal('hide');     // dismiss the dialog
              });
          });
          $("#myModal").on("hide", function() {    // remove the event listeners when the dialog is dismissed
        $("#myModal a.btn").off("click");
    });
    
    $("#myModal").on("hidden", function() {  // remove the actual elements from the DOM when fully hidden
        $("#myModal").remove();
    });
    
    $("#myModal").modal({                    // wire up the actual modal functionality and show the dialog
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true                     // ensure the modal is shown immediately
    });

          }
      });

      $('#confirm').click(function(){
        $('#submit').click();
      });
  </script>
  
@endsection