@extends('layouts.admin')

@section('title', 'Edit')
@section('content')

<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
        {!! Form::model($faq, ['method' => 'PUT', 'route' => ['admin.faq.update', $faq], 'method' => 'PUT', 'class' => 'bs-example form-horizontal', 'files' => true]) !!}
           <div class="form-group {{ $errors->has('question') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Question</label>
              <div class="col-lg-4">
                {!! Form::text('question', null, ['class' => 'form-control', 'placeholder' => 'Question']) !!}
              </div>
                @if ($errors->has('question'))
                    <span class="help-block">
                        <strong>{{ $errors->first('question') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('answer') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Answer</label>
              <div class="col-lg-4">
                {!! Form::textarea('answer', null, ['rows' => '4', 'class' => 'form-control', 'placeholder' => 'answer']) !!}
              </div>
                @if ($errors->has('answer'))
                    <span class="help-block">
                        <strong>{{ $errors->first('answer') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group btn-group-justified">
              <div class="col-lg-offset-4 col-lg-4">
                <button class="btn m-b-xs btn-md btn-info btn-addon center-block"><i class="fa fa-check"></i> Save</button>
              </div>
            </div>
          {!! Form::close() !!}
        </div>
  </div>
</div>

@endsection