@extends('layouts.admin')

@section('title', 'FAQ')

@section('content')
<div class="wrapper-md">
@include('errors.admin_form')
    <a href="{{ route('admin.faq.create') }}"><button class="btn m-b-xs btn-sm btn-primary btn-addon center-block"><i class="fa fa-plus"></i>Create new faq</button></a>
    <br>
    
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
          <br>
          @if(count($faqs) == 0)
            <div class="alert alert-warning">There is no faqs at this moment</div>
          @else
          <table class="table table-striped m-b-none">
          <thead>
            <tr>
              <th>#</th>
              <th>Question</th>
              <th>Answer</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($faqs as $faq)
            <tr>                   
              <td>#{{ $faq->id }}</td>
              <td>{{ $faq->question }}</td>
              <td>{{ $faq->answer }}</td>
              <td>
                  <a href="{{ route('admin.faq.edit', $faq) }}" class="btn btn-info btn-xs">Edit</a> -
                  {!! Form::open(['method' => 'DELETE', 'route' => ['admin.faq.destroy', $faq] ,'style' => 'display:inline']) !!}
                      {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                  {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        <br>
        <br>
        @endif
      </div>
  </div>
</div>

@endsection