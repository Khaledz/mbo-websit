@extends('layouts.admin')

@section('title', $renwal->title)
@section('content')
<div class="wrapper-md">
  <div class="row">
    <div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
          <p class="pull-right">Sent via: {{ $renwal->via }}</p>
        </div>
        <div class="panel-body">
        <h5 class="text-left">{!! html_entity_decode($renwal->content) !!}</h5>
        </div>
    </div>
  </div>
   <div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          Members
        </div>
        <div class="panel-body">
        @if($renwal->users->count() == 0)
          No data
        @else
        <ul style="list-style: none;">
        @foreach($renwal->users as $user)
          <li style="margin-bottom: 10px;"><img src="{{ asset($user->avatar()) }}" width="25" height="25"><a href="{{ route('app.user.show', [$user])}}">{{ $user->fullName }}</a></li>
        @endforeach
        </ul>
        @endif
        </div>
    </div>
  
  </div>
</div>

@endsection