@extends('layouts.admin')
@section('header')
  <link href="{{ asset('admin/css/summernote.css') }}" rel="stylesheet" type="text/css" media="all" />
@endsection

@section('title', 'Create')
@section('content')
<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
             @include('errors.admin_form')
            {!! Form::open(['route' => ['admin.faq.store'], 'class' => 'bs-example form-horizontal', 'enctype' => 'multipart/form-data']) !!}

            <div class="form-group {{ $errors->has('question') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Question</label>
              <div class="col-lg-4">
                {!! Form::text('question', null, ['class' => 'form-control', 'placeholder' => 'question']) !!}
              </div>
                @if ($errors->has('question'))
                    <span class="help-block">
                        <strong>{{ $errors->first('question') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('answer') ? 'has-error' : ''}}">
            <label class="col-lg-2 control-label">Answer</label>
            <div class="col-lg-9">
            {!! Form::textarea('answer', null, ['rows' => '4', 'class' => 'form-control', 'placeholder' => 'Content']) !!}
            </div>
              @if ($errors->has('answer'))
                  <span class="help-block">
                      <strong>{{ $errors->first('answer') }}</strong>
                  </span>
              @endif
          </div>

         
        </div>

        <div class="form-group btn-group-justified">
              <div class="col-lg-offset-4 col-lg-4">
                <button class="btn m-b-xs btn-md btn-info btn-addon center-block"><i class="fa fa-check"></i> Save</button>
              </div>
            </div>
  </div>
</div>

    
 {!! Form::close() !!}
@endsection

@section('script')
<script src="{{ asset('admin/js/summernote.min.js') }}"></script>
    <script>
    $(document).ready(function() {
      $('.summernote').summernote({
        placeholder: 'Description',
        tabsize: 2,
        height: 300
      });
    });
    </script>
@endsection