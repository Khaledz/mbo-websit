@extends('layouts.admin')

@section('title', $gallery->name)
@section('content')
<div class="wrapper-md">
  <div class="row">
    <div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
        <h5 class="text-left">{!! html_entity_decode($gallery->description) !!}</h5>
        </div>
    </div>
  </div>
  
  </div>
</div>

<div class="wrapper-md">
  <div class="row">
    <div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          Files
        </div>
        <div class="panel-body">
        @if($gallery->count() == 0)
          There is no files attach to this gallery.
        @else
          @foreach($gallery->attachments as $attachment)
          <div class="col-md-3" style="margin-bottom: 10px;">
            @if(! in_array($attachment->extention, ['jpg', 'jpeg', 'gif', 'png']))
              <a href="{{ asset('storage/galleries/' . $gallery->id .'/'. $attachment->name) }}"><img src="{{ asset('app/img/'. $attachment->extention .'.png') }}" width="300" height="300" /></a>
              <br>
                <a href="{{ route('admin.attachment.destroy', [$gallery, $attachment]) }}" style="color: red;">Remove</a>
            @else
                <img src="{{ asset('storage/galleries/' . $gallery->id .'/'. $attachment->name) }}" width="300" height="300" />
                <br>
                <a href="{{ route('admin.attachment.destroy', [$gallery, $attachment]) }}" style="color: red;">Remove</a>
            @endif
            </div>
          @endforeach
        @endif
        </div>
    </div>
  </div>
  
  </div>
</div>


@endsection