@extends('layouts.admin')

@section('title', 'List galleries')

@section('content')
<div class="wrapper-md">
@include('errors.admin_form')
    <a href="{{ route('admin.gallery.create') }}"><button class="btn m-b-xs btn-sm btn-primary btn-addon center-block"><i class="fa fa-plus"></i>Create new gallery</button></a>
    <br>
    
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
          <br>
          @if(count($galleries) == 0)
            <div class="alert alert-warning">There is no galleries at this moment</div>
          @else
          <table class="table table-striped m-b-none">
          <thead>
            <tr>
              <th>#</th>
              <th>Image</th>
              <th>Name</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($galleries as $gallery)
            <tr>                    
              <td>#{{ $gallery->id }}</td>
              <td><img src="{{ $gallery->thumb() }}" width="100" height="50" /></td>
              <td>{{ $gallery->name }}</td>
              <td>
                   <a href="{{ route('admin.gallery.show', $gallery) }}" class="btn btn-info btn-xs">View</a> - 
                  <a href="{{ route('admin.gallery.edit', $gallery) }}" class="btn btn-info btn-xs">Update</a> - 
                  {!! Form::open(['method' => 'DELETE', 'route' => ['admin.gallery.destroy', $gallery] ,'style' => 'display:inline']) !!}
                      {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                  {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        <br>
        <br>
        <center>{{ $galleries->links('vendor.pagination.default') }}</center>
        @endif
      </div>
  </div>
</div>

@endsection