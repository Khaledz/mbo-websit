@extends('layouts.admin')
@section('header')
  <link href="{{ asset('admin/libs/jquery/chosen/bootstrap-chosen.css') }}" rel="stylesheet" type="text/css" media="all" />
  <link href="{{ asset('admin/css/summernote.css') }}" rel="stylesheet" type="text/css" media="all" />
@endsection
@section('title', 'Edit a event')
@section('content')
<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
        {!! Form::model($gallery, ['method' => 'PUT', 'route' => ['admin.gallery.update', $gallery], 'method' => 'PUT', 'class' => 'bs-example form-horizontal', 'files' => true]) !!}
           <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Gallery Name</label>
              <div class="col-lg-4">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'gallery Title']) !!}
              </div>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('date') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Gallery Date</label>
              <div class="col-lg-4">
                <div class="input-group date" data-provide="datepicker">
                  <input type="text" class="form-control" id="datepicker" name="date">
                  <div class="input-group-addon">
                      <span class="glyphicon glyphicon-th"></span>
                  </div>
              </div>

              </div>
                @if ($errors->has('date'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Gallery Description</label>
              <div class="col-lg-9">
                {!! Form::textarea('description', null, ['rows' => '4', 'class' => 'form-control summernote', 'id' => 'summernote', 'placeholder' => 'Gallery Description']) !!}
              </div>
                @if ($errors->has('description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
              <label class="col-lg-2 control-label">Images</label>
              <div class="col-lg-4">
                <input type="file" name="files[]" multiple="">
              </div>
              @if($gallery->attachments->count() > 0)
              <div class="col-lg-4">
              <ul>
                @foreach($gallery->attachments as $attachment)
                <li>
                  <a href="{{ asset('storage/galleries/' . $gallery->id .'/'. $attachment->name) }}"/>{{ $attachment->orginal_name }}</a>
                   - <a href="{{ route('admin.attachment.destroy', [$gallery, $attachment]) }}" style="color: red;"> Remove</a>
                </li>
                @endforeach
              </ul>
              </div>
              @endif
            </div>

            <div class="form-group btn-group-justified">
              <div class="col-lg-offset-4 col-lg-4">
                <button class="btn m-b-xs btn-md btn-info btn-addon center-block"><i class="fa fa-check"></i> Save</button>
              </div>
            </div>
          {!! Form::close() !!}
        </div>
  </div>
</div>
@endsection
@section('script')
<script src="{{ asset('admin/js/summernote.min.js') }}"></script>
    <script>
    $(document).ready(function() {
      $('.summernote').summernote({
        placeholder: 'Description',
        tabsize: 2,
        height: 300
      });
    });
    </script>
    <script src="{{ asset('admin/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('admin/js/moment.min.js') }}"></script>
    <script src="{{ asset('admin/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('admin/libs/jquery/chosen/chosen.jquery.min.js') }}"></script>
   <script type="text/javascript">

      $(function () {
        $.fn.datepicker.defaults.format = "yyyy-mm-dd";
        var currentDate = new Date();
          $("#datepicker").datepicker("setDate" , '{{ $gallery->date }}');

          $(".w-md").chosen();

        
        var currentTime = new Date('{{ $gallery->date }}');
          $('#datetimepicker3').datetimepicker({
              format: 'LT',
              defaultDate: currentTime 
          });


          

      });

     
  </script>
  
@endsection