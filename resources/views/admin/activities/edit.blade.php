@extends('layouts.admin')
@section('header')
  <link href="{{ asset('admin/css/summernote.css') }}" rel="stylesheet" type="text/css" media="all" />
  <link href="{{ asset('admin/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet" type="text/css" media="all" />
@endsection

@section('title', 'Edit a activity')
@section('content')

<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
          <div class="panel-body">
          {!! Form::model($activity, ['method' => 'PUT', 'route' => ['admin.activity.update', $activity], 'method' => 'PUT', 'class' => 'bs-example form-horizontal', 'files' => true]) !!}
          <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
          <label class="col-lg-2 control-label">Activity Title</label>
          <div class="col-lg-4">
            {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Activity Title']) !!}
          </div>
            @if ($errors->has('title'))
                <span class="help-block">
                    <strong>{{ $errors->first('title') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
          <label class="col-lg-2 control-label">Description</label>
          <div class="col-lg-9">
            {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'summernote']) !!}
          </div>
            @if ($errors->has('description'))
                <span class="help-block">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">
          <label class="col-lg-2 control-label">Icon</label>
          <div class="col-lg-4">
            <input type="file" name="icon">
            <br>
            <span>Max slide size: {{ ini_get('upload_max_filesize') }}</span>
            @if($activity->icon())
            <img src="{{ $activity->icon() }}" width="50" height="50">
            @endif
          </div>
        </div>

        <div class="form-group {{ $errors->has('url') ? 'has-error' : ''}}">
          <label class="col-lg-2 control-label">Activity Url</label>
          <div class="col-lg-4">
            {!! Form::text('url', null, ['class' => 'form-control', 'placeholder' => 'Activity Url']) !!}
            <span>Example: <code>/event</code> for event page.</span>
          </div>
            @if ($errors->has('url'))
                <span class="help-block">
                    <strong>{{ $errors->first('url') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('background_color') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Element Backround Color</label>
              <div class="col-lg-4">
                {!! Form::text('background_color', null, ['class' => 'form-control', 'placeholder' => 'Pick color', 'id' => 'background_color']) !!}
              </div>
                @if ($errors->has('background_color'))
                    <span class="help-block">
                        <strong>{{ $errors->first('background_color') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('background_text_color') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Text Background Color</label>
              <div class="col-lg-4">
                {!! Form::text('background_text_color', null, ['class' => 'form-control', 'placeholder' => 'Pick color', 'id' => 'background_text_color']) !!}
              </div>
                @if ($errors->has('background_text_color'))
                    <span class="help-block">
                        <strong>{{ $errors->first('background_text_color') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('text_color') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Text Color</label>
              <div class="col-lg-4">
                {!! Form::text('text_color', null, ['class' => 'form-control', 'placeholder' => 'Pick Color', 'id' => 'text_color']) !!}
              </div>
                @if ($errors->has('text_color'))
                    <span class="help-block">
                        <strong>{{ $errors->first('text_color') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group btn-group-justified">
              <div class="col-lg-offset-4 col-lg-4">
                <button class="btn m-b-xs btn-md btn-info btn-addon center-block"><i class="fa fa-check"></i> Save</button>
              </div>
            </div>
          {!! Form::close() !!}
        </div>
  </div>
</div>
<script src="{{ asset('admin/js/summernote.min.js') }}"></script>
<script src="{{ asset('admin/js/bootstrap-colorpicker.min.js') }}"></script>
    <script>
    $(document).ready(function() {
      $('#summernote').summernote({
        placeholder: 'Description',
        tabsize: 2,
        height: 300
      });
      $(function() {
          $('#background_color').colorpicker();
          $('#text_color').colorpicker();
          $('#background_text_color').colorpicker();
      });
    });
    </script>
@endsection