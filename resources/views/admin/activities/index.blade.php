@extends('layouts.admin')

@section('title', 'List activities')

@section('content')
<div class="wrapper-md">
@include('errors.admin_form')
    <a href="{{ route('admin.activity.create') }}"><button class="btn m-b-xs btn-sm btn-primary btn-addon center-block"><i class="fa fa-plus"></i>Create new activity</button></a>
    <br>
    
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
          <br>
          @if(count($activities) == 0)
            <div class="alert alert-warning">There is no activities at this moment</div>
          @else
          <table class="table table-striped m-b-none">
          <thead>
            <tr>
              <th>#</th>
              <th>Image</th>
              <th>Title</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($activities as $activity)
            <tr>                    
              <td>#{{ $activity->id }}</td>
              <td><img src="{{ $activity->icon() }}" width="100" height="100"></td>
              <td>{{ $activity->title }}</td>
              <td>
                  <a href="{{ route('admin.activity.edit', $activity) }}" class="btn btn-info btn-xs">Update</a> - 
                  {!! Form::open(['method' => 'DELETE', 'route' => ['admin.activity.destroy', $activity->title] ,'style' => 'display:inline']) !!}
                      {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                  {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        <br>
        <br>
        <center>{{ $activities->links('vendor.pagination.default') }}</center>
        @endif
      </div>
  </div>
</div>

@endsection