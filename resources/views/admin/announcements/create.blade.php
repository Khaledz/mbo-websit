@extends('layouts.admin')
@section('header')
  <link href="{{ asset('admin/css/summernote.css') }}" rel="stylesheet" type="text/css" media="all" />
@endsection

@section('title', 'Create new announcement')
@section('content')
<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
             @include('errors.admin_form')
            {!! Form::open(['route' => ['admin.announcement.store'], 'class' => 'bs-example form-horizontal', 'enctype' => 'multipart/form-data']) !!}

            <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Announcement Title</label>
              <div class="col-lg-4">
                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'announcement Title']) !!}
              </div>
                @if ($errors->has('title'))
                    <span class="help-block">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Announcement Description</label>
              <div class="col-lg-9">
              {!! Form::textarea('description', null, ['rows' => '4', 'class' => 'form-control', 'id' => 'summernote', 'placeholder' => 'event Description']) !!}
              </div>
                @if ($errors->has('description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
              <label class="col-lg-2 control-label">Files (optional)</label>
              <div class="col-lg-4">
                <input type="file" name="files[]" multiple="">
                <span class="help-block m-b-none">Press and hold ctrl to select multiple files.</span>
              </div>
            </div>


            <div class="line line-dashed b-b line-lg pull-in"></div>
            <h3>Receivers</h3>

            <div class="form-group">
              <label class="col-lg-2 control-label">Send invitation to</label>
              <div class="col-lg-4">
              <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="invite" value="custom" checked>
                    <i></i>
                    Custom Members
                  </label>
                </div>
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="invite" value="all" >
                    <i></i>
                    All Members
                  </label>
                </div>
                
              </div>
            </div>

            <div id="users-list">
            <div class="form-group {{ $errors->has('users[]') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Select Members</label>
              <div class="col-lg-4">
                {!! Form::select('users[]', $users, null, ['ui-jq' => 'chosen', 'class' => 'w-md', 'multiple']) !!}
                <span class="help-block m-b-none">Start typing the member name.</span>
              </div>
                @if ($errors->has('users[]'))
                    <span class="help-block">
                        <strong>{{ $errors->first('users[]') }}</strong>
                    </span>
                @endif
            </div>
            </div>

            <div class="line line-dashed b-b line-lg pull-in"></div>
            <h3>Notifications</h3>
              <div class="form-group">
              <label class="col-lg-2 control-label"></label>
              <div class="col-lg-4">
              <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="via" value="all" checked>
                    <i></i>
                    All
                  </label>
                </div>
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="via" value="sms" >
                    <i></i>
                    SMS only
                  </label>
                </div>

                 <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="via" value="email" >
                    <i></i>
                    Email only
                  </label>
                </div>
                
              </div>
            </div>

            <div class="form-group btn-group-justified">
              <div class="col-lg-offset-4 col-lg-4">
                <button class="btn m-b-xs btn-md btn-info btn-addon center-block"><i class="fa fa-check"></i> Save</button>
              </div>
            </div>
          {!! Form::close() !!}
        </div>
  </div>
</div>

@endsection

@section('script')
    <script src="{{ asset('admin/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('admin/js/moment.min.js') }}"></script>
    <script src="{{ asset('admin/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('admin/js/summernote.min.js') }}"></script>
    <script>
    $(document).ready(function() {
      $('#summernote').summernote({
        placeholder: 'Description',
        tabsize: 2,
        height: 300
      });
    });
    </script>

   <script type="text/javascript">
      $(function () {
          $('#datetimepicker3').datetimepicker({
              format: 'LT'
          });


          {{--  Invite  --}}
          $('form input').on('change', function() {
            if($('input[name=invite]:checked', 'form').val() == 'all')
            {
              $('#users-list').css('display', 'none');
              
            } 
            if($('input[name=invite]:checked', 'form').val() == 'custom')
            {
              $('#users-list').css('display', '');
            } 
          });

      });
  </script>
  
@endsection
