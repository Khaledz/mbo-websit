@extends('layouts.admin')

@section('title', 'List announcements')

@section('content')
<div class="wrapper-md">
@include('errors.admin_form')
    <a href="{{ route('admin.announcement.create') }}"><button class="btn m-b-xs btn-sm btn-primary btn-addon center-block"><i class="fa fa-plus"></i>Create new announcement</button></a>
    <br>
    
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
          <br>
          @if(count($announcements) == 0)
            <div class="alert alert-warning">There is no announcements at this moment</div>
          @else
          <table class="table table-striped m-b-none">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Members</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($announcements as $announcement)
            <tr>                    
              <td>#{{ $announcement->id }}</td>
              <td>{{ $announcement->title }}</td>
              <td>({{ $announcement->users->count() }}) {{ str_plural('member', $announcement->description) }}</td>
              <td>
                  <a href="{{ route('admin.announcement.show', $announcement) }}" class="btn btn-info btn-xs">View</a> -
                  {!! Form::open(['method' => 'DELETE', 'route' => ['admin.announcement.destroy', $announcement] ,'style' => 'display:inline']) !!}
                      {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                  {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        <br>
        <br>
        <center>{{ $announcements->links('vendor.pagination.default') }}</center>
        @endif
      </div>
  </div>
</div>

@endsection