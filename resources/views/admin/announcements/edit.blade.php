@extends('layouts.admin')

@section('title', 'Edit a event')
@section('content')

<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
        {!! Form::model($market, ['method' => 'PUT', 'route' => ['admin.market.update', $market], 'method' => 'PUT', 'class' => 'bs-example form-horizontal', 'files' => true]) !!}
           <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Market Name</label>
              <div class="col-lg-4">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Market Name']) !!}
              </div>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Market Description</label>
              <div class="col-lg-4">
                {!! Form::textarea('description', null, ['rows' => '4', 'class' => 'form-control', 'placeholder' => 'Market Description']) !!}
              </div>
                @if ($errors->has('description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
              <label class="col-lg-4 control-label">Files (optional)</label>
              <div class="col-lg-4">
                <input type="file" name="files[]" multiple="">
                <span class="help-block m-b-none">Press and hold ctrl to select multiple files.</span>
              </div>
            </div>

            <div class="form-group {{ $errors->has('description_of_file') ? 'has-error' : ''}}">
              <label class="col-lg-4 control-label">Description of File (optional)</label>
              <div class="col-lg-4">
                {!! Form::text('description_of_file', null, ['class' => 'form-control', 'placeholder' => 'Description of File']) !!}
              </div>
                @if ($errors->has('description_of_file'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description_of_file') }}</strong>
                    </span>
                @endif
            </div>
            
            <div class="form-group btn-group-justified">
              <div class="col-lg-offset-4 col-lg-4">
                <button class="btn m-b-xs btn-md btn-info btn-addon center-block"><i class="fa fa-check"></i> Save</button>
              </div>
            </div>
          {!! Form::close() !!}
        </div>
  </div>
</div>

@endsection