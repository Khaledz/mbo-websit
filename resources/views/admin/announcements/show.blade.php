@extends('layouts.admin')

@section('title', $announcement->title)
@section('content')
<div class="wrapper-md">
  <div class="row">
    <div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
        <h5 class="text-left">{!! html_entity_decode($announcement->description) !!}</h5>
        </div>
    </div>
  </div>
   <div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          Attachments
        </div>
        <div class="panel-body">
        @if($announcement->attachments()->count() == 0)
          No data
        @else
        <ul style="list-style: none;">
        @foreach($announcement->attachments()->get() as $attachment)
          <li><a href="{{ asset('storage/announcements/'.$announcement->id. '/'.$attachment->name) }}">{{ $attachment->orginal_name }}</a></li>
        @endforeach
        </ul>
        @endif
        </div>
    </div>
    
  
  </div>
  <div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          Receivers
        </div>
        <div class="panel-body">
        @if($announcement->users()->count() == 0)
          No data
        @else
        <ul style="list-style: none;">
        @foreach($announcement->users()->get() as $user)
          <li style="margin-bottom: 5px;"><img src="{{ asset($user->avatar()) }}" width="25" height="25"><a href="{{ route('app.user.show', [$user])}}">{{ $user->fullName }}</a></li>
        @endforeach
        </ul>
        @endif
        </div>
    </div>
</div>

@endsection