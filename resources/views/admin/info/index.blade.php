@extends('layouts.admin')

@section('title', 'List activities')

@section('content')
<div class="wrapper-md">
@include('errors.admin_form')
    <a href="{{ route('admin.info.edit', 1) }}"><button class="btn m-b-xs btn-sm btn-primary btn-addon center-block"><i class="fa fa-plus"></i>Update information</button></a>
    <br>
    
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
          <br>
          @if(count($infos) == 0)
            <div class="alert alert-warning">There is no texts at this moment</div>
          @else
          <table class="table table-striped m-b-none">
          <thead>
            <tr>
              <th>Term</th>
              <th>Text</th>
            </tr>
          </thead>
          <tbody>

            @for($i = 0; $i < count($infos->settings()->all()); $i++)
            <tr>                    
              <td>{{ array_keys($infos->settings()->all())[$i] }}</td>
              <td>{{ strip_tags(array_values($infos->settings()->all())[$i]) }}</td>
            </tr>
            @endfor
          </tbody>
        </table>
        <br>
        <br>
        @endif
      </div>
  </div>
</div>

@endsection