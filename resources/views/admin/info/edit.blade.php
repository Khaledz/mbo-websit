@extends('layouts.admin')
@section('header')
  <link href="{{ asset('admin/css/summernote.css') }}" rel="stylesheet" type="text/css" media="all" />
  <link href="{{ asset('admin/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet" type="text/css" media="all" />
@endsection

@section('title', 'Edit a MBO Information')
@section('content')

<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
          <div class="panel-body">
          {!! Form::model($info, ['method' => 'PUT', 'route' => ['admin.info.update', 1], 'method' => 'PUT', 'class' => 'bs-example form-horizontal', 'files' => true]) !!}
           @for($i = 0; $i < count($info->settings()->all()); $i++)
           <div class="form-group">
            <label class="col-lg-2 control-label">{{ array_keys($info->settings()->all())[$i] }}</label>
            <div class="col-lg-8">
              {!! Form::textarea(array_keys($info->settings()->all())[$i], array_values($info->settings()->all())[$i], ['class' => 'summernote form-control']) !!}
            </div>
              
          </div>
            @endfor
            <div class="form-group btn-group-justified">
              <div class="col-lg-offset-4 col-lg-4">
                <button class="btn m-b-xs btn-md btn-info btn-addon center-block"><i class="fa fa-check"></i> Save</button>
              </div>
            </div>
          {!! Form::close() !!}
        </div>
  </div>

</div>
 <script src="{{ asset('admin/libs/jquery/chosen/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('admin/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('admin/js/moment.min.js') }}"></script>
    <script src="{{ asset('admin/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('admin/js/summernote.min.js') }}"></script>
<script src="{{ asset('admin/js/bootstrap-colorpicker.min.js') }}"></script>
    <script>

    $(document).ready(function() {
      $('.summernote').summernote({
        placeholder: 'Description',
        tabsize: 2,
        height: 300
      });
      
    });
</script>
@endsection
