@extends('layouts.admin')
@section('header')
  <link href="{{ asset('admin/css/summernote.css') }}" rel="stylesheet" type="text/css" media="all" />
@endsection

@section('title', 'Create new renwal')
@section('content')
<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
             @include('errors.admin_form')
            {!! Form::open(['route' => ['admin.renwal.store'], 'class' => 'bs-example form-horizontal', 'enctype' => 'multipart/form-data']) !!}

            <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label">Title</label>
              <div class="col-lg-4">
                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
              </div>
                @if ($errors->has('title'))
                    <span class="help-block">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('content') ? 'has-error' : ''}}">
            <label class="col-lg-2 control-label">Content</label>
            <div class="col-lg-9">
            {!! Form::textarea('content', null, ['rows' => '4', 'class' => 'form-control summernote', 'id' => 'summernote', 'placeholder' => 'Content']) !!}
            </div>
              @if ($errors->has('content'))
                  <span class="help-block">
                      <strong>{{ $errors->first('content') }}</strong>
                  </span>
              @endif
          </div>

         
        </div>
  </div>
</div>

<div class="wrapper-md">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
            <div class="panel-heading font-bold">                  
              Members
            </div>
            <div class="panel-body">
              <div class="form-group {{ $errors->has('users[]') ? 'has-error' : ''}}">
              <label class="col-lg-2 control-label text-right">Members</label>
              <div class="col-lg-4">
                {!! Form::select('users[]', $users, null, ['ui-jq' => 'chosen', 'class' => 'w-md', 'multiple']) !!}
                <span class="help-block m-b-none">Start typing the member name.</span>
              </div>
                @if ($errors->has('users[]'))
                    <span class="help-block">
                        <strong>{{ $errors->first('users[]') }}</strong>
                    </span>
                @endif
            </div>
            </div>
      </div>
    </div>
  </div>
</div>

<div class="wrapper-md">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
            <div class="panel-heading font-bold">                  
              Notifications
            </div>
            <div class="panel-body">
              <div class="form-group">
              <label class="col-lg-2 control-label"></label>
              <div class="col-lg-4">
              <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="via" value="all" checked>
                    <i></i>
                    All
                  </label>
                </div>
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="via" value="sms" >
                    <i></i>
                    SMS only
                  </label>
                </div>

                 <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="via" value="email" >
                    <i></i>
                    Email only
                  </label>
                </div>
                
              </div>
            </div>
            </div>

            <div class="form-group btn-group-justified">
              <div class="col-lg-offset-4 col-lg-4">
                <button class="btn m-b-xs btn-md btn-info btn-addon center-block"><i class="fa fa-check"></i> Save</button>
              </div>
            </div>
      </div>
    </div>
  </div>
</div>
 {!! Form::close() !!}
@endsection

@section('script')
<script src="{{ asset('admin/js/summernote.min.js') }}"></script>
    <script>
    $(document).ready(function() {
      $('.summernote').summernote({
        placeholder: 'Description',
        tabsize: 2,
        height: 300
      });
    });
    </script>
@endsection