@extends('layouts.admin')

@section('title', $post->title)
@section('content')
<div class="wrapper-md">
  <div class="row">
    <div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          {{ $post->title }}
        </div>
        <div class="panel-body">
        <h5 class="text-left">{!! html_entity_decode($post->content) !!}</h5>
        </div>
    </div>
  </div>
  
  </div>
</div>

<div class="wrapper-md">
  <div class="row">
    <div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          Posts Files
        </div>
        <div class="panel-body">
        @if($post->attachments->count() == 0)
          There is no files attach to this post.
        @else
        <div class="row">
        <div class="col-md-3" style="margin-bottom: 10px;">
          @foreach($post->attachments as $attachment)
            <a href="{{ asset('storage/clubs/'.$post->club->id. '/posts/'. $post->id .'/'. $attachment->name) }}">{{ $attachment->orginal_name }}</a><br>
          @endforeach
        </div>
      </div>
        @endif
        </div>
    </div>
  </div>

  <!-- comments -->
  <div class="wrapper-md">
  <div class="row">
    <div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          Comments
        </div>
        <div class="panel-body">
        @if($post->comments->count() == 0)
          There is no comments for this post.
        @else
        <table class="table table-striped m-b-none">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Content</th>
                  <th>Posted by</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($post->comments as $comment)
                <tr>                    
                  <td>#{{ $comment->id }}</td>
                  <td>{{ substr($comment->content, 0, 20) }}</td>
                  <td>{{ $comment->user->fullName }}</td>
                  <td>
                      <a href="{{ route('admin.comment.show', $comment) }}" class="btn btn-info btn-xs">View</a> -
                      <a href="{{ route('admin.comment.edit', $comment) }}" class="btn btn-info btn-xs">Update</a> - 
                      {!! Form::open(['method' => 'DELETE', 'route' => ['admin.comment.destroy', $comment] ,'style' => 'display:inline']) !!}
                          {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                      {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
              </tbody>
            </table>
        @endif
        </div>
    </div>
  </div>
  
  </div>
</div>


@endsection