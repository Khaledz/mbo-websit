@extends('layouts.admin')

@section('title', 'List posts')

@section('content')
<div class="wrapper-md">
@include('errors.admin_form')
    <br>
    
    <div class="panel panel-default">
        <div class="panel-heading font-bold">                  
          @yield('title', '')
        </div>
        <div class="panel-body">
          <br>
          @if(count($posts) == 0)
            <div class="alert alert-warning">There is no posts at this moment</div>
          @else
          <table class="table table-striped m-b-none">
          <thead>
            <tr>
              <th>#</th>
              <th>Title</th>
              <th>Content</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($posts as $post)
            <tr>                    
              <td>#{{ $post->id }}</td>
              <td>{{ $post->title }}</td>
              <td>{{ substr($post->content, 0, 20) }}</td>
              <td>
                   <a href="{{ route('admin.post.show', $post) }}" class="btn btn-info btn-xs">View</a> - 
                  <a href="{{ route('admin.post.edit', $post) }}" class="btn btn-info btn-xs">Update</a> - 
                  {!! Form::open(['method' => 'DELETE', 'route' => ['admin.post.destroy', $post] ,'style' => 'display:inline']) !!}
                      {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                  {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        <br>
        <br>
        <center>{{ $posts->links('vendor.pagination.default') }}</center>
        @endif
      </div>
  </div>
</div>

@endsection