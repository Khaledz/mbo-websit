<?php

return [
	'name' => 'MBO',
	
	'App\Http\Controllers\App\HomesController' => [
		'index' => 'Better leaders through higher values',
		'value' => 'Value',
		'directory' => 'Board Members',
		'faq' => 'FAQ',
		'contact' => 'Contact us',
		'terms' => 'Terms',
		'list_members' => 'MBO Members',
		'about' => 'About Us'
	],
	
	'App\Http\Controllers\App\EventsController' => [
		'index' => 'List of Events',
		'show' => 'slug',
		'allMembers' => 'All Members'
	],

	'App\Http\Controllers\App\SettingsController' => [
		'show' => 'Settings'
	],

	'App\Http\Controllers\App\CalendarsController' => [
		'index' => 'Calendar'
	],

	'App\Http\Controllers\App\ClubsController' => [
		'index' => 'Multaqa',
		'show' => 'slug'
	],

	'App\Http\Controllers\App\ClubsPostsController' => [
		'create' => 'create a new post',
		'show' => 'slug'
	],

	'App\Http\Controllers\App\GalleriesController' => [
		'index' => 'Gallery',
		'show' => 'name',
		'filterByYear' => 'Galleries by year'
	],

	'App\Http\Controllers\App\MarketPlacesController' => [
		'index' => 'Market Places',
		'create' => 'Create market place',
		'show' => 'slug'
	],

	'App\Http\Controllers\Auth\RegisterController' => [
		'showRegistrationForm' => 'Registration',
	],

	'App\Http\Controllers\Auth\LoginController' => [
		'showLoginForm' => 'Membership Login',
	],

	'App\Http\Controllers\App\UsersController' => [
		'show' => "name",
	],

	'App\Http\Controllers\App\EventCategoriesController' => [
		'index' => "Event Categories",
	],
];