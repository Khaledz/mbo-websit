function uploadImage(input, request)
{
    var avatar = $(input)[0].files[0];
    var form = new FormData();
    if(input.charAt(0) === '#' || input.charAt(0) === '.')
    {
    	var inputWithoutIdOrClass = input.substr(1);
    }
    form.append(inputWithoutIdOrClass, avatar);
    $.ajax({
        url: request,
        data: form,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success: function(data) {
            if(data.status == true)
            {
                window.location = data.redirect;
            }
        }
    });
}

function follow(parentDiv, formDiv)
{
    $(parentDiv).on('click', '.follow', function(){
        
        var form = $(this).parent().closest(formDiv);
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            success: function(data) {
                if(data.status == true)
                {
                    var formUrl = form.attr('action').replace('follow', 'unfollow');
                    form.attr('action', formUrl);
                    form.children('span').html('<a href="#" class="unfollow">إلغاء المتابعة</a>');
                }
            }
        });
    });

    $(parentDiv).on('click', '.unfollow', function(){
        
        var form = $(this).parent().closest(formDiv);
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            success: function(data) {
                if(data.status == true)
                {
                    var formUrl = form.attr('action').replace('unfollow', 'follow');
                    form.attr('action', formUrl);
                    form.children('span').html('<a href="#" class="follow">تابع</a>');
                }
            }
        });
    });
}

function vote()
{
    $(".project-item").on('click', '.post_upvote', function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        $.ajax({
            type: "GET",
            url: url,
            success: function(data) {
                if(data.status == true)
                {
                    $(this).find('.post_points').html('wooooooow');
                }
            }
        });
    });

    $(".project-item").on('click', '.post_downvote', function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        $.ajax({
            type: "GET",
            url: url,
            success: function(data) {
                if(data.status == true)
                {
                    $(this).closest('.post_vote').find('.post_points').html('wooooooow');
                }
            }
        });
    });
}