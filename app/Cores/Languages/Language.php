<?php

namespace App\Cores\Languages;

use App;
use Auth;
use Cache;
use App\Models\Language as LanguageModel;

class Language
{
	/**
     * The locale of the app.
     *
     * @var string
     */
	protected $locale;

	/**
     * The data of all the language.
     *
     * @var array
     */
	protected $data = [];

	/**
     * Build instance of Language class.
     *
     * @var boolean
     */
	public function __construct()
	{
		return App::setLocale($this->get()->code);
	}

	/**
     * Set the language of the app.
	 *
	 * @param string locale
     * @return string
     */
	public function set($locale = null)
	{
		$this->locale = $locale ?: $this->setDefault();

		if(cache()->has('language') && cache('language')->code != $this->locale)
		{
			cache()->forever('language', LanguageModel::where('code', $this->locale)->first());
		}

		return App::setLocale($this->locale);
	}

	/**
     * Get the language of the app.
	 *
     * @return array of language.
     */
	public function get()
	{
		if(! cache()->has('language'))
		{
			$this->setDefault();
		}

		return cache('language');
	}

	/**
     * Get the available languages except the current app langaue.
	 *
     * @return collection
     */
	public function switches()
	{
		if(! cache()->has('language_switchers') or cache()->has('language_switchers') && cache('language_switchers')->first()->code == cache('language')->code)
		{
			Cache::forever('language_switchers', LanguageModel::where('code', '!=', $this->get()->code)->get());
		}
		
		return cache('language_switchers');
	}

	/**
     * Get the direction of the language
	 *
     * @return string
     */
	public function getDir()
	{
		return $this->get()->direction;
	}

	/**
     * Set the description of the app.
     *
     * @param $description string
     * @return boolean
     */
	public function setDescription($description)
	{
		return Cache::set('app_description', $description);
	}

	/**
     * Get the description of the app.
     *
     * @return string
     */
	public function getDescription()
	{
		return cache('app_description');
	}

	/**
     * Set the keywords of the app.
     *
     * @param $meta string
     * @return string
     */
	public function setKeywrods($keywords)
	{
		return Cache::set('app_keywords', $keywords);
	}

	/**
     * Get the keywords of the app.
     *
     * @return string
     */
	public function getKeywords()
	{
		return cache('keywords');
	}

	/**
     * Check if the langauge of app equals the passed parameter.
     *
     * @param  string $locale
     * @return boolean
     */
	public function is($locale)
	{

	}

	/**
     * Fetch the whole lines of app language as json.
     *
     * @return json
     */
	public function fetch()
	{
		return $this->readFiles() ? json_encode($this->output) : [];
	}

	/**
     * Process the reading of each file in the language.
     * Format {locale: {filename: {key: value}}
     * @return Exception | true
     */
	protected function readFiles()
	{
		$path = resource_path('lang') .'/'. $this->locale;

		if(!is_dir($path))
		{
			throw new \Exception("The path {$path} does not exists.");
		}

		$handle = opendir($path);

		$this->output = [$this->locale => array()];

	    while (false !== ($file = readdir($handle)))
	    {
	    	if ($file == '.' or $file == '..') continue;
	        $this->output[$this->locale][pathinfo($file, PATHINFO_FILENAME)] = include($path . '/'. $file);
	    }

	    closedir($handle);

	    return true;
	}

	/**
     * Set the cache to default values. 
     *
     * @param  string $locale
     * @return string locale
     */
	protected function setDefault()
	{
		cache()->forever('language', LanguageModel::where('code', App::getLocale())->first());

		return App::getLocale();
	}

	/**
     * Check if the language is available
     *
     * @param  string $locale
     * @return boolean
     */
	protected function isAvailable($locale)
	{
		return LanguageModel::where('code', $locale)->count() > 0;
	}
}