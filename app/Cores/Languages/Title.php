<?php

namespace App\Cores\Languages;

use Route;

class Title
{
	/**
     * File name of the translation.
     *
     * @var fileName
     */
	protected $fileName = 'title';

	/**
     * Laravel Route
     *
     * @var route
     */
    protected $route;
    
    /**
     * $request
     *
     * @var \Illuminate\Http\Request $request
     */
	protected $request;

	/**
     * Create a new title instance.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
	public function __construct($request)
	{
        $this->request = $request;
        $this->route = \Route::getCurrentRoute()->getAction();
	}

	/**
     * Get the title by the language of the app.
     *
     * @return string
     */
	public function get()
	{
        if($this->availability() == false)
        {
            return '';
        }

        if(trans("{$this->fileName}.{$this->getController()}.{$this->getAction()}") == 'slug')
        {
            return $this->getSlug();
        }

        if(trans("{$this->fileName}.{$this->getController()}.{$this->getAction()}") == 'name')
        {
            return $this->getName();
        }
        
        return trans("{$this->fileName}.{$this->getController()}.{$this->getAction()}");
	}

	/**
     * Get the controler of the current route.
     *
     * @return string
     */
	protected function getController()
	{
		return explode('@', $this->route['controller'])[0];
	}

	/**
     * Get the action of the current route.
     *
     * @return string
     */
	protected function getAction()
	{
		return explode('@', $this->route['controller'])[1];
	}

    /**
     * check if the title is available in the uses in route.
     *
     * @return boolean
     */
    private function availability()
    {
        return $this->route['controller'] != null;
    }

    /**
     * Get the last string of url.
     *
     * @return string
     */
    private function getSlug()
    {
        return str_replace(["-", "–"], ' ', array_slice(explode('/', rtrim($this->request->getRequestUri(), '/')), -1)[0]);
    }

    /**
     * Get the last string of url.
     *
     * @return string
     */
    private function getName()
    {
        return str_replace("%20", " ", array_slice(explode('/', rtrim($this->request->getRequestUri(), '/')), -1)[0]);
    }
}