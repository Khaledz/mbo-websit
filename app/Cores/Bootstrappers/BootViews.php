<?php

namespace App\Cores\Bootstrappers;

use App\Cores\Languages\Language;
use Cache;
use View;

class BootViews implements Bootstrapper
{
	public function boot()
	{
		View::share('language_switchers', app('language')->switches());
		View::share('language', app('language')->get());
		View::share('app_description', app('language')->getDescription());
		View::share('app_keywords', app('language')->getKeywords());
	}
}