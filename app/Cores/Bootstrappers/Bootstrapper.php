<?php

namespace App\Cores\Bootstrappers;

interface Bootstrapper
{
	public function boot();
}