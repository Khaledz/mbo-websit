<?php

namespace App\Cores\Communications\SMS;
use Nexmo as NexmoVendor;

class Nexmo implements SMS
{
	/**
     * send from
     * @var string
     */
	protected $from;

	/**
     * send to
     * @var string
     */
	protected $to;

	/**
     * type of send
     * @var string
     */
	protected $type;

	/**
     * title of the SMS
     * @var string
     */
	protected $title;

	/**
     * The text that are going to send.
     * @var string
     */
	protected $text;

	/**
     * Create new instance.
     * 
     * @param array $configs
     * @return this
     */
	public function __construct()
	{
		$this->from  = config('nexmo.from');
		$this->title = config('app.name');
	}

	/**
     * Set any kind of configs
     * 
     * @param array $configs
     * @return this
     */
	public function setConfig(array $configs)
	{
		$this->configs = $configs;

		return $this;
	}

	/**
     * Set the SMS from
     * 
     * @param string $from
     * @return this
     */
	 public function title($title)
	 {
		 $this->title = $title;
 
		 return $this;
	 }

	/**
     * Set the SMS from
     * 
     * @param string $from
     * @return this
     */
	public function from($from)
	{
		$this->from = $from;

		return $this;
	}

	/**
     * Set the SMS to
     * 
     * @param string $to
     * @return this
     */
	public function to($to)
	{
		$this->to = $to;

		return $this;
	}

	/**
     * Set the SMS text
     * 
     * @param string $text
     * @return this
     */
	public function text($text)
	{
		$this->text = $text;

		return $this;
	}

	/**
     * Send the sms object.
     * 
     * @return boolean | Exceptions
     */
	public function send()
	{
		if(empty($this->text))
		{
			throw new \Exception('The text of SMS is empty!');
		}
		if(count($this->to) > 1)
		{
			foreach($this->to as $user)
			{
				$phone = 966 . $user->phone;
				NexmoVendor::message()->send([
					'to' => $phone,
					'from' => $this->from,
					'text' => $this->text,
					'title' => $this->title,
					'type' => 'unicode'
				]);
			}
		}
		else
		{
			$phone = 966 . $this->to->phone;
			return NexmoVendor::message()->send([
				'to' => $phone,
				'from' => $this->from,
				'text' => $this->text,
				'title' => $this->title,
				'type' => 'unicode'
			]);
		}
	}
}