<?php

namespace App\Cores\Communications\SMS;

interface SMS
{
	/**
     * Set any kind of configs
     * 
     * @param array $configs
     * @return this
     */
	public function setConfig(array $configs);

	/**
     * Set the SMS from
     * 
     * @param string $from
     * @return this
     */
	public function from($from);

	/**
     * Set the SMS to
     * 
     * @param string $to
     * @return this
     */
	public function to($to);

	/**
     * Set the SMS text
     * 
     * @param string $text
     * @return this
     */
	public function text($text);

	/**
     * Send the sms object.
     * 
     * @return bolean | Exceptions
     */
	public function send();
}