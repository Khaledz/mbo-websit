<?php

namespace App\Listeners;

use App\Events\UserWasRegistred;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use App\Mail\Admin\NewUser;
use Mail;

class MailNewUserToAdmins implements ShouldQueue
{
    public $admins;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->admins = User::role('admin')->get();
    }

    /**
     * Handle the event.
     *
     * @param  UserWasRegistred  $event
     * @return void
     */
    public function handle(UserWasRegistred $event)
    {
        if($this->admins->count() > 0)
        {
            foreach($this->admins as $admin)
            {
                Mail::to($admin)->queue(new NewUser($admin, $event));
            }
        }
    }
}
