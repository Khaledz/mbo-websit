<?php

namespace App\Listeners;

use App\Events\EventWasPublished;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\Membership\EventInvitation;
use Mail;
use App\Cores\Communications\SMS\Nexmo;
use App\Notifications\Event;

class SendInvitation implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EventWasPublished  $event
     * @return void
     */
    public function handle(EventWasPublished $event)
    {
        if(in_array('email', $event->via))
        {
            foreach($event->users as $user)
            {
                Mail::to($user)->queue(new EventInvitation($event->event, $user));
            }
        }

        if(in_array('sms', $event->via))
        {
            // There is no title in Nexmo, it called from.
            (new Nexmo())->title($event->event->title .' - MBO')->to($event->users)->text(strip_tags($event->event->description))->send();            
        }
    }
}
