<?php

namespace App\Listeners;

use App\Events\AnnouncementWasPublished;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use App\Mail\Membership\AnnouncementMail;
use Mail;
use App\Cores\Communications\SMS\Nexmo;

class SendAnnouncements implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AnnouncementWasPublished  $event
     * @return void
     */
    public function handle(AnnouncementWasPublished $event)
    {
        if(in_array('email', $event->via))
        {
            if($event->users->count() > 0)
            {
                foreach($event->users as $user)
                {
                    Mail::to($user)->queue(new AnnouncementMail($event->announcement, $user));
                }
            }
        }

        if(in_array('sms', $event->via))
        {
            (new Nexmo())->to($event->users)->text(strip_tags($event->announcement->description))->send();
        }
    }
}
