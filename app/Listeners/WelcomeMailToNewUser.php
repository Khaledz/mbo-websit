<?php

namespace App\Listeners;

use App\Events\UserWasRegistred;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\Membership\WelcomeUser;
use Mail;

class WelcomeMailToNewUser implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserWasRegistred  $event
     * @return void
     */
    public function handle(UserWasRegistred $event)
    {
        return Mail::to($event->user)->queue(new WelcomeUser($event->user));
    }
}
