<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AnnouncementWasPublished
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $announcement;

    public $users;

    public $via = [];

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($announcement, $users, array $via)
    {
        $this->announcement = $announcement;
        $this->users = $users;
        $this->via = $via;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
