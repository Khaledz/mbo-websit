<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\Event;

class EventWasPublished
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $event;

    public $users;

    public $via = [];

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Event $event, $users, array $via)
    {
        $this->event = $event;
        $this->users = $users;
        $this->via = $via;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
