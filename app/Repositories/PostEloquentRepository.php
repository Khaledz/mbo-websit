<?php
namespace App\Repositories;

use App\Models\Post;
use App\Models\Club;
use Illuminate\Http\Request;
use DB;

class PostEloquentRepository implements PostRepositoryInterface
{
    /**
     * The lists of available filters in the app.
     *
     * @return array
     */
    protected $filters = [
        'latest', 'votes', 'popular', 'favorites', 'mine'
    ];

    /**
     * Get all posts of app
     * 
     * @param Illuminate\Http\Request $request
     * @return Collection
     */
	public function all(Request $request)
    {
        $this->validateUrl($request);

        return Post::latest();
    }


    /**
     * Get the first post with needed relations.
     * 
     * @ App\Models\Post $post
     * @return Collection
     */
	// public function first()
    // {
    //     return var_dump('works');
    // }

    /**
     * Get the posts by a query string
     *
     * @param Illuminate\Http\Request $request
     * @return Collection
     */
    public function getPostsBy(Club $club, Request $request)
    {
        $this->validateUrl($request);

        if($request->filter == 'latest')
        {
            $query = $club->posts()->withCount('votes')->latest();
        }

        if($request->filter == 'votes')
        {
            $query = $club->posts()->withCount('votes')->orderBy('votes_count', 'desc');
        }

        if($request->filter == 'favorites')
        {
            $query = $club->posts()->withCount('votes')->orderBy('favorites_count', 'desc');
        }

        if($request->filter == 'popular')
        {
            $query = $club->posts()->withCount('votes')->orderBy('views', 'desc');
        }

        if($request->filter == 'mine')
        {
            $query = $club->posts()->withCount('votes')->where('user_id', auth()->user()->id);
        }

        return $query;
    }

    /**
     * Count a posts of model
     *
     * @param Illuminate\Database\Eloquent\Model $model
     * @return Collection
     */
    public function countPosts(Post $post)
    {
        
    }

    /**
     * Validate the coming request.
     *
     * @param Illuminate\Http\Request $request
     * @return boolean
     */
    private function validateUrl(Request $request)
    {
        if(! isset($request->filter))
        {
            $request->merge(['filter' => 'latest']);
        }

        if(! in_array($request->filter, $this->filters))
        {
            return abort(409, 'حصلت مشكلة أثناء معالجة طلبك.');
        }

        return true;
    }
}