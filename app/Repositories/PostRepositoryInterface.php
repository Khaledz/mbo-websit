<?php 
namespace App\Repositories;

use App\Models\Post;
use App\Models\Club;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

interface PostRepositoryInterface
{
    // public function first();
    public function all(Request $query);
    public function getPostsBy(Club $club, Request $query);
    // public function countPosts(Model $query);
}
