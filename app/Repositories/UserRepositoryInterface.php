<?php 
namespace App\Repositories;

interface UserRepositoryInterface
{
    public function find($user);
    public function feed($user);
}
