<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Slider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       \Schema::defaultStringLength(191);
        try{
            $sliders = Slider::where('status', 1)->get();
            \View::share('sliders', $sliders);
            \View::share('info', \App\Models\Info::first());
        }catch(\Exception $e)
        {

        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
