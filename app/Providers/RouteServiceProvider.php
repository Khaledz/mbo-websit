<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use App\Models\User;
use App\Models\Club;
use App\Models\Event;
use App\Models\EventCategory;
use App\Models\Announcement;
use App\Models\Post;
use App\Models\Slider;
use App\Models\Gallery;
use App\Models\Activity;
use App\Models\Faq;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();

        Route::bind('user', function ($value) {
            return User::where('username', $value)->firstOrFail();
        });

        Route::bind('club', function ($value) {
            return Club::where('slug', $value)->firstOrFail();
        });

        Route::bind('category', function ($value) {
            return EventCategory::where('id', $value)->firstOrFail();
        });

        Route::bind('event', function ($value) {
            return Event::where('slug', $value)->firstOrFail();
        });
        
        Route::bind('announcement', function ($value) {
            return Announcement::where('title', $value)->firstOrFail();
        });

        Route::bind('post', function ($value) {
            return Post::where('slug', $value)->firstOrFail();
        });

        Route::bind('slider', function ($value) {
            return Slider::where('name', $value)->firstOrFail();
        });

        Route::bind('gallery', function ($value) {
            return Gallery::where('id', $value)->firstOrFail();
        });

        Route::bind('activity', function ($value) {
            return Activity::where('id', $value)->firstOrFail();
        });

        Route::bind('faq', function ($value) {
            return Faq::where('id', $value)->firstOrFail();
        });
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
