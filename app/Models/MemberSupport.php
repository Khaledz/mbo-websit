<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberSupport extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];
    
    public function clubs()
    {
        return $this->belongsToMany('App\Models\Club');
    }
}