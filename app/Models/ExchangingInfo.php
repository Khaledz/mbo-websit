<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExchangingInfo extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The table
     *
     * @var array
     */
     protected $table = 'exchanging_info';
    
    public function clubs()
    {
        return $this->belongsToMany('App\Models\Club');
    }
}