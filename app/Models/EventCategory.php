<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventCategory extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'avatar'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'event_categories';

    public $timestamps = false;

    public function events()
    {
        return $this->hasMany('App\Models\Event', 'event_category_id');
    }
}