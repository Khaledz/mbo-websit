<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'attachments';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'orginal_name', 'description', 'extention', 'model', 'model_id', 'user_id'];

    public function model()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\AttachmentType');
    }

    public function event()
    {
        return $this->belongsTo('App\Models\Event', 'model_id');
    }

    public function gallery()
    {
        return $this->belongsTo('App\Models\Gallery', 'model_id');
    }
}
