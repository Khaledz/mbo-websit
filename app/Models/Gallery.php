<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Gallery extends Model
{
    /**
     * The table
     *
     * @var string
     */
     protected $table = 'galleries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'model', 'model_id', 'date', 'user_id'];

    protected $appends = ['year'];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
     public function getRouteKeyName()
     {
         return 'id';
     }
    
    public function model()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function event()
    {
        return $this->belongsTo('App\Models\Event', 'model_id');
    }

    public function attachments()
    {
        return $this->morphMany('App\Models\Attachment', 'model', 'model');
    }

    public function thumb()
    {
        foreach($this->morphMany('App\Models\Attachment', 'model', 'model')->get() as $file)
        {
            if($file->extention == 'png' || $file->extention == 'jpg' || $file->extention == 'jpeg')
            {
                return asset('storage/galleries/' . $this->id .'/'. $file->name);
            }
        }

        return asset('app/img/no_image_thumb.png');
    }

    public function getDateAttribute($date)
    {
        return Carbon::parse($date)->toDateString();
    }

    public function getYearAttribute($date)
    {
        return Carbon::parse($this->date)->year;
    }
}