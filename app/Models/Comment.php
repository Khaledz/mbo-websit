<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'comments';

    /**
     * Count the votes for post.
     *
     * @var array
     */
    // protected $withCount = ['votes'];

    /**
     * Load the needed related models
     *
     * @var array
     */
    protected $with = ['user'];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['content', 'user_id', 'model', 'model_id', 'parent_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function replies()
    {
        return $this->hasMany($this, 'parent_id');
    }

    public function model()
    {
        return $this->morphTo();
    }

    public function attachments()
    {
        return $this->morphMany('App\Models\Attachment', 'model', 'model');
    }

    public function post()
    {
        return $this->belongsTo('App\Models\Post', 'model_id');
    }
}