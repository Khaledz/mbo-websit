<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'launched', 'slug', 'goals', 'club_category_id', 'status', 'user_id'];
    
    protected $with = ['posts', 'members'];
    
    /**
     * Get the route key for the model.
     *
     * @return string
     */
     public function getRouteKeyName()
     {
         return 'slug';
     }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function alliances()
    {
        return $this->belongsToMany('App\Models\Alliance', 'club_alliances');
    }

    public function exchanging_info()
    {
        return $this->belongsToMany('App\Models\ExchangingInfo', 'club_exchanging_info');
    }

    public function member_supports()
    {
        return $this->belongsToMany('App\Models\MemberSupport', 'club_member_supports');
    }

    public function networkings()
    {
        return $this->belongsToMany('App\Models\Networking', 'club_networkings');
    }

    public function sharing_experiences()
    {
        return $this->belongsToMany('App\Models\SharingExperience', 'club_sharing_experiences');
    }

    public function members()
    {
        return $this->belongsToMany('App\Models\User', 'club_members');
    }

    public function champions()
    {
        return $this->belongsToMany('App\Models\User', 'club_champions');
    }

    public function pendingMembers()
    {
        return $this->belongsToMany('App\Models\User', 'club_members')->where('status', 0);
    }

    public function posts()
    {
        return $this->morphMany('App\Models\Post', 'model', 'model');
    }

    public function getLastPost()
    {
        return $this->hasOne('App\Models\Post', 'model_id')->latest();
    }

    public function isChampion($user)
    {
        return $this->belongsToMany('App\Models\User', 'club_champions')
        ->where('user_id', $user->id)->exists();
    }
}