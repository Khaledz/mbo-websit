<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarketPlace extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'slug', 'user_id', 'logo', 'status'];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
     public function getRouteKeyName()
     {
         return 'slug';
     }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function attachments()
    {
        return $this->morphMany('App\Models\Attachment', 'model', 'model');
    }

    public function comments()
    {
        return $this->morphMany('App\Models\Comment', 'model', 'model');
    }
}