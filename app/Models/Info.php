<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Cores\Settings\User as AppSettings;
use App\Cores\Settings\Faq;

class Info extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['settings'];

    protected $table = 'info';

    public $timestamps = false;

    /**
     * The list of attributes to cast.
     *
     * @var array
     */
    protected $casts = [
        'settings' => 'json',
    ];

    public function settings()
    {
        return new AppSettings($this->settings, $this);
    }
}