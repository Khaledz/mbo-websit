<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts';

    /**
     * Load the needed related models
     *
     * @var array
     */
    protected $with = ['user', 'comments'];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['content', 'views', 'slug', 'title', 'status', 'model', 'model_id', 'user_id'];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
    
    public function model()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function comments()
    {
        return $this->morphMany('App\Models\Comment', 'model', 'model');
    }

    public function attachments()
    {
        return $this->morphMany('App\Models\Attachment', 'model', 'model');
    }

    public function club()
    {
        return $this->belongsTo('App\Models\Club', 'model_id');
    }
}
