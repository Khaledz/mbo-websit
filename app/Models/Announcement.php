<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'user_id'];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
     public function getRouteKeyName()
     {
         return 'title';
     }
    
    public function user()
    {
        return $this->belongsT('App\Models\User');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'announcement_users');
    }

    public function attachments()
    {
        return $this->morphMany('App\Models\Attachment', 'model', 'model');
    }

    public function getDescriptionAttribute($value)
    {
        return html_entity_decode($value);
    }
}