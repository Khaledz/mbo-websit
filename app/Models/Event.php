<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Event extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'date', 'slug', 'user_id', 'time', 'speaker', 'location', 'note', 'program', 'event_category_id', 'status'];

     /**
     * The list of attributes to cast.
     *
     * @var array
     */
    protected $casts = [
        'status' => 'int',
    ];

    protected $appends = ['year'];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
     public function getRouteKeyName()
     {
         return 'slug';
     }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\EventCategory', 'event_category_id');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'event_users')->withPivot('status', 'note', 'answered');
    }

    public function attachments()
    {
        return $this->morphMany('App\Models\Attachment', 'model', 'model');
    }

    public function getDateAttribute($date)
    {
        return Carbon::parse($date)->toDateString();
    }
    
    public function galleries()
    {
        return $this->morphMany('App\Models\Gallery', 'model', 'model');
    }

    public function getYearAttribute($date)
    {
        return Carbon::parse($this->date)->year;
    }
}