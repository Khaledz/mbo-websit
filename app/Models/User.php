<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Cores\Settings\User as UserSettings;
use App\Traits\NewUser;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, NewUser, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'username', 'settings', 'status', 'phone', 'fname', 'lname', 'invite', 'champion', 'joined'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The list of attributes to cast.
     *
     * @var array
     */
    protected $casts = [
        'settings' => 'json',
        'status' => 'int'
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'username';
    }

    public function settings()
    {
        return new UserSettings($this->settings, $this);
    }

    public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }

    public function events()
    {
        return $this->belongsToMany('App\Models\Event', 'event_users');
    }

    public function clubs()
    {
        return $this->belongsToMany('App\Models\Club', 'club_members');
    }

    public function members()
    {
        return $this->belongsToMany('App\Models\User', 'club_members');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }

    public function attachments()
    {
        return $this->hasMany('App\Models\Attachment');
    }

    public function albums()
    {
        return $this->hasMany('App\Models\Album');
    }

    public function avatar()
    {
        return ($this->settings()->get('avatar') != '') ? asset('storage/users/' . $this->username . '/' . $this->settings()->get('avatar')) : asset('app/img/no-avatar.png');
    }

    public function getFullNameAttribute()
    {
       return ucfirst($this->fname) . ' ' . ucfirst($this->lname);
    }

    public function isJoinedClub(Club $club)
    {
        if($club->members()->where('user_id', $this->id)->where('club_members.status', 1)->exists())
        {
            return 0;
        }

        if($club->members()->where('user_id', $this->id)->where('club_members.status', '!=', 1)->exists())
        {
            return 1;
        }

        if(! $club->members()->where('user_id', $this->id)->exists()
        && ! $club->champions()->where('user_id', $this->id)->exists())
        {
            return 2;
        }
    }

    public function markets()
    {
        return $this->hasMany('App\Models\MarketPlace', 'user_id');
    }
}
