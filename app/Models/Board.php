<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Board extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['from', 'to', 'status'];
        
    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'board_users')->withPivot('position');
    }

    public function getFromAttribute($date)
    {
        return Carbon::parse($date)->toDateString();
    }

    public function getToAttribute($date)
    {
        return Carbon::parse($date)->toDateString();
    }
}