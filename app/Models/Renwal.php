<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Renwal extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'content', 'via', 'user_id'];
    
    /**
     * Get the route key for the model.
     *
     * @return string
     */
     public function getRouteKeyName()
     {
         return 'title';
     }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'renwal_users');
    }    
}