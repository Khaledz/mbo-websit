<?php

namespace App\Mail\Membership;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;

class WelcomeUser extends Mailable implements ShouldQueue 
{
    use Queueable, SerializesModels;

    /**
     * The user instance
     *
     * @return App\Models\User
     */
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Weclome to MBO');
        return $this->markdown('mail.membership.welcome_user');
    }
}
