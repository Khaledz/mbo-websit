<?php

namespace App\Mail\Membership;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MembershipRenwal extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The user instance
     *
     * @return App\Models\User
     */
     public $user;


     public function __construct($user)
     {
         $this->user = $user;
     }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Renew your membership - MBO');
        $this->markdown('mail.membership.membership_renwal');
    }
}
