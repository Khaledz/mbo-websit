<?php

namespace App\Mail\Membership;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AnnouncementMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * The user instance
     *
     * @return App\Models\User
     */
     public $user;

     /**
     * The announcement instance
     *
     * @return App\Models\Announcement
     */
     public $announcement;

     public function __construct($announcement, $user)
     {
         $this->user = $user;
         $this->announcement = $announcement;
     }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Announcement from MBO');
        // if sender is Mr. Khaled Gama, then change the global from and address of the email.
        if(auth()->user() && auth()->user()->id == 3)
        {
            $this->subject('Announcement from champion Khaled Gama - mbo');
        }
        
        // if($this->announcement->attachments->count() > 0)
        // {
        //     $email = $this->markdown('mail.membership.send_announcement')->subject($this->announcement->title .' - MBO Website');
        //     foreach($this->announcement->attachments as $attachment)
        //     {
        //         return $email->attach(asset('storage/announcements/'. $this->announcement->title. '/'. $attachment->name));
        //     }
        //     return $email;
        // }
        return $this->markdown('mail.membership.send_announcement');
    }
}
