<?php

namespace App\Mail\Membership;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Event;
use App\Models\User;

class EventInvitation extends Mailable
{
    use Queueable, SerializesModels;

    public $event;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Event $event, User $user)
    {
        $this->event = $event;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('MBO has sent you invitation');
        return $this->markdown('mail.membership.event_invite');
    }
}
