<?php

namespace App\Policies;

use App\Models\Post;
use App\Models\User;
use App\Models\Club;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the club.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Club  $club
     * @return mixed
     */
     public function view(User $user, Post $post)
     {
         if($user->hasRole('admin'))
         {
             return true;
         }
         
         // if user is champios of this club or member of club
         if($post->club->members()->where('user_id', $user->id)->where('club_members.status', 1)->count() > 0)
         {
             return true;
         }
 
         if($post->club->champions()->where('user_id', $user->id)->count() > 0)
         {
             return true;
         }
 
         return false;
     }
 
     /**
      * Determine whether the user can create clubs.
      *
      * @param  \App\Models\User  $user
      * @return mixed
      */
     public function create(User $user)
     {
        //  if($user->hasRole('admin'))
        //  {
        //      return true;
        //  }
 
         return false;
     }
 
     /**
      * Determine whether the user can update the club.
      *
      * @param  \App\Models\User  $user
      * @param  \App\Models\Club  $club
      * @return mixed
      */
     public function update(User $user, Club $club)
     {
        //  if($club->champions()->where('user_id', $user->id)->count() > 0)
        //  {
        //      return true;
        //  }
 
         return false;
     }
 
     /**
      * Determine whether the user can delete the club.
      *
      * @param  \App\Models\User  $user
      * @param  \App\Models\Club  $club
      * @return mixed
      */
     public function delete(User $user, Club $club)
     {
        //  if($club->champions()->where('user_id', $user->id)->count() > 0)
        //  {
        //      return true;
        //  }
     }
}
