<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MarketPlace;
use App\Models\Attachment;

class MarketPlacesController extends Controller
{
    public function index()
    {
        $markets = MarketPlace::where('status', 1)->latest()->paginate(15);

        return view('app.markets.index', compact('markets'));
    }

    public function show(MarketPlace $market)
    {
        return view('app.markets.show', compact('market'));
    }

    public function create()
    {
    	return view('app.markets.create');
    }

    public function store(Request $request)
    {
    	$this->validate($request, 
        [
            'name' => 'required|min:3|unique:market_places,name',
            'description' => 'required',
        ]);

        $market = MarketPlace::create(array_merge($request->all(), ['user_id' => auth()->user()->id, 'slug' => str_slug($request->name)]));
        
        if($request->hasFile('logo'))
        {
            if($request->file('logo')->isValid())
            {
                $file = $request->file('logo');
                $file->store('market_places/logos/'. $market->id);
                $attachment = Attachment::create(['name' => $file->hashName(), 'orginal_name' => $file->getClientOriginalName(), 'extention' => $file->extension(), 'user_id' => auth()->user()->id, 'description' => $request->description_of_file]);
                $market->attachments()->save($attachment);
                $market->update(['logo' => $file->hashName()]);
            }
        }

        if($request->hasFile('files'))
        {
            foreach($request->file('files') as $file)
            {
                if($file->isValid())
                {
                    $file->store('market_places/'. $market->id);
                    $attachment = Attachment::create(['name' => $file->hashName(), 'orginal_name' => $file->getClientOriginalName(), 'extention' => $file->extension(), 'user_id' => auth()->user()->id, 'description' => $request->description_of_file]);
                    $market->attachments()->save($attachment);
                }
            }
        }

        return redirect()->route('app.market.index')
        ->with('message_header', 'The market place has been created successfully.')
        ->with('message_body', "We'll review your post before we publish it.");
    }

    public function edit(MarketPlace $market)
    {
    	
    }

    public function update(Request $request, $market)
    {
    	
    }

    public function destroy(MarketPlace $market)
    {
    	
    }
}