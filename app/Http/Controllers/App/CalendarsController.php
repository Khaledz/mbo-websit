<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\EventCategory;
use App\Models\User;
use Carbon\Carbon;

class CalendarsController extends Controller
{
    public function index()
    {
        return view('app.calendars.index');
    }

    public function show()
    {
        $user = auth()->user();
        $events = Event::whereHas('users', function($query) use ($user){
            return $query->where('user_id', $user->id);
        })->get();

        if($events->count() > 0)
        {
            foreach($events as $event)
            {
                $eventsData[] = 
                [
                    'id' => $event->id,
                    'title' => $event->name, 
                    'start' => $event->date,
                    'url' => route('app.event.show', $event),
                    'backgroundColor' => '#4a90e2',
                ];
            }
            return (json_encode($eventsData)) ;
        }
    }
}