<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Post;
use App\Models\Attachment;

class PostsCommentsController extends Controller
{
    public function index()
    {
        
    }

    public function show(Post $post, Comment $comment)
    {
        
    }

    public function create(Post $post, Comment $comment)
    {
    	
    }

    public function store(Post $post, Request $request)
    {
    	$this->validate($request, 
        [
            'content' => 'required',
        ]);

    	$comment = Comment::create(array_merge($request->all(), ['user_id' => auth()->user()->id]));

        $post->comments()->save($comment);

        if($request->hasFile('files'))
        {
            foreach($request->file('files') as $file)
            {
                if($file->isValid())
                {
                    $file->store('clubs/' .$post->club->id. '/posts/' . $post->id .'/'. $comment->id);
                    $attachment = Attachment::create(['name' => $file->hashName(), 'orginal_name' => $file->getClientOriginalName(), 'extention' => $file->extension(), 'user_id' => auth()->user()->id]);
                    $comment->attachments()->save($attachment);
                }
            }
        }

        return redirect()->route('app.club.post.show', [$post->club, $post])
                ->with('message_header', 'Your comment has been sent!');
    }

    public function edit(Club $club, Post $post)
    {
    	return view('app.posts.edit', compact('post','club'));
    }

    public function update(Request $request, Club $club, Post $post)
    {
    	$this->validate($request, 
        [
            'name' => 'required|min:3|unique:clubs,name,'.$club->id,
            'description' => 'required',
            'launched' => 'required',
            'networking_id' => 'exists:networkings,id',
            'alliance_id' => 'exists:alliances,id',
            'exchanging_info_id' => 'exists:exchanging_info,id',
            'member_support_id' => 'exists:member_supports,id',
            'sharing_experience_id' => 'exists:sharing_experiences,id',
        ]);

    	return redirect()->route('app.posts.index')->with('message', 'The club has been edited successfully.');
    }

    public function destroy(Post $post, Comment $comment)
    {
    	$comment->delete();
    	return redirect()->back()->with('message_header', 'The comment has been removed successfully.');
    }
}