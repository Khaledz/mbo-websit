<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Club;
use App\Models\Post;
use App\Models\Attachment;

class ClubsPostsController extends Controller
{
    public function index()
    {
        
    }

    public function show(Club $club, Post $post)
    {
        $this->authorize($post, $club);
        $post->update(['views' => $post->views + 1]);
        return view('app.posts.show', compact('club', 'post'));
    }

    public function create(Club $club)
    {
    	return view('app.posts.create', compact('club'));
    }

    public function store(Club $club, Request $request)
    {
    	$this->validate($request, 
        [
            'title' => 'required|min:3',
            'content' => 'required',
        ]);

    	$post = Post::create(array_merge($request->all(), ['user_id' => auth()->user()->id, 'slug' => str_slug($request->title), 'status' => 1]));

        $club->posts()->save($post);

        if($request->hasFile('files'))
        {
            foreach($request->file('files') as $file)
            {
                if($file->isValid())
                {
                    $file->store('clubs/' .$club->id. '/posts/' . $post->id);
                    $attachment = Attachment::create(['name' => $file->hashName(), 'orginal_name' => $file->getClientOriginalName(), 'extention' => $file->extension(), 'user_id' => auth()->user()->id]);
                    $post->attachments()->save($attachment);
                }
            }
        }

        return redirect()->route('app.club.post.show', [$club, $post])
                ->with('message_header', 'Your post has been sent!');
    }

    public function edit(Club $club, Post $post)
    {
    	return view('app.posts.edit', compact('post','club'));
    }

    public function update(Request $request, Club $club, Post $post)
    {
    	$this->validate($request, 
        [
            'name' => 'required|min:3|unique:clubs,name,'.$club->id,
            'description' => 'required',
            'launched' => 'required',
            'networking_id' => 'exists:networkings,id',
            'alliance_id' => 'exists:alliances,id',
            'exchanging_info_id' => 'exists:exchanging_info,id',
            'member_support_id' => 'exists:member_supports,id',
            'sharing_experience_id' => 'exists:sharing_experiences,id',
        ]);

    	return redirect()->route('app.posts.index')->with('message', 'The club has been edited successfully.');
    }

    public function destroy(Club $club, Post $post)
    {
    	$post->delete();
    	return redirect()->route('app.club.show', $club)->with('message_header', 'The post has been removed successfully.');
    }
}