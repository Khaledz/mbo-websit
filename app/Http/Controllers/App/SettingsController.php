<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

class SettingsController extends Controller
{
    public function show(User $user)
    {
        return view('app.settings.show', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $this->validate($request, 
            [
                'fname' => 'required',
                'lname' => 'required',
                'email' => 'required|email',
                'website' => 'URL|nullable'
            ]
        );
        
        $user->settings()->merge($request->only(['sector', 'job', 'children', 'wife', 'birthday', 'website', 'company', 'linkedin', 'public_profile', 'personal_assistant_email', 'personal_assistant_name', 'personal_assistant_phone']));
        $user->update($request->only(['fname', 'lname', 'email']));
        
        if(! is_null($request->password))
        {
            $user->update(['password' => bcrypt($request->password)]);
        }

        return back()->with('message_header', 'Your data has been saved successfully.');
    }
}
