<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\EventCategory;
use App\Models\User;
use App\Models\Attachment;
use Carbon\Carbon;

class EventsController extends Controller
{
    public function index(EventCategory $category)
    {        
        $user = auth()->user();
        $upcomings = Event::whereHas('users', function($query) use ($user){
                return $query->where('user_id', $user->id);
            })->whereDate('date' , '>=' , Carbon::now()->toDateString())->where('time', '>=', date('g:i A'))->get();

        $categories = EventCategory::with(['events' => function($query) use ($user){
            return $query->whereHas('users', function($query) use ($user){
                return $query->where('user_id', $user->id);
            });
        }])->get();

        return view('app.events.index', compact('upcomings', 'categories'));
    }

    public function show(Event $event)
    {
        $user = \DB::table('event_users')->where('user_id', auth()->user()->id)->where('event_id', $event->id)->first();
        if(! is_null($user))
        {
            return view('app.events.show', compact('event', 'user'));
        }
        return view('app.events.show', compact('event'));
    }

    public function create()
    {
        $categories = EventCategory::pluck('name', 'id');
        $users = User::all()->pluck('FullName', 'id');    

    	return view('app.events.create', compact('categories', 'users'));
    }

    public function store(Request $request)
    {
    	$this->validate($request, 
        [
            'event_category_id' => 'exists:event_categories,id',
            'name' => 'required|min:3|unique:events,name',
            'description' => 'required',
            'date' => 'required|date',
            'time' => 'required',
            'speaker' => 'required|min:3',
            'location' => 'required|min:3',
            'note' => 'min:3',
            'program' => 'min:3'
        ]);
        
        $event = Event::create(array_merge($request->all(), ['user_id' => auth()->user()->id, 'slug' => str_slug($request->name)]));
        
        if($request->hasFile('files'))
        {
            foreach($request->file('files') as $file)
            {
                if($file->isValid())
                {
                    $file->store('events/'. $request->name);
                    $attachment = Attachment::create(['name' => $file->hashName(), 'orginal_name' => $file->getClientOriginalName(), 'extention' => $file->extension(), 'user_id' => auth()->user()->id]);
                    $event->attachments()->save($attachment);
                }
            }
        }

        if($request->invite == 'all')
        {
            $users = User::role('membership')->get();
            $token = str_random(10);
            $event->users()->attach($users, ['token' => $token]);
        }

        if(isset($request->users) && $request->invite == 'custom')
        {
            $token = str_random(10);
            $event->users()->attach($request->users, ['token' => $token]);
        }

        if($request->via == 'all')
        {
            event(new EventWasPublished($event, $token));
            var_dump('The email and sms has been sent to all receivers');
        }

        if($request->via == 'email')
        {
            var_dump('The email has been sent to all receivers');
        }

        if($request->via == 'sms')
        {
            var_dump('The sms has been sent to all receivers');
        }

    	return redirect()->route('app.event.index')->with('message', 'The event has been created successfully.');
    }

    public function edit($event)
    {
        $categories = EventCategory::pluck('name', 'id');
        $users = User::all()->pluck('FullName', 'id'); 

    	return view('app.events.edit', compact('event', 'categories', 'users'));
    }

    public function update(Request $request, $event)
    {
    	$this->validate($request, 
        [
            'event_category_id' => 'exists:event_categories,id',
            'name' => 'required|min:3|unique:events,name,'. $event->id,
            'description' => 'required',
            'date' => 'required|date',
            'time' => 'required',
            'speaker' => 'required|min:3',
            'location' => 'required|min:3',
        ]);

        $event->update($request->all());
        
        if($request->hasFile('files'))
        {
            foreach($request->file('files') as $file)
            {
                if($file->isValid())
                {
                    $file->store('events/'. $request->name);
                    $attachment = Attachment::create(['name' => $file->hashName(), 'orginal_name' => $file->getClientOriginalName(), 'extention' => $file->extension(), 'user_id' => auth()->user()->id]);
                    $event->attachments()->save($attachment);
                }
            }
        }

        if($request->invite == 'all')
        {
            $users = User::role('membership')->get();
            $event->users()->sync($users);
        }

        if($request->invite == 'custom')
        {
            $event->users()->sync($request->users);
        }

    	return redirect()->route('app.event.index')->with('message', 'The event has been edited successfully.');
    }

    public function destroy(Event $event)
    {
    	$event->delete();
    	return redirect()->route('app.event.index')->with('message', 'The Event has been deleted successfully');
    }

    public function attend(Request $request, Event $event)
    {
        $this->validate($request, 
        [
            'status' => 'required|integer'
        ]);

        $event->users()->where('user_id', auth()->user()->id)->update(['note' => ($request->note) ?: $request->note, 'event_users.status' => $request->status, 'answered' => 1]);
        return back()->with('message_header', 'Your answer has been submitted successfully.');
    }

    public function change(Event $event)
    {
        $event->users()->where('user_id', auth()->user()->id)->update(['answered' => 0]);
        return back();
    }

    public function allMembers(Event $event)
    {
        return view('app.events.all_members', compact('event'));
    }
}