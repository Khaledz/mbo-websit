<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\MarketPlace;
use App\Models\Attachment;

class MarketsCommentsController extends Controller
{
    public function index()
    {
        
    }

    public function show(MarketPlace $market, Comment $comment)
    {
        
    }

    public function create(MarketPlace $market, Comment $comment)
    {
    	
    }

    public function store(MarketPlace $market, Request $request)
    {
    	$this->validate($request, 
        [
            'content' => 'required',
        ]);

    	$comment = Comment::create(array_merge($request->all(), ['user_id' => auth()->user()->id]));

        $market->comments()->save($comment);

        if($request->hasFile('files'))
        {
            foreach($request->file('files') as $file)
            {
                if($file->isValid())
                {
                    $file->store('/market_places/'.$market->id. '/comments/'. $comment->id);
                    $attachment = Attachment::create(['name' => $file->hashName(), 'orginal_name' => $file->getClientOriginalName(), 'extention' => $file->extension(), 'user_id' => auth()->user()->id]);
                    $comment->attachments()->save($attachment);
                }
            }
        }

        return redirect()->route('app.market.show', [$market])
                ->with('message_header', 'Your comment has been sent!');
    }

    public function edit(MarketPlace $market, Comment $comment)
    {
    }

    public function update(Request $request, MarketPlace $market, Comment $comment)
    {
    }

    public function destroy(MarketPlace $market, Comment $comment)
    {
    }
}