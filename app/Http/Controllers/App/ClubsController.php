<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Club;
use App\Models\Networking;
use App\Models\Alliance;
use App\Models\ExchangingInfo;
use App\Models\MemberSupport;
use App\Models\SharingExperience;
use App\Models\User;
use Spatie\Permission\Models\Role;

class ClubsController extends Controller
{
    public function index()
    {
        $clubs = Club::where('status', 1)->paginate(10);

        return view('app.clubs.index', compact('clubs'));
    }

    public function show(Club $club)
    {
        $this->authorize($club);
        
        return view('app.clubs.show', compact('club'));
    }

    public function create()
    {
        $networkings = Networking::pluck('name', 'id');
        $alliances = Alliance::pluck('name', 'id');
        $member_supports = MemberSupport::pluck('name', 'id');
        $exchanging_info = ExchangingInfo::pluck('name', 'id');
        $sharing_experiences = SharingExperience::pluck('name', 'id');     
        $users = User::all()->pluck('FullName', 'id');

    	return view('app.clubs.create', compact('networkings', 'member_supports', 'alliances', 'exchanging_info', 'sharing_experiences', 'users'));
    }

    public function store(Request $request)
    {
    	$this->validate($request, 
        [
            'name' => 'required|min:3|unique:clubs,name',
            'description' => 'required',
            'launched' => 'required',
            'networking_id' => 'exists:networkings,id',
            'alliance_id' => 'exists:alliances,id',
            'exchanging_info_id' => 'exists:exchanging_info,id',
            'member_support_id' => 'exists:member_supports,id',
            'sharing_experience_id' => 'exists:sharing_experiences,id'
        ]);

    	$club = Club::create(array_merge($request->all(), ['user_id' => auth()->user()->id, 'slug' => str_slug($request->name), 'status' => 1]));

        $club->alliances()->attach($request->alliances_id);
        $club->exchanging_info()->attach($request->exchanging_info_id);
        $club->member_supports()->attach($request->member_support_id);
        $club->networkings()->attach($request->networking_id);
        $club->sharing_experiences()->attach($request->sharing_experience_id);
        $club->save();

        // Assign members.
        $club->users()->attach($request->users, ['status' => 1]);

        // Assign champions
        if(isset($request->champions))
        {
            // Assign roles
            foreach($request->champions as $champion)
            {
                User::find($champion)->assignRole('champion');
            }
            $club->users()->attach($request->champions, ['champion' => 1, 'status' => 1]);
        }

    	return redirect()->route('app.club.index')->with('message', 'The club has been created successfully.');
    }

    public function edit($club)
    {
        $networkings = Networking::pluck('name', 'id');
        $alliances = Alliance::pluck('name', 'id');
        $member_supports = MemberSupport::pluck('name', 'id');
        $exchanging_info = ExchangingInfo::pluck('name', 'id');
        $sharing_experiences = SharingExperience::pluck('name', 'id');
        $users = User::all()->pluck('FullName', 'id');

    	return view('app.clubs.edit', compact('users','club', 'networkings', 'member_supports', 'alliances', 'exchanging_info', 'sharing_experiences'));
    }

    public function update(Request $request, $club)
    {
    	$this->validate($request, 
        [
            'name' => 'required|min:3|unique:clubs,name,'.$club->id,
            'description' => 'required',
            'launched' => 'required',
            'networking_id' => 'exists:networkings,id',
            'alliance_id' => 'exists:alliances,id',
            'exchanging_info_id' => 'exists:exchanging_info,id',
            'member_support_id' => 'exists:member_supports,id',
            'sharing_experience_id' => 'exists:sharing_experiences,id',
        ]);

    	$club->update($request->all());

        $club->alliances()->sync($request->alliances_id);
        $club->exchanging_info()->sync($request->exchanging_info_id);
        $club->member_supports()->sync($request->member_support_id);
        $club->networkings()->sync($request->networking_id);
        $club->sharing_experiences()->sync($request->sharing_experience_id);
        $club->save();

    	return redirect()->route('app.club.index')->with('message', 'The club has been edited successfully.');
    }

    public function destroy(Club $club)
    {
    	$club->delete();
    	return redirect()->route('app.club.index')->with('message', 'The club has been deleted successfully');
    }

    public function join(Request $request)
    {
        if($request->ajax())
        {
            $join = Club::findOrFail($request->club_id)->members()->attach(auth()->user()->id);
            return response()->json(['message' => 'Request sent!', 200]);
        }
    }
}