<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Slider;
use App\Models\Attachment;
use App\Models\EventCategory;
use App\Models\Board;
use App\Models\Activity;
use App\Models\User;
use App\Models\Event;
use App\Models\Faq;
use Mail;
use App\Mail\Membership\ContactUs;
use Carbon\Carbon;

class HomesController extends Controller
{
    public function index()
    {
        (new \App\Cores\Communications\SMS\Nexmo())->to(User::first())->text(strip_tags('عربي عربي'))->send();
        $sliders = Slider::where('status', 1)->get();
        $attachments = Attachment::with('gallery')->inRandomOrder()->take(6)
        ->where('model', 'App\Models\Gallery')->whereIn('extention', ['png', 'jpg', 'jpeg'])->get();

        $activities = Activity::get();
        
        // Archive old events.
        Event::whereDate('date' , '<' , Carbon::now()->toDateString())->update(['status' => 0]);

    	if(Auth::guest())
        {
        	return view('app.home.index', compact('sliders', 'attachments', 'activities'));
        }

        $user = auth()->user();
        $upcomings = Event::whereHas('users', function($query) use ($user){
                return $query->where('user_id', $user->id);
            })->whereDate('date' , '>=' , Carbon::now()->toDateString())->get();

        return view('app.home.member', compact('sliders', 'attachments', 'activities', 'upcomings'));
    }

    public function contact()
    {
        return view('app.home.contact');
    }

    public function value()
    {
    	return view('app.home.value');
    }

    public function directory()
    {
        $board = Board::where('status', 1)->first();

        $boards = Board::where('status', 0)->get();

        return view('app.home.directory', compact('board', 'boards'));
    }

    public function faq()
    {
        $faqs = Faq::all();
        return view('app.home.faq', compact('faqs'));
    }

    public function post_contact(Request $request)
    {
        $this->validate($request,['name' => 'required', 'message' => 'required', 'email' => 'required|email']);

        Mail::to(config('mail.from.address'))->send(new ContactUs($request->all()));

        return redirect()->route('app.home.index')->with('message_header', 'Your message has been sent!')->with('message_body', "We'll contact you during 24 hours.");
    }

    public function terms()
    {
        return view('app.home.terms');
    }

    public function list_members()
    {
        $users = User::get();
        return view('app.home.members', compact('users'));
    }

    public function about()
    {
        $board = Board::where('status', 1)->first();

        $boards = Board::where('status', 0)->get();

        return view('app.home.about', compact('board', 'boards'));
    }
}
