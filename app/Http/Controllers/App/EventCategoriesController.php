<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EventCategory;
use Carbon\Carbon;

class EventCategoriesController extends Controller
{
    // Get all the dates of event
    public function index(EventCategory $category)
    {
        // get the available years.
        $dates = collect();
        foreach($category->events as $event)
        {
            $dates->push(Carbon::parse($event->date)->year);
        }
        $dates = $dates->unique();

        return view('app.categories.index', compact('dates', 'category'));
    }

    public function show(EventCategory $category)
    {
        return view('App.event_categories.show', compact('category'));
    }

    public function create()
    {
        $categories = EventCategory::pluck('name', 'id');

    	return view('App.events.create', compact('categories'));
    }

    public function store(Request $request)
    {
    	$this->validate($request, 
        [
            'name' => 'required|min:3|unique:event_categories,name',
        ]);

    	EventCategory::create($request->all());

    	return redirect()->route('App.event_categories.index')->with('message', 'The category has been created successfully.');
    }

    public function edit(EventCategory $category)
    {
        $categories = EventCategory::pluck('name', 'id');

    	return view('App.event_categories.edit', compact('category', 'categories'));
    }

    public function update(Request $request, $category)
    {
    	$this->validate($request, 
        [
            'name' => 'required|min:3|unique:event_categories,name'. $category->id,
        ]);

    	$category->update($request->all());

    	return redirect()->route('App.event.index')->with('message', 'The category has been edited successfully.');
    }

    public function destroy(EventCategory $category)
    {
    	$category->delete();
    	return redirect()->route('App.event.index')->with('message', 'The category has been deleted successfully');
    }
}