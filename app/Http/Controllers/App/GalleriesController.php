<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Models\Event;
use App\Models\EventCategory;
use Carbon\Carbon;

class GalleriesController extends Controller
{
    public function index(EventCategory $category)
    {
        $galleries = Gallery::latest()->paginate(9);

        // get the available years.
        $dates = collect();
        foreach(Event::all() as $event)
        {
            $dates->push(Carbon::parse($event->date)->year);
        }
        $dates = $dates->unique();
       
        return view('app.galleries.index', compact('galleries', 'dates'));
    }

    public function show(Gallery $gallery)
    {
        return view('app.galleries.show', compact('gallery'));
    }

    public function create(Gallery $gallery)
    {
    	
    }

    public function store(MarketPlace $market, Request $request)
    {
    	$this->validate($request, 
        [
            'content' => 'required',
        ]);

    	$comment = Comment::create(array_merge($request->all(), ['user_id' => auth()->user()->id]));

        $market->comments()->save($comment);

        if($request->hasFile('files'))
        {
            foreach($request->file('files') as $file)
            {
                if($file->isValid())
                {
                    $file->store('/market_places/'.$market->name. '/'. $comment->id);
                    $attachment = Attachment::create(['name' => $file->hashName(), 'orginal_name' => $file->getClientOriginalName(), 'extention' => $file->extension(), 'user_id' => auth()->user()->id]);
                    $comment->attachments()->save($attachment);
                }
            }
        }

        return redirect()->route('app.market.show', [$market])
                ->with('message_header', 'Your comment has been sent!');
    }

    public function filterByYear(EventCategory $category, $year)
    {
        // Get galleries where its events by year and category
        $galleries = Gallery::with(['event' => function($query) use ($category){
            return $query->where('event_category_id', $category->id)->get();
        }])->get();

        $filters = collect();

        // remove gallery that its event is null
        foreach($galleries as $gallery)
        {
            if(is_null($gallery->event))
            {
                continue;
            }
            if(Carbon::parse($gallery->date)->year == $year)
            {
                $filters->push($gallery);
            }
        }

        return view('app.galleries.filter', compact('category', 'year', 'filters'));
    }

    public function update(Request $request, MarketPlace $market, Comment $comment)
    {
    }

    public function destroy(MarketPlace $market, Comment $comment)
    {
    }
}