<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Spatie\Permission\Models\Role;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::paginate(15);
        $roles = Role::all();

        return view('App.users.index', compact('users', 'roles'));
    }

    public function filter($role)
    {
    	$users = User::role($role)->paginate(15);
        $roles = Role::all();

    	return view('App.users.index', compact('users', 'roles'));
    }

    public function show(User $user)
    {
        if(is_null($user->settings()->get('public_profile')) && auth()->user()->id != $user->id && ! auth()->user()->hasRole(['admin', 'champion']))
        {
            return view('app.users.show');
        }

        return view('app.users.show', compact('user'));
    }

    public function create()
    {
    	$roles = Role::pluck('name', 'id');

    	return view('App.users.create', compact('roles'));
    }

    public function store(Request $request)
    {
    	$this->validate($request, 
        [
            'username' => 'required|min:3|unique:users,username',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6',
            'phone' => 'required|max:9|min:9|unique:users,phone',
            'fname' => 'required|min:3',
            'lname' => 'required|min:3',
            'role_id' => 'required|exists:roles,id'
        ]);

    	$user = User::create($request->all());
    	$user->assignRole(Role::find($request->role_id)->name);

    	return redirect()->route('App.user.index')->with('message', 'The user has been created successfully.');
    }

    public function edit($user)
    {
    	$roles = Role::pluck('name', 'id');

    	return view('App.users.edit', compact('roles', 'user'));
    }

    public function update(Request $request, $user)
    {
    	$this->validate($request, 
        [
            'username' => 'required|min:3|unique:users,username,'.$user->id,
            'email' => 'required|email|unique:users,email,'.$user->id,
            'phone' => 'required|max:9|min:9|unique:users,phone,'.$user->id,
            'fname' => 'required|min:3',
            'lname' => 'required|min:3',
            'role_id' => 'required|exists:roles,id'
        ]);

    	$user->update($request->all());
    	$user->syncRoles(Role::find($request->role_id)->name);
    	return redirect()->route('App.user.index')->with('message', 'The user has been edited successfully.');
    }

    public function destroy(User $user)
    {
    	$user->delete();
    	return redirect()->route('App.user.index')->with('message', 'The user has been deleted successfully');
    }

    public function uploadAvatar(User $user, Request $request)
    {
        if($request->hasFile('avatar'))
        {
            $file = $request->file('avatar');
            if($file->isValid())
            {
                $file->store('users/'. $user->username);
                $user->settings()->set('avatar', $file->hashName());
            }
        }
        
        return back()->with('message_header', 'Your avatar has been updated.');
    }
}