<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Announcement;
use App\Models\User;

class AttachmentsController extends Controller
{
    public function index()
    {
        $announcements = Announcement::paginate(15);

        return view('App.announcements.index', compact('announcements'));
    }

    public function show(Announcement $announcement)
    {
        return view('App.announcements.show', compact('announcement'));
    }

    public function create()
    {
        $users = User::all()->pluck('FullName', 'id');

    	return view('App.announcements.create', compact('users'));
    }

    public function store(Request $request)
    {
    	$this->validate($request, 
        [
            'title' => 'required|min:3',
            'description' => 'required|min:3',
        ]);
        
        $announcement = Announcement::create(array_merge($request->all(), ['user_id' => auth()->user()->id]));
        
        if($request->hasFile('files'))
        {
            foreach($request->file('files') as $file)
            {
                if($file->isValid())
                {
                    $file->store('announcements/'. $request->name);
                    $attachment = Attachment::create(['name' => $file->hashName(), 'orginal_name' => $file->getClientOriginalName(), 'extention' => $file->extension(), 'user_id' => auth()->user()->id]);
                    $announcement->attachments()->save($attachment);
                }
            }
        }

        if($request->invite == 'all')
        {
            $users = User::role('membership')->get();
            $announcement->users()->attach($users);
        }

        if(isset($request->users) && $request->invite == 'custom')
        {
            $announcement->users()->attach($request->users);
        }

    	return redirect()->route('app.announcement.index')->with('message', 'The announcement has been created successfully.');
    }

    public function edit($announcement)
    {
    	return view('App.announcements.edit');
    }

    public function update(Request $request, $announcement)
    {
    	$this->validate($request, 
        [
            'title' => 'required|min:3',
            'description' => 'required|min:3',
        ]);

        $announcement->update($request->all());
        
        if($request->hasFile('files'))
        {
            foreach($request->file('files') as $file)
            {
                if($file->isValid())
                {
                    $file->store('announcements/'. $request->name);
                    $attachment = Attachment::create(['name' => $file->hashName(), 'orginal_name' => $file->getClientOriginalName(), 'extention' => $file->extension(), 'user_id' => auth()->user()->id]);
                    $announcement->attachments()->save($attachment);
                }
            }
        }

    	return redirect()->route('App.announcement.index')->with('message', 'The announcement has been edited successfully.');
    }

    public function destroy(Announcement $announcement)
    {
    	$announcement->delete();
    	return redirect()->route('App.announcement.index')->with('message', 'The announcement has been deleted successfully');
    }
}