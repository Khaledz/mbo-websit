<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\EventCategory;
use App\Models\User;
use App\Models\Attachment;
use App\Models\Gallery;
use App\Events\EventWasPublished;
use Carbon\Carbon;
use App\Cores\Communications\SMS\Nexmo;

class EventsController extends Controller
{
    public function index()
    {
        $events = Event::latest()->paginate(15);

        return view('admin.events.index', compact('events'));
    }

    public function show(Event $event)
    {
        $users = \DB::table('event_users')
                ->select('*', 'event_users.status as status')
                ->join('users', 'users.id', '=', 'event_users.user_id')
                ->where('event_id', $event->id)->get();

        return view('admin.events.show', compact('event', 'users'));
    }

    public function create()
    {
        $categories = EventCategory::pluck('name', 'id');
        $users = User::all()->pluck('FullName', 'id');    

    	return view('admin.events.create', compact('categories', 'users'));
    }

    public function store(Request $request)
    {
    	$this->validate($request, 
        [
            'event_category_id' => 'exists:event_categories,id',
            'name' => 'required|min:3',
            'description' => 'required',
            'date' => 'required|date',
            'time' => 'required',
        ]);
        
        $event = Event::create(array_merge($request->all(), ['user_id' => auth()->user()->id, 'slug' => str_slug($request->name), 'date' => Carbon::parse($request->date)->toDateString()]));
        
        if($request->hasFile('files'))
        {
            // Add attachments to gallery
            $gallery = Gallery::create(array_merge($request->only(['name', 'description']), ['user_id' => auth()->user()->id, 'date' => $event->date]));
            $event->galleries()->save($gallery);
            
            foreach($request->file('files') as $file)
            {
                if($file->isValid())
                {
                    $file->store('galleries/'. $gallery->id);
                    $attachment = Attachment::create(['name' => $file->hashName(), 'orginal_name' => $file->getClientOriginalName(), 'extention' => $file->extension(), 'user_id' => auth()->user()->id]);
                    $gallery->attachments()->save($attachment);
                }
            }
        }

        if($request->invite == 'all')
        {
            $users = User::get();
            $event->users()->attach($users);
        }

        if(isset($request->users) && $request->invite == 'custom')
        {
            $event->users()->attach($request->users);
        }

        if($request->via == 'all')
        {
            event(new EventWasPublished($event, $event->users, ['email', 'sms']));
        }
        
        if($request->via == 'sms')
        {
            event(new EventWasPublished($event, $event->users, ['sms']));
        }
        
        if($request->via == 'email')
        {
            event(new EventWasPublished($event, $event->users, ['email']));
        }

    	return redirect()->route('admin.event.index')->with('message', 'The event has been created successfully.');
    }

    public function edit(Event $event)
    {
        $categories = EventCategory::pluck('name', 'id');
        $users = User::all()->pluck('FullName', 'id');

    	return view('admin.events.edit', compact('event', 'categories', 'users'));
    }

    public function update(Request $request, Event $event)
    {
    	$this->validate($request, 
        [
            'event_category_id' => 'exists:event_categories,id',
            'name' => 'required|min:3',
            'description' => 'required',
            'date' => 'required|date',
            'time' => 'required',
        ]);

        $event->update($request->all());
        
        if($request->hasFile('files'))
        {
            // Create gallery in case we are updating the event with uploading some images.
            if($event->galleries()->count() == 0)
            {
                // Add attachments to gallery
                $gallery = Gallery::create(array_merge($request->only(['name', 'description']), ['user_id' => auth()->user()->id, 'date' => $event->date]));
                $event->galleries()->save($gallery);
            }

            foreach($request->file('files') as $file)
            {
                if($file->isValid())
                {
                    $file->store('galleries/'. $event->galleries()->first()->id);
                    $attachment = Attachment::create(['name' => $file->hashName(), 'orginal_name' => $file->getClientOriginalName(), 'extention' => $file->extension(), 'user_id' => auth()->user()->id]);
                    $event->galleries()->first()->attachments()->save($attachment);
                }
            }
        }

        if($request->invite == 'all')
        {
            $users = User::get();
            $event->users()->sync($users);
        }

        if($request->invite == 'custom')
        {
            $event->users()->sync($request->users);
        }

        if($request->via == 'all')
        {
            event(new EventWasPublished($event, $event->users, ['email', 'sms']));
        }
        
        if($request->via == 'sms')
        {
            event(new EventWasPublished($event, $event->users, ['sms']));
        }
        
        if($request->via == 'email')
        {
            event(new EventWasPublished($event, $event->users, ['email']));
        }

    	return redirect()->route('admin.event.index')->with('message', 'The event has been edited successfully.');
    }

    public function destroy(Event $event)
    {
        $event->delete();
        if(! is_null($event->galleries()->first()))
        {
            $event->galleries()->first()->delete();
        }
    	return redirect()->route('admin.event.index')->with('message', 'The Event has been deleted successfully');
    }
}