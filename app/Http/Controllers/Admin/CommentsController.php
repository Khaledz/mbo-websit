<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Attachment;

class CommentsController extends Controller
{
    public function index()
    {
        $comments = Comment::paginate(15);

        return view('admin.comments.index', compact('comments'));
    }

    public function show(Comment $comment)
    {
        return view('admin.comments.show', compact('comment'));
    }

    public function create()
    {
        // return view('admin.faq.create', compact('faq'));
    }

    public function store(Request $request)
    {
        // $this->validate($request, 
        // [
        //     'question' => 'required|min:3',
        //     'answer' => 'required|min:3',
        // ]);

        // Faq::create($request->all());

        // return redirect()->route('admin.faq.index')->with('message', 'The question and answer has been saved successfully.');
    }

    public function edit(Comment $comment)
    {
        return view('admin.comments.edit', compact('comment'));
    }

    public function update(Request $request, Comment $comment)
    {
         $this->validate($request, 
        [
            'content' => 'required|min:3',
        ]);

        $comment->update($request->all());

        if($request->hasFile('files'))
        {
            foreach($request->file('files') as $file)
            {
                if($file->isValid())
                {
                    $file->store('clubs/' .$comment->post->club->id. '/posts/' . $comment->post->id .'/'. $comment->id);
                    $attachment = Attachment::create(['name' => $file->hashName(), 'orginal_name' => $file->getClientOriginalName(), 'extention' => $file->extension(), 'user_id' => auth()->user()->id]);
                    $comment->attachments()->save($attachment);
                }
            }
        }

        return redirect()->back()->with('message', 'The comment has been updated successfully.');
    }

    public function destroy(Comment $comment)
    {
        $comment->delete();

        return redirect()->back()->with('message', 'The comment has been removed successfully.');
    }
}