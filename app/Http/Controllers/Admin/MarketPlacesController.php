<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MarketPlace;
use App\Models\Attachment;

class MarketPlacesController extends Controller
{
    public function index()
    {
        $markets = MarketPlace::latest()->paginate(15);

        return view('admin.markets.index', compact('markets'));
    }

    public function show(MarketPlace $market)
    {
        return view('admin.markets.show', compact('market'));
    }

    public function create()
    {
    	return view('admin.markets.create');
    }

    public function store(Request $request)
    {
    	$this->validate($request, 
        [
            'name' => 'required|min:3|unique:market_places,name',
            'description' => 'required',
        ]);

        $market = MarketPlace::create(array_merge($request->all(), ['user_id' => auth()->user()->id, 'slug' => str_slug($request->name), 'status' => 1]));
        
        if($request->hasFile('logo'))
        {
            if($request->file('logo')->isValid())
            {
                $file = $request->file('logo');
                $file->store('market_places/logos/'. $market->id);
                $market->update(['logo' => $file->hashName()]);
            }
        }

        if($request->hasFile('files'))
        {
            foreach($request->file('files') as $file)
            {
                if($file->isValid())
                {
                    $file->store('market_places/'. $market->id);
                    $attachment = Attachment::create(['name' => $file->hashName(), 'orginal_name' => $file->getClientOriginalName(), 'extention' => $file->extension(), 'user_id' => auth()->user()->id, 'description' => $request->description_of_file]);
                    $market->attachments()->save($attachment);
                }
            }
        }

    	return redirect()->route('admin.market.index')->with('message', 'The market place has been created successfully.');
    }

    public function edit(MarketPlace $market)
    {
    	return view('admin.markets.edit', compact('market'));
    }

    public function update(Request $request, MarketPlace $market)
    {
    	$this->validate($request, 
        [
            'name' => 'required|max:255|min:3',
            'description' => 'required|max:1000',
        ]);

        $market->update($request->all());

        if($request->hasFile('logo'))
        {
            if($request->file('logo')->isValid())
            {
                $file = $request->file('logo');
                $file->store('market_places/logos/'. $market->id);
                $market->update(['logo' => $file->hashName()]);
            }
        }
        
        if($request->hasFile('files'))
        {
            foreach($request->file('files') as $file)
            {
                if($file->isValid())
                {
                    $file->store('market_places/'. $market->id);
                    $attachment = Attachment::create(['name' => $file->hashName(), 'orginal_name' => $file->getClientOriginalName(), 'extention' => $file->extension(), 'user_id' => auth()->user()->id, 'description' => $request->description_of_file]);
                    $market->attachments()->save($attachment);
                }
            }
        }

    	return redirect()->route('admin.market.index')->with('message', 'The market has been edited successfully.');
    }

    public function destroy(MarketPlace $market)
    {
    	$market->delete();
    	return redirect()->route('admin.market.index')->with('message', 'The market has been deleted successfully');
    }
}