<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Spatie\Permission\Models\Role;

class RolesController extends Controller
{
    public function index()
    {
        $roles = Role::paginate(15);

        return view('admin.roles.index', compact('roles'));
    }

    public function show($id)
    {
        $role = Role::find($id);
        return view('admin.roles.show', compact('role'));
    }

    public function create()
    {
    	return view('admin.roles.create');
    }

    public function store(Request $request)
    {
    	$this->validate($request, 
        [
            'name' => 'required|min:3,unique:roles,name',
        ]);

        Role::create($request->all());

    	return redirect()->route('admin.role.index')->with('message', 'The role has been created successfully.');
    }

    public function edit($id)
    {
    	$role = Role::find($id);

    	return view('admin.roles.edit', compact('role'));
    }

    public function update(Request $request, $id)
    {
        $role = Role::find($id);

    	$this->validate($request, 
        [
            'name' => 'required|min:3|unique:roles,name,'.$role->id,
        ]);

    	$role->update($request->all());

    	return redirect()->route('admin.roles.index')->with('message', 'The role has been edited successfully.');
    }

    public function destroy($id)
    {
    	$role = Role::find($id)->delete();

    	return redirect()->route('admin.role.index')->with('message', 'The role has been deleted successfully');
    }
}