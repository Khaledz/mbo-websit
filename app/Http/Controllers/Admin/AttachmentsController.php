<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Attachment;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class AttachmentsController extends Controller
{
    public function index()
    {
        
    }

    public function show(Announcement $announcement)
    {
        
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
    	
    }

    public function edit($announcement)
    {
    	
    }

    public function update(Request $request, Attachment $attachment)
    {
    	$this->validate($request, 
        [
            'file' => 'required',
        ]);

        $announcement->update($request->all());
        
        if($request->hasFile('files'))
        {
            foreach($request->file('files') as $file)
            {
                if($file->isValid())
                {
                    $file->store('announcements/'. $announcement->id);
                    $attachment = Attachment::create(['name' => $file->hashName(), 'orginal_name' => $file->getClientOriginalName(), 'extention' => $file->extension(), 'user_id' => auth()->user()->id]);
                    $announcement->attachments()->save($attachment);
                }
            }
        }

    	return redirect()->route('admin.announcement.index')->with('message', 'The announcement has been edited successfully.');
    }

    public function destroy($model, Attachment $attachment)
    {
        $modelName = $attachment->model;
        $model = (new $modelName)->find((int)$attachment->model_id);

        // if gallery and event togather
        if($model->model == 'App\Models\Event')
        {
            if(file_exists(storage_path('app/public' .'/events/'. $model->id .'/'. $attachment->name)))
            {
                unlink(storage_path('app/public' .'/events/'. $model->id .'/'. $attachment->name));
                $attachment->delete();
            }
        }

        if(file_exists(storage_path('app/public' .'/'. $model->getTable() .'/'. $model->id .'/'. $attachment->name)))
        {
            unlink(storage_path('app/public' .'/'. $model->getTable() .'/'. $model->id .'/'. $attachment->name));
            $attachment->delete();
        }

        if(file_exists(storage_path('app/public' .'/'. $model->getTable() .'/logos/'. $model->id .'/'. $attachment->name)))
        {
            unlink(storage_path('app/public' .'/'. $model->getTable() .'/logos/'. $model->id .'/'. $attachment->name));
            $attachment->delete();
        }

        if(file_exists(storage_path('app/public' .'/'. str_plural($model->getTable()) .'/'. $model->id .'/'. $attachment->name)))
        {
            $attachment->delete();
        }

        if(get_class($model) == 'App\Models\Post')
        {
            if(file_exists(storage_path('app/public' .'/'. str_plural($model->club->getTable()) .'/'. $model->club->id .'/'. str_plural($model->getTable()) .'/'. $model->id .'/'. $attachment->name)))
            {
                $attachment->delete();
            }
        }
        
        if(get_class($model) == 'App\Models\Comment')
        {
            if(file_exists(storage_path('app/public' .'/'. str_plural($model->post->club->getTable()) .'/'. $model->post->club->id .'/'. str_plural($model->post->getTable()) .'/'. $model->post->id .'/'. $model->id .'/'. $attachment->name)))
            {
                $attachment->delete();
            }
        }
    	
    	return back()->with('message', 'The attachment has been deleted successfully');
    }
}