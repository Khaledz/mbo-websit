<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Slider;
use App\Models\User;
use App\Models\Attachment;

class SlidersController extends Controller
{
    public function index()
    {
        $sliders = Slider::latest()->paginate(15);

        return view('admin.sliders.index', compact('sliders'));
    }

    public function show(Slider $slider)
    {
        return view('admin.sliders.show', compact('slider'));
    }

    public function create()
    {
    	return view('admin.sliders.create');
    }

    public function store(Request $request, Slider $slider)
    {
    	$this->validate($request, 
        [
            'name' => 'required',
            'status' => 'required',
            'file' => 'mimes:jpg,png,gif,jpeg|dimensions:min_width=300,min_height=350'
        ]);
        
        $slider = Slider::create($request->all());
        
        if($request->hasFile('file'))
        {
            if($request->file->isValid())
            {
                $request->file->store('sliders/'. $slider->id);
                $attachment = Attachment::create(['name' => $request->file->hashName(), 'orginal_name' => $request->file->getClientOriginalName(), 'extention' => $request->file->extension(), 'user_id' => auth()->user()->id]);
                $slider->attachments()->save($attachment);
            }
        }

    	return redirect()->route('admin.slider.index')->with('message', 'The slider has been created successfully.');
    }

    public function edit(Slider $slider)
    {
    	return view('admin.sliders.edit', compact('slider'));
    }

    public function update(Request $request, Slider $slider)
    {
    	$this->validate($request, 
        [
            'name' => 'required',
            'status' => 'required',
            'file' => 'mimes:jpg,png,gif,jpeg|dimensions:min_width=300,min_height=350'
        ]);
        
        $slider->update($request->all());

        if($request->hasFile('file'))
        {
            if($request->file->isValid())
            {
                $request->file->store('sliders/'. $slider->id);
                $attachment = Attachment::create(['name' => $request->file->hashName(), 'orginal_name' => $request->file->getClientOriginalName(), 'extention' => $request->file->extension(), 'user_id' => auth()->user()->id]);
                $slider->attachments()->save($attachment);
            }
        }

    	return redirect()->route('admin.slider.index')->with('message', 'The slider has been edited successfully.');
    }

    public function destroy(Slider $slider)
    {
    	$slider->delete();
    	return redirect()->route('admin.slider.index')->with('message', 'The slider has been deleted successfully');
    }
}