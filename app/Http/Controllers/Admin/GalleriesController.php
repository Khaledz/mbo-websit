<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Models\User;
use App\Models\Attachment;

class GalleriesController extends Controller
{
    public function index()
    {
        $galleries = Gallery::latest()->paginate(15);

        return view('admin.galleries.index', compact('galleries'));
    }

    public function show(Gallery $gallery)
    {
        return view('admin.galleries.show', compact('gallery'));
    }

    public function create()
    {
    	return view('admin.galleries.create');
    }

    public function store(Request $request, Gallery $gallery)
    {
    	$this->validate($request, 
        [
            'name' => 'required',
            'files.*' => 'required'
        ]);
        
        $gallery = Gallery::create(array_merge($request->all(), ['user_id' => auth()->user()->id]));
        
        if($request->hasFile('files'))
        {
            foreach($request->file('files') as $file)
            {
                if($file->isValid())
                {
                    $file->store('galleries/'. $gallery->id);
                    $attachment = Attachment::create(['name' => $file->hashName(), 'orginal_name' => $file->getClientOriginalName(), 'extention' => $file->extension(), 'user_id' => auth()->user()->id]);
                    $gallery->attachments()->save($attachment);
                }
            }
        }

    	return redirect()->route('admin.gallery.index')->with('message', 'The gallery has been created successfully.');
    }

    public function edit(Gallery $gallery)
    {
    	return view('admin.galleries.edit', compact('gallery'));
    }

    public function update(Request $request, Gallery $gallery)
    {
    	$this->validate($request, 
        [
            'name' => 'required',
            'files.*' => 'required'
        ]);
        
        $gallery->update($request->all());
        
        if($request->hasFile('files'))
        {
            foreach($request->file('files') as $file)
            {
                if($file->isValid())
                {
                    $file->store('galleries/'. $gallery->id);
                    $attachment = Attachment::create(['name' => $file->hashName(), 'orginal_name' => $file->getClientOriginalName(), 'extention' => $file->extension(), 'user_id' => auth()->user()->id, 'description' => $request->description_of_file]);
                    $gallery->attachments()->save($attachment);
                }
            }
        }

    	return redirect()->route('admin.gallery.index')->with('message', 'The gallery has been edited successfully.');
    }

    public function destroy(Gallery $gallery)
    {
    	$gallery->delete();
        // it works but might they don't want it.
        // $dirname = storage_path('app/public' .'/'. $gallery->getTable() .'/'. $gallery->name);
        // @array_map('unlink', glob("$dirname/*.*"));
        // @rmdir($dirname);

    	return redirect()->route('admin.gallery.index')->with('message', 'The gallery has been deleted successfully');
    }
}