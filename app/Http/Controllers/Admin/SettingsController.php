<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Info;

class SettingsController extends Controller
{
    public function index()
    {
        $infos = Info::first();

        return view('admin.info.index', compact('infos'));
    }

    // public function show(Activity $activity)
    // {
    //     return view('admin.activities.show', compact('activity'));
    // }

    public function create()
    {
        return view('admin.activities.create');
    }

    public function store(Request $request)
    {
        // $this->validate($request, 
        // [
        //     'title' => 'required|min:3',
        //     'icon' => 'required|image',
        //     'url' => 'required'
        // ]);
        
        // $activity = Activity::create($request->all());
        
        // if($request->hasFile('icon'))
        // {
        //     $file = $request->file('icon');
        //     if($file->isValid())
        //     {
        //         $file->store('activities/'. $activity->id);
        //         $activity->update(['icon' => $file->hashName()]);
        //     }
        // }

        // return redirect()->route('admin.activity.index')->with('message', 'The activity has been created successfully.');
    }

    public function edit()
    {
        $info = Info::first();

        return view('admin.info.edit', compact('info'));
    }

    public function update(Request $request)
    {
        $info = Info::first();
        $info->settings()->merge($request->all());
        
        return redirect()->route('admin.info.index')->with('message', 'The mbo information has been edited successfully.');
    }

    public function destroy(Activity $activity)
    {
        $activity->delete();
        return redirect()->route('admin.activity.index')->with('message', 'The activity has been deleted successfully');
    }
}