<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Announcement;
use App\Models\User;
use App\Models\Attachment;
use App\Events\AnnouncementWasPublished;

class AnnouncementsController extends Controller
{
    public function index()
    {
        $announcements = Announcement::latest()->paginate(15);

        return view('admin.announcements.index', compact('announcements'));
    }

    public function show(Announcement $announcement)
    {
        return view('admin.announcements.show', compact('announcement'));
    }

    public function create()
    {
        $users = User::all()->pluck('FullName', 'id');

    	return view('admin.announcements.create', compact('users'));
    }

    public function store(Request $request)
    {
    	$this->validate($request, 
        [
            'title' => 'required|min:3',
            'description' => 'required|min:3',
        ]);
        
        $announcement = Announcement::create(array_merge($request->all(), ['user_id' => auth()->user()->id]));
        
        if($request->hasFile('files'))
        {
            foreach($request->file('files') as $file)
            {
                if($file->isValid())
                {
                    $file->store('announcements/'. $announcement->id);
                    $attachment = Attachment::create(['name' => $file->hashName(), 'orginal_name' => $file->getClientOriginalName(), 'extention' => $file->extension(), 'user_id' => auth()->user()->id]);
                    $announcement->attachments()->save($attachment);
                }
            }
        }

        if($request->invite == 'all')
        {
            $users = User::get();
            $announcement->users()->attach($users, ['via' => $request->via]);
        }

        if(isset($request->users) && $request->invite == 'custom')
        {
            $announcement->users()->attach($request->users, ['via' => $request->via]);
            $users = $announcement->users()->get();
        }
                           
        if($request->via == 'all')
        {
            event(new AnnouncementWasPublished($announcement, $users, ['email', 'sms']));
        }
        
        if($request->via == 'sms')
        {
            event(new AnnouncementWasPublished($announcement, $users, ['sms']));
        }
        
        if($request->via == 'email')
        {
            event(new AnnouncementWasPublished($announcement, $users, ['email']));
        }

    	return redirect()->route('admin.announcement.index')->with('message', 'The announcement has been created successfully.');
    }

    public function edit(Announcement $announcement)
    {
    	return view('admin.announcements.edit');
    }

    public function update(Request $request, Announcement $announcement)
    {
    	$this->validate($request, 
        [
            'title' => 'required|min:3',
            'description' => 'required|min:3',
        ]);

        $announcement->update($request->all());
        
        if($request->hasFile('files'))
        {
            foreach($request->file('files') as $file)
            {
                if($file->isValid())
                {
                    $file->store('announcements/'. $announcement->id);
                    $attachment = Attachment::create(['name' => $file->hashName(), 'orginal_name' => $file->getClientOriginalName(), 'extention' => $file->extension(), 'user_id' => auth()->user()->id]);
                    $announcement->attachments()->save($attachment);
                }
            }
        }

    	return redirect()->route('admin.announcement.index')->with('message', 'The announcement has been edited successfully.');
    }

    public function destroy(Announcement $announcement)
    {
    	$announcement->delete();
    	return redirect()->route('admin.announcement.index')->with('message', 'The announcement has been deleted successfully');
    }
}