<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Club;
use App\Models\MarketPlace;
use App\Models\Event;
use App\Models\Post;
use App\Models\Attachment;

class HomesController extends Controller
{
    public function index()
    {
        $admin = User::role('admin')->count();
        $champion = User::role('champion')->count();
        $membership = User::role('membership')->count();
        $clubs = Club::count();
        $marketPlaces = MarketPlace::count();
        $event = Event::count();
        $post = Post::count();
        $files = Attachment::count();
        $users = User::take(5)->latest()->get();
        $posts = Post::take(5)->latest()->get();
        $pendingMemberships = Club::with(['members' => function($query){
            return $query->where('club_members.status', 0);
        }])->get();
        $pendingMarketPlaces = MarketPlace::where('status', 0)->take(5)->latest()->get();
        
        return view('admin.home.index', 
        compact('admin', 'champion', 'membership', 'clubs', 'marketPlaces',
        'event', 'post', 'files', 'users', 'posts', 'pendingMemberships', 'pendingMarketPlaces'));
    }
}
