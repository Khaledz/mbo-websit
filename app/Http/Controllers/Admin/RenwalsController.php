<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Renwal;
use App\Models\User;
use App\Cores\Communications\SMS\Nexmo;
use App\Mail\Membership\MembershipRenwal;
use Mail;

class RenwalsController extends Controller
{
    public function index()
    {
        $renwals = Renwal::latest()->paginate(15);

        return view('admin.renwals.index', compact('renwals'));
    }

    public function show(Renwal $renwal)
    {
        return view('admin.renwals.show', compact('renwal'));
    }

    public function create()
    {
        $users = User::all()->pluck('FullName', 'id');
    	return view('admin.renwals.create', compact('users'));
    }

    public function store(Request $request)
    {
    	$this->validate($request, 
        [
            'title' => 'required|min:3',
            'content' => 'required|min:3',
            'users' => 'required'
        ]);
        
        $renwal = Renwal::create(array_merge($request->all(), ['user_id' => auth()->user()->id]));
        
        $renwal->users()->attach($request->users);
        $users = User::whereIn('id', $request->users)->get();
              
        if($request->via == 'all')
        {
            foreach($users as $user)
            {
                Mail::to($user)->queue(new MembershipRenwal($user));
            }

            (new Nexmo())->to($users)->text($request->content)->send();
        }
        
        if($request->via == 'sms')
        {
            (new Nexmo())->to($users)->text($request->content)->send();
        }
        
        if($request->via == 'email')
        {
            foreach($users as $user)
            {
                Mail::to($user)->queue(new MembershipRenwal($user));
            }
        }

    	return redirect()->route('admin.renwal.index')->with('message', 'The membership renwal has been sent successfully.');
    }

    public function edit(Renwal $renwal)
    {
    	
    }

    public function update(Request $request, Renwal $renwal)
    {
    	
    }

    public function destroy(Renwal $renwal)
    {
    	
    }
}