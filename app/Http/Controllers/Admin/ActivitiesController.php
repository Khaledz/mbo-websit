<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Activity;
use App\Models\User;
use App\Models\Attachment;

class ActivitiesController extends Controller
{
    public function index()
    {
        $activities = Activity::latest()->paginate(15);

        return view('admin.activities.index', compact('activities'));
    }

    // public function show(Activity $activity)
    // {
    //     return view('admin.activities.show', compact('activity'));
    // }

    public function create()
    {
    	return view('admin.activities.create');
    }

    public function store(Request $request)
    {
    	$this->validate($request, 
        [
            'title' => 'required|min:3',
            'icon' => 'required|image',
            'url' => 'required'
        ]);
        
        $activity = Activity::create($request->all());
        
        if($request->hasFile('icon'))
        {
            $file = $request->file('icon');
            if($file->isValid())
            {
                $file->store('activities/'. $activity->id);
                $activity->update(['icon' => $file->hashName()]);
            }
        }

    	return redirect()->route('admin.activity.index')->with('message', 'The activity has been created successfully.');
    }

    public function edit(Activity $activity)
    {
    	return view('admin.activities.edit', compact('activity'));
    }

    public function update(Request $request, Activity $activity)
    {
    	$this->validate($request, 
        [
            'title' => 'required|min:3',
            'url' => 'required'
        ]);

        $activity->update($request->all());
        
        if($request->hasFile('icon'))
        {
            $file = $request->file('icon');
            if($file->isValid())
            {
                $file->store('activities/'. $activity->id);
                $activity->update(['icon' => $file->hashName()]);
            }
        }

    	return redirect()->route('admin.activity.index')->with('message', 'The activity has been edited successfully.');
    }

    public function destroy(Activity $activity)
    {
    	$activity->delete();
    	return redirect()->route('admin.activity.index')->with('message', 'The activity has been deleted successfully');
    }
}