<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Faq;

class FaqsController extends Controller
{
    public function index()
    {
        $faqs = Faq::all();

        return view('admin.faq.index', compact('faqs'));
    }

    public function show(Faq $faq)
    {
        return view('admin.faq.show', compact('faq'));
    }

    public function create()
    {
        return view('admin.faq.create', compact('faq'));
    }

    public function store(Request $request)
    {
        $this->validate($request, 
        [
            'question' => 'required|min:3',
            'answer' => 'required|min:3',
        ]);

        Faq::create($request->all());

        return redirect()->route('admin.faq.index')->with('message', 'The question and answer has been saved successfully.');
    }

    public function edit(Faq $faq)
    {
        return view('admin.faq.edit', compact('faq'));
    }

    public function update(Request $request, Faq $faq)
    {
         $this->validate($request, 
        [
            'question' => 'required|min:3',
            'answer' => 'required|min:3',
        ]);

        $faq->update($request->all());

        return redirect()->route('admin.faq.index')->with('message', 'The question and answer has been updated successfully.');
    }

    public function destroy(Faq $faq)
    {
        $faq->delete();

        return redirect()->route('admin.faq.index')->with('message', 'The question and answer has been removed successfully.');
    }
}