<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Board;
use App\Models\User;

class boardsController extends Controller
{
    public function index()
    {
        $boards = Board::latest()->paginate(15);

        return view('admin.boards.index', compact('boards'));
    }

    public function show(Board $board)
    {
        return view('admin.boards.show', compact('board'));
    }

    public function create()
    {
        $users = User::all()->pluck('FullName', 'id');

    	return view('admin.boards.create', compact('users'));
    }

    public function store(Request $request)
    {
    	$this->validate($request, 
        [
            'to' => 'required',
            'from' => 'required',
            'users' => 'required',
            'positions' => 'required'
        ]);
        
        $board = Board::create($request->all());
        
        // Reset all boards to inactive.
        Board::where('id', '!=', $board->id)->update(['status' => 0]);
                
        for($i = 0; $i < count($request->users); $i++)
        {
            \DB::table('board_users')->insert(
                [
                    'user_id' => $request->users[$i],
                    'position' => $request->positions[$i],
                    'board_id' => $board->id
                ]
            );

            // Assing them to role board.
            User::find($request->users[$i])->assignRole('board');
        }
        
    	return redirect()->route('admin.board.index')->with('message', 'The board members has been created successfully.');
    }

    public function edit(Board $board)
    {
        $users = User::all()->pluck('FullName', 'id');

    	return view('admin.boards.edit', compact('board', 'users'));
    }

    public function update(Request $request, Board $board)
    {
        return $request->all();
    	$this->validate($request, 
        [
            'to' => 'required|min:4',
            'from' => 'required|min:4',
        ]);

        $board->update($request->all());
        
    	return redirect()->route('admin.board.index')->with('message', 'The board member has been edited successfully.');
    }

    public function destroy(Board $board)
    {
    	$board->delete();
    	return redirect()->route('admin.board.index')->with('message', 'The board member has been deleted successfully');
    }
}