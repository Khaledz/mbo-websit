<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Spatie\Permission\Models\Role;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::latest()->get();
        $roles = Role::all();

        return view('admin.users.index', compact('users', 'roles'));
    }

    public function filter($role)
    {
    	$users = User::role($role)->get();
        $roles = Role::all();

    	return view('admin.users.index', compact('users', 'roles'));
    }

    public function show(User $user)
    {
        return view('admin.users.show', compact('user'));
    }

    public function create()
    {
    	$roles = Role::pluck('name', 'id');

    	return view('admin.users.create', compact('roles'));
    }

    public function store(Request $request)
    {
    	$this->validate($request, 
        [
            'username' => 'required|min:3|unique:users,username',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6',
            'phone' => 'required|max:9|min:9|unique:users,phone',
            'fname' => 'required|min:3',
            'lname' => 'required|min:3',
            'role_id' => 'required|exists:roles,id',
            'website' => 'nullable|URL'
        ]);

    	$user = User::create(array_merge($request->except(['password', 'sector', 'job', 'children', 'wife', 'website', 'company', 'linkedin']), ['password' => bcrypt($request->password)]));
       
        $user->assignRole(Role::find($request->role_id)->name);
        $user->settings()->merge($request->only(['sector', 'job', 'children', 'wife', 'website', 'company', 'linkedin']));

    	return redirect()->route('admin.user.index')->with('message', 'The user has been created successfully.');
    }

    public function edit($user)
    {
    	$roles = Role::pluck('name', 'id');

    	return view('admin.users.edit', compact('roles', 'user'));
    }

    public function update(Request $request, $user)
    {
    	$this->validate($request, 
        [
            'username' => 'required|min:3|unique:users,username,'.$user->id,
            'email' => 'required|email|unique:users,email,'.$user->id,
            'phone' => 'required|max:9|min:9|unique:users,phone,'.$user->id,
            'fname' => 'required|min:3',
            'lname' => 'required|min:3',
            'role_id' => 'required|exists:roles,id',
            'website' => 'nullable|URL'
        ]);

    	$user->update($request->except(['password', 'sector', 'job', 'children', 'wife', 'website', 'company', 'linkedin']));
        $user->syncRoles(Role::find($request->role_id)->name);
        $user->settings()->merge($request->only(['sector', 'job', 'children', 'wife', 'website', 'company', 'linkedin']));

        if(! is_null($request->password))
        {
            $user->update(['password' => bcrypt($request->password)]);
        }

    	return redirect()->route('admin.user.index')->with('message', 'The user has been edited successfully.');
    }

    public function destroy(User $user)
    {
    	$user->delete();
    	return redirect()->route('admin.user.index')->with('message', 'The user has been deleted successfully');
    }
}