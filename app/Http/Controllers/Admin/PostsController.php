<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Attachment;

class PostsController extends Controller
{
    public function index()
    {
        $posts = Post::paginate(15);

        return view('admin.posts.index', compact('posts'));
    }

    public function show(Post $post)
    {
        return view('admin.posts.show', compact('post'));
    }

    public function create()
    {
        // return view('admin.faq.create', compact('faq'));
    }

    public function store(Request $request)
    {
        // $this->validate($request, 
        // [
        //     'question' => 'required|min:3',
        //     'answer' => 'required|min:3',
        // ]);

        // Faq::create($request->all());

        // return redirect()->route('admin.faq.index')->with('message', 'The question and answer has been saved successfully.');
    }

    public function edit(Post $post)
    {
        return view('admin.posts.edit', compact('post'));
    }

    public function update(Request $request, Post $post)
    {
         $this->validate($request, 
        [
            'content' => 'required|min:3',
            'title' => 'required|min:3',
        ]);

        $post->update($request->all());

        if($request->hasFile('files'))
        {
            foreach($request->file('files') as $file)
            {
                if($file->isValid())
                {
                    $file->store('clubs/' .$post->club->id. '/posts/' . $post->id);
                    $attachment = Attachment::create(['name' => $file->hashName(), 'orginal_name' => $file->getClientOriginalName(), 'extention' => $file->extension(), 'user_id' => auth()->user()->id]);
                    $post->attachments()->save($attachment);
                }
            }
        }

        return redirect()->back()->with('message', 'The post has been updated successfully.');
    }

    public function destroy(Post $post)
    {
        $post->delete();

        return redirect()->back()->with('message', 'The post has been removed successfully.');
    }
}