<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Club;
use App\Models\Networking;
use App\Models\Alliance;
use App\Models\ExchangingInfo;
use App\Models\MemberSupport;
use App\Models\SharingExperience;
use App\Models\User;
use Spatie\Permission\Models\Role;

class ClubsController extends Controller
{
    public function index()
    {
        $clubs = Club::latest()->paginate(15);

        return view('admin.clubs.index', compact('clubs'));
    }

    public function show(Club $club)
    {
        $this->authorize($club);
        
        return view('admin.clubs.show', compact('club'));
    }

    public function create()
    {
        $networkings = Networking::pluck('name', 'id');
        $alliances = Alliance::pluck('name', 'id');
        $member_supports = MemberSupport::pluck('name', 'id');
        $exchanging_info = ExchangingInfo::pluck('name', 'id');
        $sharing_experiences = SharingExperience::pluck('name', 'id');     
        $users = User::all()->pluck('FullName', 'id');

    	return view('admin.clubs.create', compact('networkings', 'member_supports', 'alliances', 'exchanging_info', 'sharing_experiences', 'users'));
    }

    public function store(Request $request)
    {
    	$this->validate($request, 
        [
            'name' => 'required|unique:clubs,name',
            'description' => 'required',
            'launched' => 'required|min:4|integer',
            'networking_id' => 'exists:networkings,id',
            'alliance_id' => 'exists:alliances,id',
            'exchanging_info_id' => 'exists:exchanging_info,id',
            'member_support_id' => 'exists:member_supports,id',
            'sharing_experience_id' => 'exists:sharing_experiences,id',
            'members' => 'required',
            'champions' => 'required',
        ]);

    	$club = Club::create(array_merge($request->all(), ['user_id' => auth()->user()->id, 'slug' => str_slug($request->name), 'status' => 1]));

        $club->alliances()->attach($request->alliances_id);
        $club->exchanging_info()->attach($request->exchanging_info_id);
        $club->member_supports()->attach($request->member_support_id);
        $club->networkings()->attach($request->networking_id);
        $club->sharing_experiences()->attach($request->sharing_experience_id);
        $club->save();

        // Assign members.
        $club->members()->attach($request->members, ['status' => 1]);

        // Assign champions
        if(isset($request->champions))
        {
            // Assign roles
            foreach($request->champions as $champion)
            {
                $user = User::find($champion);
                if(! $user->hasAnyRole(['champion', 'admin']))
                {
                    $user->syncRoles('champion');
                }
            }
            $club->champions()->attach($request->champions);
        }

    	return redirect()->route('admin.club.index')->with('message', 'The club has been created successfully.');
    }

    public function edit($club)
    {
        $this->authorize('update', $club);

        $networkings = Networking::pluck('name', 'id');
        $alliances = Alliance::pluck('name', 'id');
        $member_supports = MemberSupport::pluck('name', 'id');
        $exchanging_info = ExchangingInfo::pluck('name', 'id');
        $sharing_experiences = SharingExperience::pluck('name', 'id');
        $users = User::all()->pluck('FullName', 'id');

    	return view('admin.clubs.edit', compact('users','club', 'networkings', 'member_supports', 'alliances', 'exchanging_info', 'sharing_experiences'));
    }

    public function update(Request $request, $club)
    {
        $this->authorize($club);

    	$this->validate($request, 
        [
            'name' => 'required|min:3|unique:clubs,name,'.$club->id,
            'description' => 'required',
            'launched' => 'required|min:4|integer',
            'networking_id' => 'exists:networkings,id',
            'alliance_id' => 'exists:alliances,id',
            'exchanging_info_id' => 'exists:exchanging_info,id',
            'member_support_id' => 'exists:member_supports,id',
            'sharing_experience_id' => 'exists:sharing_experiences,id',
            'members' => 'required',
            'champions' => 'required',
        ]);

    	$club->update($request->all());

        $club->alliances()->sync($request->alliances_id);
        $club->exchanging_info()->sync($request->exchanging_info_id);
        $club->member_supports()->sync($request->member_support_id);
        $club->networkings()->sync($request->networking_id);
        $club->sharing_experiences()->sync($request->sharing_experience_id);
        $club->save();

        // Assign members.
        $club->members()->sync($request->members, ['status' => 1]);
        
        // Assign champions
        if(isset($request->champions))
        {
            // Assign roles
            foreach($request->champions as $champion)
            {
                $user = User::find($champion);
                if(! $user->hasAnyRole(['champion', 'admin']))
                {
                    $user->syncRoles('champion');
                }
            }

            $club->champions()->sync($request->champions);
        }

    	return redirect()->route('admin.club.index')->with('message', 'The club has been edited successfully.');
    }

    public function destroy(club $club)
    {
        $this->authorize($club);

    	$club->delete();
    	return redirect()->route('admin.club.index')->with('message', 'The club has been deleted successfully');
    }

    public function getPending(club $club)
    {
        $pendings = $club->members()->where('club_members.status', 0)->get();
    	return view('admin.clubs.pending', compact('club', 'pendings'));
    }

    public function postPending(club $club, Request $request)
    {
        $pendings = array_fill_keys($request->pendings, ['status' => 1]);
        $club->members()->sync($pendings);
    	return redirect()->route('admin.club.index')->with('message', 'The data has been saved successfully');
    }
}