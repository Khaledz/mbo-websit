<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EventCategory;

class EventCategoriesController extends Controller
{
    public function index()
    {
        $categories = EventCategory::paginate(15);

        return view('admin.event_categories.index', compact('categories'));
    }

    public function show(EventCategory $category)
    {
        return view('admin.event_categories.show', compact('category'));
    }

    public function create()
    {
        $categories = EventCategory::pluck('name', 'id');

    	return view('admin.event_categories.create', compact('categories'));
    }

    public function store(Request $request)
    {
    	$this->validate($request, 
        [
            'name' => 'required|min:3|unique:event_categories,name',
        ]);

    	EventCategory::create($request->all());

    	return redirect()->route('admin.category.index')->with('message', 'The category has been created successfully.');
    }

    public function edit(EventCategory $category)
    {
    	return view('admin.event_categories.edit', compact('category'));
    }

    public function update(Request $request, EventCategory $category)
    {
    	$this->validate($request, 
        [
            // |unique:events,name,'. $event->id,
            'name' => 'required|min:3|unique:event_categories,name,'. $category->name,
        ]);

    	$category->name = $request->name;
        $category->save();

    	return redirect()->route('admin.category.index')->with('message', 'The category has been edited successfully.');
    }

    public function destroy(EventCategory $category)
    {
    	$category->delete();
    	return redirect()->route('admin.category.index')->with('message', 'The category has been deleted successfully');
    }
}