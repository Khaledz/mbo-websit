<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use View;
use Illuminate\Http\Response;
use Route;

class InactiveMembership
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if(! auth()->guest())
        // {
        //     $user = auth()->user();
        //     // Check if user is membership and status in inactive and not came from registartion
        //     if($user->hasRole('membership') && $user->status == 0)
        //     {
        //         return new Response(view('errors.inactive'));
        //     }
        // }

        return $next($request);
    }
}
