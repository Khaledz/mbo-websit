<?php

namespace App\Http\Middleware;

use Closure;
use App\Cores\Languages\Title;
use View;
use Auth;
use Request;

class TitleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        View::share('title', (new Title($request))->get());

        if(! auth()->guest() && auth()->user()->status == 0 && isset(\Route::getCurrentRoute()->getAction()['controller']) && \Route::getCurrentRoute()->getAction()['controller'] != 'App\Http\Controllers\App\HomesController@index' && \Route::getCurrentRoute()->getAction()['controller'] != 'App\Http\Controllers\Auth\LoginController@logout' && \Route::getCurrentRoute()->getAction()['controller'] != 'App\Http\Controllers\Auth\RegisterController@showRegistrationForm')
        {
            return response(view('errors.inactive'));
        }

        return $next($request);
    }
}
