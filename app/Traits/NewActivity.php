<?php

namespace App\Traits;

use Auth;

trait NewActivity
{
	/**
     * Register the necessary event listeners. Will be booted first.
     *
     * @return void
     */
    protected static function bootActivity()
    {
        foreach (static::getModelEvents() as $event) {
            static::$event(function ($model) use ($event) {
                // get type of model
                // Insert in DB.
                // from frontend fetch the message
            });
        }
    }

    /**
     * Get the model events to record activity for.
     *
     * @return array
     */
    protected static function getModelEvents()
    {
        if (isset(static::$recordEvents)) {
            return static::$recordEvents;
        }

        return [
            'created', 'deleted', 'updated'
        ];
    }
}