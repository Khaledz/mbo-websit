<?php

namespace App\Traits;

use Auth;

trait UserSettings
{
	/**
     * Register the necessary event listeners. Will be booted first.
     *
     * @return void
     */
    protected static function bootUserSettings()
    {
        foreach (static::getModelEvents() as $event) {
            static::$event(function ($model) use ($event) {
                foreach(static::getAvailbeSettings() as $function)
                {
                    if(method_exists($model, $function))
                    {
                        $model->$function($event);
                    }
                }
            });
        }
    }

    /**
     * Get the model events to record activity for.
     *
     * @return array
     */
    protected static function getModelEvents()
    {
        if (isset(static::$recordEvents)) {
            return static::$recordEvents;
        }

        return [
            'created', 'deleted', 'updated'
        ];
    }

    /**
     * Get the settings that need to be handled.
     *
     * @return array
     */
    protected static function getAvailbeSettings()
    {
        if (isset(static::$settings)) {
            return static::$settings;
        }

        return array_keys(Auth::user()->settings);
    }

    /**
     * Example
     *
     * @param  string $event
     * @return void
     */
    public function email_notification($event)
    {
        // If the {"email_notification": TRUE}
        if(Auth::user()->settings()->email_notification)
        {
            return var_dump('Send notification to email');
        }

        return var_dump('OFF');
    }
}