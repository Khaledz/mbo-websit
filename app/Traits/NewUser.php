<?php

namespace App\Traits;

use Mail;
use App\Events\UserWasRegistred;

trait NewUser
{
	/**
     * Register the necessary event listeners. Will be booted first.
     *
     * @return void
     */
    protected static function bootNewUser()
    {
        foreach (static::getModelEvents() as $event) {
            static::$event(function ($model) use ($event) {
                if($event == 'created')
                {
                    $model->update(['settings' => config('settings')]);
                    event(new UserWasRegistred($model));
                }
            });
        }
    }

	/**
     * Get the model events to record activity for.
     *
     * @return array
     */
    protected static function getModelEvents()
    {
        if (isset(static::$recordEvents)) {
            return static::$recordEvents;
        }

        return [
            'created', 'deleted', 'updated'
        ];
    }
}