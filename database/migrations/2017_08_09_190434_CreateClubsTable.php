<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('networkings', function(Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name', 150)->unique();
        });

        Schema::create('alliances', function(Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name', 150)->unique();
        });

        Schema::create('exchanging_info', function(Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name', 150)->unique();
        });

        Schema::create('sharing_experiences', function(Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name', 150)->unique();
        });

        Schema::create('member_supports', function(Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name', 150)->unique();
        });

        Schema::create('clubs', function(Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->text('name');
            $table->text('slug');
            $table->longText('description', 1000);
            $table->string('launched');
            $table->longText('goals')->nullable();
            $table->integer('user_id')->unsigned();
            $table->boolean('status')->default(0);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('club_networkings', function(Blueprint $table) {
            $table->integer('club_id')->unsigned();
            $table->integer('networking_id')->unsigned();

            $table->foreign('club_id')->references('id')->on('clubs')->onDelete('cascade');
            $table->foreign('networking_id')->references('id')->on('networkings')->onDelete('cascade');
        });

        Schema::create('club_alliances', function(Blueprint $table) {
            $table->integer('club_id')->unsigned();
            $table->integer('alliance_id')->unsigned();

            $table->foreign('club_id')->references('id')->on('clubs')->onDelete('cascade');
            $table->foreign('alliance_id')->references('id')->on('alliances')->onDelete('cascade');
        });

        Schema::create('club_exchanging_info', function(Blueprint $table) {
            $table->integer('club_id')->unsigned();
            $table->integer('exchanging_info_id')->unsigned();

            $table->foreign('club_id')->references('id')->on('clubs')->onDelete('cascade');
            $table->foreign('exchanging_info_id')->references('id')->on('exchanging_info')->onDelete('cascade');
        });

        Schema::create('club_sharing_experiences', function(Blueprint $table) {
            $table->integer('club_id')->unsigned();
            $table->integer('sharing_experience_id')->unsigned();

            $table->foreign('club_id')->references('id')->on('clubs')->onDelete('cascade');
            $table->foreign('sharing_experience_id')->references('id')->on('sharing_experiences')->onDelete('cascade');
        });

        Schema::create('club_member_supports', function(Blueprint $table) {
            $table->integer('club_id')->unsigned();
            $table->integer('member_support_id')->unsigned();

            $table->foreign('club_id')->references('id')->on('clubs')->onDelete('cascade');
            $table->foreign('member_support_id')->references('id')->on('member_supports')->onDelete('cascade');
        });

        Schema::create('club_members', function(Blueprint $table) {
            $table->integer('club_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->boolean('status')->default(0);
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('club_id')->references('id')->on('clubs')->onDelete('cascade');
        });

        Schema::create('club_champions', function(Blueprint $table) {
            $table->integer('club_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('club_id')->references('id')->on('clubs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('networkings');
        Schema::dropIfExists('alliances');
        Schema::dropIfExists('exchanging_info');
        Schema::dropIfExists('sharing_experiences');
        Schema::dropIfExists('member_supports');
        Schema::dropIfExists('clubs');
        Schema::dropIfExists('club_users');
    }
}