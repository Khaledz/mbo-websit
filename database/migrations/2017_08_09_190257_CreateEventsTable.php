<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_categories', function(Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name', 150)->unique();
            $table->string('avatar', 150)->nullable();
        });

        Schema::create('events', function(Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->text('name');
            $table->text('slug');
            $table->longText('description');
            $table->string('date');
            $table->string('time');
            $table->text('speaker');
            $table->text('location')->nullable();
            $table->text('note')->nullable();
            $table->longText('program')->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('event_category_id')->unsigned();
            $table->boolean('status')->default(1);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('event_category_id')->references('id')->on('event_categories')->onDelete('cascade');
        });

        Schema::create('event_users', function(Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('event_id')->unsigned();
            $table->boolean('status')->default(0);
            $table->text('note')->nullable();
            $table->boolean('answered')->default(0);

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}