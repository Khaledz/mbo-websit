<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->text('username');
            $table->text('email');
            $table->text('fname');
            $table->text('lname');
            $table->text('password');
            $table->text('phone');
            $table->text('settings')->nullable();
            $table->boolean('status')->default(0);
            $table->string('invite')->nullable();
            $table->text('joined')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
