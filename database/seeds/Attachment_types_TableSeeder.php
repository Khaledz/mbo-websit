<?php

use Illuminate\Database\Seeder;

class Attachment_types_TableSeeder extends Seeder {
    public function run() {
        $entries = 
        [
            [
                "id" => "1",
                "name" => "image"
            ],
            [
                "id" => "2",
                "name" => "video"
            ]
        ];


        foreach($entries as $entry){
            DB::table('attachment_types')->insert($entry);
        }
    }
}