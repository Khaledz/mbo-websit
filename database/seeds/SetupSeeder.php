<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SetupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'admin',
            'guard_name' => 'web'
        ]);

        DB::table('roles')->insert([
            'name' => 'champion',
            'guard_name' => 'web'
        ]);

        DB::table('roles')->insert([
            'name' => 'membership',
            'guard_name' => 'web'
        ]);

        DB::table('roles')->insert([
            'name' => 'board',
            'guard_name' => 'web'
        ]);

        DB::table('roles')->insert([
            'name' => 'board admin',
            'guard_name' => 'web'
        ]);

        DB::table('alliances')->insert([
            'name' => 'Negotiations',
        ]);

        DB::table('alliances')->insert([
            'name' => 'Contractors',
        ]);

        DB::table('alliances')->insert([
            'name' => 'Landlords',
        ]);

        DB::table('alliances')->insert([
            'name' => 'Others',
        ]);

        DB::table('exchanging_info')->insert([
            'name' => 'Market Info',
        ]);

        DB::table('exchanging_info')->insert([
            'name' => 'Legislations',
        ]);

        DB::table('member_supports')->insert([
            'name' => 'MBO Events',
        ]);

        DB::table('member_supports')->insert([
            'name' => 'RE reference for MBO members',
        ]);

        DB::table('networkings')->insert([
            'name' => 'Local',
        ]);

        DB::table('networkings')->insert([
            'name' => 'National',
        ]);

        DB::table('networkings')->insert([
            'name' => 'International',
        ]);

        DB::table('event_categories')->insert([
            'name' => 'Social Events',
            'avatar' => 'events.png'
        ]);

        DB::table('event_categories')->insert([
            'name' => 'Eductional Events',
            'avatar' => 'events.png'
        ]);

        DB::table('event_categories')->insert([
            'name' => 'Networking Events',
            'avatar' => 'events.png'
        ]);

        DB::table('event_categories')->insert([
            'name' => 'CSR Events',
            'avatar' => 'events.png'
        ]);

        DB::table('event_categories')->insert([
            'name' => 'International Events',
            'avatar' => 'events.png'
        ]);

        DB::table('users')->insert([
            'username' => 'shatri',
            'email' => 'shatri@mbo.com',
            'password' => bcrypt('123456'),
            'fname' => 'Mohammed',
            'lname' => 'Alshatri',
            'phone' => 123456789,
            'settings' => json_encode(config('settings')),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'status' => 1
        ]);

        DB::table('users')->insert([
            'username' => 'saif',
            'email' => 'saif@mbo.com',
            'password' => bcrypt('123456'),
            'fname' => 'Saif',
            'lname' => 'Mofti',
            'phone' => 123456788,
            'settings' => json_encode(config('settings')),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'status' => 1
        ]);

        DB::table('model_has_roles')->insert([
            'role_id' => 1,
            'model_id' => 1,
            'model_type' => 'App\Models\User',
        ]);

        DB::table('model_has_roles')->insert([
            'role_id' => 2,
            'model_id' => 2,
            'model_type' => 'App\Models\User',
        ]);

        DB::table('activities')->insert([
            'title' => 'Social Events',
            'icon' => 'events.png',
            'url' => 'category/1'
        ]);

        DB::table('activities')->insert([
            'title' => 'Eductional Events',
            'icon' => 'events.png',
            'url' => 'category/2'
        ]);

        DB::table('activities')->insert([
            'title' => 'Networking Events',
            'icon' => 'events.png',
            'url' => 'category/3'
        ]);

        DB::table('activities')->insert([
            'title' => 'CSR Events',
            'icon' => 'events.png',
            'url' => 'category/4'
        ]);

        DB::table('activities')->insert([
            'title' => 'International Events',
            'icon' => 'events.png',
            'url' => 'category/5'
        ]);

        DB::table('activities')->insert([
            'title' => 'Multaqa',
            'icon' => 'moltaqa.png',
            'url' => 'club'
        ]);

        DB::table('activities')->insert([
            'title' => 'Market Place',
            'icon' => 'marketplace.png',
            'url' => 'market'
        ]);

        DB::table('activities')->insert([
            'title' => 'Calendar',
            'icon' => 'calendar.png',
            'url' => 'calendar'
        ]);

        DB::table('info')->insert([
            'settings' => json_encode(config('texts')),
        ]);

        foreach(config('faq') as $key => $value)
        {
            DB::table('faqs')->insert([
                'question' => $key,
                'answer' => $value
            ]);
        }
        
    }
}
