<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SetupSeeder::class);
        // factory('App\Models\User', 100)->create();
        // factory('App\Models\Post', 14)->create();
    }
}
