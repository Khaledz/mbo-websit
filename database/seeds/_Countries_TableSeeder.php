<?php

use Illuminate\Database\Seeder;

class _Countries_TableSeeder extends Seeder {
    public function run() {
        $entries = 
        [
            [
                "id" => "1",
               
                "code" => "AD",
                "iso3" => "AND",
                "population" => "85082",
                "name" => "Andorra",
                "full_name" => "Principality of Andorra",
                "continent_id" => "4"
            ],
            [
                "id" => "2",
               
                "code" => "AE",
                "iso3" => "ARE",
                "population" => "8264070",
                "name" => "United Arab Emirates",
                "full_name" => "United Arab Emirates",
                "continent_id" => "3"
            ],
            [
                "id" => "3",
               
                "code" => "AF",
                "iso3" => "AFG",
                "population" => "30419928",
                "name" => "Afghanistan",
                "full_name" => "Islamic Republic of Afghanistan",
                "continent_id" => "3"
            ],
            [
                "id" => "4",
               
                "code" => "AG",
                "iso3" => "ATG",
                "population" => "81799",
                "name" => "Antigua and Barbuda",
                "full_name" => "Antigua and Barbuda",
                "continent_id" => "5"
            ],
            [
                "id" => "5",
               
                "code" => "AI",
                "iso3" => "AIA",
                "population" => "13600",
                "name" => "Anguilla",
                "full_name" => "Anguilla",
                "continent_id" => "5"
            ],
            [
                "id" => "6",
               
                "code" => "AL",
                "iso3" => "ALB",
                "population" => "3002859",
                "name" => "Albania",
                "full_name" => "Republic of Albania",
                "continent_id" => "4"
            ],
            [
                "id" => "7",
               
                "code" => "AM",
                "iso3" => "ARM",
                "population" => "2970495",
                "name" => "Armenia",
                "full_name" => "Republic of Armenia",
                "continent_id" => "3"
            ],
            [
                "id" => "8",
               
                "code" => "NA",
                "iso3" => "",
                "population" => "",
                "name" => "Netherlands Antilles",
                "full_name" => "Netherlands Antilles",
                "continent_id" => "5"
            ],
            [
                "id" => "9",
               
                "code" => "AO",
                "iso3" => "AGO",
                "population" => "18498000",
                "name" => "Angola",
                "full_name" => "Republic of Angola",
                "continent_id" => "1"
            ],
            [
                "id" => "10",
               
                "code" => "AQ",
                "iso3" => "",
                "population" => "",
                "name" => "Antarctica",
                "full_name" => "Antarctica (the territory South of 60 deg S)",
                "continent_id" => "2"
            ],
            [
                "id" => "11",
               
                "code" => "AR",
                "iso3" => "ARG",
                "population" => "40518425",
                "name" => "Argentina",
                "full_name" => "Argentine Republic",
                "continent_id" => "7"
            ],
            [
                "id" => "12",
               
                "code" => "AS",
                "iso3" => "ASM",
                "population" => "55519",
                "name" => "American Samoa",
                "full_name" => "American Samoa",
                "continent_id" => "6"
            ],
            [
                "id" => "13",
               
                "code" => "AT",
                "iso3" => "AUT",
                "population" => "8414638",
                "name" => "Austria",
                "full_name" => "Republic of Austria",
                "continent_id" => "4"
            ],
            [
                "id" => "14",
               
                "code" => "AU",
                "iso3" => "AUS",
                "population" => "22028000",
                "name" => "Australia",
                "full_name" => "Commonwealth of Australia",
                "continent_id" => "6"
            ],
            [
                "id" => "15",
               
                "code" => "AW",
                "iso3" => "ABW",
                "population" => "101484",
                "name" => "Aruba",
                "full_name" => "Aruba",
                "continent_id" => "5"
            ],
            [
                "id" => "16",
               
                "code" => "AX",
                "iso3" => "",
                "population" => "",
                "name" => "Åland",
                "full_name" => "Åland Islands",
                "continent_id" => "4"
            ],
            [
                "id" => "17",
               
                "code" => "AZ",
                "iso3" => "AZE",
                "population" => "9165000",
                "name" => "Azerbaijan",
                "full_name" => "Republic of Azerbaijan",
                "continent_id" => "3"
            ],
            [
                "id" => "18",
               
                "code" => "BA",
                "iso3" => "BIH",
                "population" => "3839737",
                "name" => "Bosnia and Herzegovina",
                "full_name" => "Bosnia and Herzegovina",
                "continent_id" => "4"
            ],
            [
                "id" => "19",
               
                "code" => "BB",
                "iso3" => "BRB",
                "population" => "284589",
                "name" => "Barbados",
                "full_name" => "Barbados",
                "continent_id" => "5"
            ],
            [
                "id" => "20",
               
                "code" => "BD",
                "iso3" => "BGD",
                "population" => "152518015",
                "name" => "Bangladesh",
                "full_name" => "People\\\\'s Republic of Bangladesh",
                "continent_id" => "3"
            ],
            [
                "id" => "21",
               
                "code" => "BE",
                "iso3" => "BEL",
                "population" => "11007020",
                "name" => "Belgium",
                "full_name" => "Kingdom of Belgium",
                "continent_id" => "4"
            ],
            [
                "id" => "22",
               
                "code" => "BF",
                "iso3" => "BFA",
                "population" => "15730977",
                "name" => "Burkina Faso",
                "full_name" => "Burkina Faso",
                "continent_id" => "1"
            ],
            [
                "id" => "23",
               
                "code" => "BG",
                "iso3" => "BGR",
                "population" => "7364570",
                "name" => "Bulgaria",
                "full_name" => "Republic of Bulgaria",
                "continent_id" => "4"
            ],
            [
                "id" => "24",
               
                "code" => "BH",
                "iso3" => "BHR",
                "population" => "1234571",
                "name" => "Bahrain",
                "full_name" => "Kingdom of Bahrain",
                "continent_id" => "3"
            ],
            [
                "id" => "25",
               
                "code" => "BI",
                "iso3" => "BDI",
                "population" => "8749000",
                "name" => "Burundi",
                "full_name" => "Republic of Burundi",
                "continent_id" => "1"
            ],
            [
                "id" => "26",
               
                "code" => "BJ",
                "iso3" => "BEN",
                "population" => "9598787",
                "name" => "Benin",
                "full_name" => "Republic of Benin",
                "continent_id" => "1"
            ],
            [
                "id" => "27",
               
                "code" => "BL",
                "iso3" => "",
                "population" => "",
                "name" => "Saint Barthélemy",
                "full_name" => "Saint Barthelemy",
                "continent_id" => "5"
            ],
            [
                "id" => "28",
               
                "code" => "BM",
                "iso3" => "BMU",
                "population" => "64268",
                "name" => "Bermuda",
                "full_name" => "Bermuda",
                "continent_id" => "5"
            ],
            [
                "id" => "29",
               
                "code" => "BN",
                "iso3" => "",
                "population" => "",
                "name" => "Brunei Darussalam",
                "full_name" => "Brunei Darussalam",
                "continent_id" => "3"
            ],
            [
                "id" => "30",
               
                "code" => "BO",
                "iso3" => "BOL",
                "population" => "10907778",
                "name" => "Bolivia",
                "full_name" => "Republic of Bolivia",
                "continent_id" => "7"
            ],
            [
                "id" => "31",
               
                "code" => "BR",
                "iso3" => "BRA",
                "population" => "192380000",
                "name" => "Brazil",
                "full_name" => "Federative Republic of Brazil",
                "continent_id" => "7"
            ],
            [
                "id" => "32",
               
                "code" => "BS",
                "iso3" => "BHS",
                "population" => "353658",
                "name" => "Bahamas",
                "full_name" => "Commonwealth of the Bahamas",
                "continent_id" => "5"
            ],
            [
                "id" => "33",
               
                "code" => "BT",
                "iso3" => "BTN",
                "population" => "742737",
                "name" => "Bhutan",
                "full_name" => "Kingdom of Bhutan",
                "continent_id" => "3"
            ],
            [
                "id" => "34",
               
                "code" => "BV",
                "iso3" => "",
                "population" => "",
                "name" => "Bouvet Island",
                "full_name" => "Bouvet Island (Bouvetoya)",
                "continent_id" => "2"
            ],
            [
                "id" => "35",
               
                "code" => "BW",
                "iso3" => "BWA",
                "population" => "2029307",
                "name" => "Botswana",
                "full_name" => "Republic of Botswana",
                "continent_id" => "1"
            ],
            [
                "id" => "36",
               
                "code" => "BY",
                "iso3" => "BLR",
                "population" => "9643566",
                "name" => "Belarus",
                "full_name" => "Republic of Belarus",
                "continent_id" => "4"
            ],
            [
                "id" => "37",
               
                "code" => "BZ",
                "iso3" => "BLZ",
                "population" => "307000",
                "name" => "Belize",
                "full_name" => "Belize",
                "continent_id" => "5"
            ],
            [
                "id" => "38",
               
                "code" => "CA",
                "iso3" => "CAN",
                "population" => "34278406",
                "name" => "Canada",
                "full_name" => "Canada",
                "continent_id" => "5"
            ],
            [
                "id" => "39",
               
                "code" => "CC",
                "iso3" => "CCK",
                "population" => "596",
                "name" => "Cocos (Keeling) Islands",
                "full_name" => "Cocos (Keeling) Islands",
                "continent_id" => "3"
            ],
            [
                "id" => "40",
               
                "code" => "CD",
                "iso3" => "",
                "population" => "",
                "name" => "Congo (Kinshasa)",
                "full_name" => "Democratic Republic of the Congo",
                "continent_id" => "1"
            ],
            [
                "id" => "41",
               
                "code" => "CF",
                "iso3" => "CAF",
                "population" => "4422000",
                "name" => "Central African Republic",
                "full_name" => "Central African Republic",
                "continent_id" => "1"
            ],
            [
                "id" => "42",
               
                "code" => "CG",
                "iso3" => "",
                "population" => "",
                "name" => "Congo (Brazzaville)",
                "full_name" => "Republic of the Congo",
                "continent_id" => "1"
            ],
            [
                "id" => "43",
               
                "code" => "CH",
                "iso3" => "CHE",
                "population" => "7866500",
                "name" => "Switzerland",
                "full_name" => "Swiss Confederation",
                "continent_id" => "4"
            ],
            [
                "id" => "44",
               
                "code" => "CI",
                "iso3" => "",
                "population" => "",
                "name" => "Côte d\\\\'Ivoire",
                "full_name" => "Republic of Cote d\\\\'Ivoire",
                "continent_id" => "1"
            ],
            [
                "id" => "45",
               
                "code" => "CK",
                "iso3" => "COK",
                "population" => "19569",
                "name" => "Cook Islands",
                "full_name" => "Cook Islands",
                "continent_id" => "6"
            ],
            [
                "id" => "46",
               
                "code" => "CL",
                "iso3" => "CHL",
                "population" => "16763470",
                "name" => "Chile",
                "full_name" => "Republic of Chile",
                "continent_id" => "7"
            ],
            [
                "id" => "47",
               
                "code" => "CM",
                "iso3" => "CMR",
                "population" => "18879301",
                "name" => "Cameroon",
                "full_name" => "Republic of Cameroon",
                "continent_id" => "1"
            ],
            [
                "id" => "48",
               
                "code" => "CN",
                "iso3" => "CHN",
                "population" => "1339724852",
                "name" => "China",
                "full_name" => "People\\\\'s Republic of China",
                "continent_id" => "3"
            ],
            [
                "id" => "49",
               
                "code" => "CO",
                "iso3" => "COL",
                "population" => "46413791",
                "name" => "Colombia",
                "full_name" => "Republic of Colombia",
                "continent_id" => "7"
            ],
            [
                "id" => "50",
               
                "code" => "CR",
                "iso3" => "CRI",
                "population" => "4579000",
                "name" => "Costa Rica",
                "full_name" => "Republic of Costa Rica",
                "continent_id" => "5"
            ],
            [
                "id" => "51",
               
                "code" => "CU",
                "iso3" => "CUB",
                "population" => "11204000",
                "name" => "Cuba",
                "full_name" => "Republic of Cuba",
                "continent_id" => "5"
            ],
            [
                "id" => "52",
               
                "code" => "CV",
                "iso3" => "CPV",
                "population" => "567000",
                "name" => "Cape Verde",
                "full_name" => "Republic of Cape Verde",
                "continent_id" => "1"
            ],
            [
                "id" => "53",
               
                "code" => "CX",
                "iso3" => "CXR",
                "population" => "2072",
                "name" => "Christmas Island",
                "full_name" => "Christmas Island",
                "continent_id" => "3"
            ],
            [
                "id" => "54",
               
                "code" => "CY",
                "iso3" => "CYP",
                "population" => "1099341",
                "name" => "Cyprus",
                "full_name" => "Republic of Cyprus",
                "continent_id" => "3"
            ],
            [
                "id" => "55",
               
                "code" => "CZ",
                "iso3" => "CZE",
                "population" => "10535811",
                "name" => "Czech Republic",
                "full_name" => "Czech Republic",
                "continent_id" => "4"
            ],
            [
                "id" => "56",
               
                "code" => "DE",
                "iso3" => "DEU",
                "population" => "81799600",
                "name" => "Germany",
                "full_name" => "Federal Republic of Germany",
                "continent_id" => "4"
            ],
            [
                "id" => "57",
               
                "code" => "DJ",
                "iso3" => "DJI",
                "population" => "923000",
                "name" => "Djibouti",
                "full_name" => "Republic of Djibouti",
                "continent_id" => "1"
            ],
            [
                "id" => "58",
               
                "code" => "DK",
                "iso3" => "DNK",
                "population" => "5564219",
                "name" => "Denmark",
                "full_name" => "Kingdom of Denmark",
                "continent_id" => "4"
            ],
            [
                "id" => "59",
               
                "code" => "DM",
                "iso3" => "DMA",
                "population" => "73126",
                "name" => "Dominica",
                "full_name" => "Commonwealth of Dominica",
                "continent_id" => "5"
            ],
            [
                "id" => "60",
               
                "code" => "DO",
                "iso3" => "DOM",
                "population" => "10090000",
                "name" => "Dominican Republic",
                "full_name" => "Dominican Republic",
                "continent_id" => "5"
            ],
            [
                "id" => "61",
               
                "code" => "DZ",
                "iso3" => "DZA",
                "population" => "34178188",
                "name" => "Algeria",
                "full_name" => "People\\\\'s Democratic Republic of Algeria",
                "continent_id" => "1"
            ],
            [
                "id" => "62",
               
                "code" => "EC",
                "iso3" => "ECU",
                "population" => "15007343",
                "name" => "Ecuador",
                "full_name" => "Republic of Ecuador",
                "continent_id" => "7"
            ],
            [
                "id" => "63",
               
                "code" => "EE",
                "iso3" => "EST",
                "population" => "1340194",
                "name" => "Estonia",
                "full_name" => "Republic of Estonia",
                "continent_id" => "4"
            ],
            [
                "id" => "64",
               
                "code" => "EG",
                "iso3" => "EGY",
                "population" => "82868000",
                "name" => "Egypt",
                "full_name" => "Arab Republic of Egypt",
                "continent_id" => "1"
            ],
            [
                "id" => "65",
               
                "code" => "EH",
                "iso3" => "",
                "population" => "",
                "name" => "Western Sahara",
                "full_name" => "Western Sahara",
                "continent_id" => "1"
            ],
            [
                "id" => "66",
               
                "code" => "ER",
                "iso3" => "ERI",
                "population" => "6086495",
                "name" => "Eritrea",
                "full_name" => "State of Eritrea",
                "continent_id" => "1"
            ],
            [
                "id" => "67",
               
                "code" => "ES",
                "iso3" => "ESP",
                "population" => "46030109",
                "name" => "Spain",
                "full_name" => "Kingdom of Spain",
                "continent_id" => "4"
            ],
            [
                "id" => "68",
               
                "code" => "ET",
                "iso3" => "ETH",
                "population" => "84320987",
                "name" => "Ethiopia",
                "full_name" => "Federal Democratic Republic of Ethiopia",
                "continent_id" => "1"
            ],
            [
                "id" => "69",
               
                "code" => "FI",
                "iso3" => "FIN",
                "population" => "5391700",
                "name" => "Finland",
                "full_name" => "Republic of Finland",
                "continent_id" => "4"
            ],
            [
                "id" => "70",
               
                "code" => "FJ",
                "iso3" => "FJI",
                "population" => "849000",
                "name" => "Fiji",
                "full_name" => "Republic of the Fiji Islands",
                "continent_id" => "6"
            ],
            [
                "id" => "71",
               
                "code" => "FK",
                "iso3" => "FLK",
                "population" => "2841",
                "name" => "Falkland Islands",
                "full_name" => "Falkland Islands (Malvinas)",
                "continent_id" => "7"
            ],
            [
                "id" => "72",
               
                "code" => "FM",
                "iso3" => "FSM",
                "population" => "111000",
                "name" => "Micronesia",
                "full_name" => "Federated States of Micronesia",
                "continent_id" => "6"
            ],
            [
                "id" => "73",
               
                "code" => "FO",
                "iso3" => "FRO",
                "population" => "49267",
                "name" => "Faroe Islands",
                "full_name" => "Faroe Islands",
                "continent_id" => "4"
            ],
            [
                "id" => "74",
               
                "code" => "FR",
                "iso3" => "FRA",
                "population" => "65821885",
                "name" => "France",
                "full_name" => "French Republic",
                "continent_id" => "4"
            ],
            [
                "id" => "75",
               
                "code" => "GA",
                "iso3" => "GAB",
                "population" => "1475000",
                "name" => "Gabon",
                "full_name" => "Gabonese Republic",
                "continent_id" => "1"
            ],
            [
                "id" => "76",
               
                "code" => "GB",
                "iso3" => "",
                "population" => "",
                "name" => "United Kingdom",
                "full_name" => "United Kingdom of Great Britain & Northern Ireland",
                "continent_id" => "4"
            ],
            [
                "id" => "77",
               
                "code" => "GD",
                "iso3" => "GRD",
                "population" => "110000",
                "name" => "Grenada",
                "full_name" => "Grenada",
                "continent_id" => "5"
            ],
            [
                "id" => "78",
               
                "code" => "GE",
                "iso3" => "GEO",
                "population" => "4570934",
                "name" => "Georgia",
                "full_name" => "Georgia",
                "continent_id" => "3"
            ],
            [
                "id" => "79",
               
                "code" => "GF",
                "iso3" => "GUF",
                "population" => "236250",
                "name" => "French Guiana",
                "full_name" => "French Guiana",
                "continent_id" => "7"
            ],
            [
                "id" => "80",
               
                "code" => "GG",
                "iso3" => "GGY",
                "population" => "62300",
                "name" => "Guernsey",
                "full_name" => "Bailiwick of Guernsey",
                "continent_id" => "4"
            ],
            [
                "id" => "81",
               
                "code" => "GH",
                "iso3" => "GHA",
                "population" => "23832495",
                "name" => "Ghana",
                "full_name" => "Republic of Ghana",
                "continent_id" => "1"
            ],
            [
                "id" => "82",
               
                "code" => "GI",
                "iso3" => "GIB",
                "population" => "29200",
                "name" => "Gibraltar",
                "full_name" => "Gibraltar",
                "continent_id" => "4"
            ],
            [
                "id" => "83",
               
                "code" => "GL",
                "iso3" => "GRL",
                "population" => "56370",
                "name" => "Greenland",
                "full_name" => "Greenland",
                "continent_id" => "5"
            ],
            [
                "id" => "84",
               
                "code" => "GM",
                "iso3" => "GMB",
                "population" => "1782893",
                "name" => "Gambia",
                "full_name" => "Republic of the Gambia",
                "continent_id" => "1"
            ],
            [
                "id" => "85",
               
                "code" => "GN",
                "iso3" => "GIN",
                "population" => "10057975",
                "name" => "Guinea",
                "full_name" => "Republic of Guinea",
                "continent_id" => "1"
            ],
            [
                "id" => "86",
               
                "code" => "GP",
                "iso3" => "GPE",
                "population" => "405500",
                "name" => "Guadeloupe",
                "full_name" => "Guadeloupe",
                "continent_id" => "5"
            ],
            [
                "id" => "87",
               
                "code" => "GQ",
                "iso3" => "GNQ",
                "population" => "676000",
                "name" => "Equatorial Guinea",
                "full_name" => "Republic of Equatorial Guinea",
                "continent_id" => "1"
            ],
            [
                "id" => "88",
               
                "code" => "GR",
                "iso3" => "GRC",
                "population" => "10787690",
                "name" => "Greece",
                "full_name" => "Hellenic Republic Greece",
                "continent_id" => "4"
            ],
            [
                "id" => "89",
               
                "code" => "GS",
                "iso3" => "",
                "population" => "",
                "name" => "South Georgia and South Sandwich Islands",
                "full_name" => "South Georgia and the South Sandwich Islands",
                "continent_id" => "2"
            ],
            [
                "id" => "90",
               
                "code" => "GT",
                "iso3" => "GTM",
                "population" => "14027000",
                "name" => "Guatemala",
                "full_name" => "Republic of Guatemala",
                "continent_id" => "5"
            ],
            [
                "id" => "91",
               
                "code" => "GU",
                "iso3" => "GUM",
                "population" => "159358",
                "name" => "Guam",
                "full_name" => "Guam",
                "continent_id" => "6"
            ],
            [
                "id" => "92",
               
                "code" => "GW",
                "iso3" => "GNB",
                "population" => "1647000",
                "name" => "Guinea-Bissau",
                "full_name" => "Republic of Guinea-Bissau",
                "continent_id" => "1"
            ],
            [
                "id" => "93",
               
                "code" => "GY",
                "iso3" => "GUY",
                "population" => "752940",
                "name" => "Guyana",
                "full_name" => "Co-operative Republic of Guyana",
                "continent_id" => "7"
            ],
            [
                "id" => "94",
               
                "code" => "HK",
                "iso3" => "HKG",
                "population" => "7061200",
                "name" => "Hong Kong",
                "full_name" => "Hong Kong Special Administrative Region of China",
                "continent_id" => "3"
            ],
            [
                "id" => "95",
               
                "code" => "HM",
                "iso3" => "",
                "population" => "",
                "name" => "Heard and McDonald Islands",
                "full_name" => "Heard Island and McDonald Islands",
                "continent_id" => "2"
            ],
            [
                "id" => "96",
               
                "code" => "HN",
                "iso3" => "HND",
                "population" => "7466000",
                "name" => "Honduras",
                "full_name" => "Republic of Honduras",
                "continent_id" => "5"
            ],
            [
                "id" => "97",
               
                "code" => "HR",
                "iso3" => "HRV",
                "population" => "4290612",
                "name" => "Croatia",
                "full_name" => "Republic of Croatia",
                "continent_id" => "4"
            ],
            [
                "id" => "98",
               
                "code" => "HT",
                "iso3" => "HTI",
                "population" => "9719932",
                "name" => "Haiti",
                "full_name" => "Republic of Haiti",
                "continent_id" => "5"
            ],
            [
                "id" => "99",
               
                "code" => "HU",
                "iso3" => "HUN",
                "population" => "9979000",
                "name" => "Hungary",
                "full_name" => "Republic of Hungary",
                "continent_id" => "4"
            ],
            [
                "id" => "100",
               
                "code" => "ID",
                "iso3" => "IDN",
                "population" => "241030522",
                "name" => "Indonesia",
                "full_name" => "Republic of Indonesia",
                "continent_id" => "3"
            ],
            [
                "id" => "101",
               
                "code" => "IE",
                "iso3" => "",
                "population" => "",
                "name" => "Ireland",
                "full_name" => "Ireland",
                "continent_id" => "4"
            ],
            [
                "id" => "102",
               
                "code" => "IL",
                "iso3" => "ISR",
                "population" => "7653600",
                "name" => "Israel",
                "full_name" => "State of Israel",
                "continent_id" => "3"
            ],
            [
                "id" => "103",
               
                "code" => "IM",
                "iso3" => "IMN",
                "population" => "82900",
                "name" => "Isle of Man",
                "full_name" => "Isle of Man",
                "continent_id" => "4"
            ],
            [
                "id" => "104",
               
                "code" => "IN",
                "iso3" => "IND",
                "population" => "1210193422",
                "name" => "India",
                "full_name" => "Republic of India",
                "continent_id" => "3"
            ],
            [
                "id" => "105",
               
                "code" => "IO",
                "iso3" => "IOT",
                "population" => "4000",
                "name" => "British Indian Ocean Territory",
                "full_name" => "British Indian Ocean Territory (Chagos Archipelago)",
                "continent_id" => "3"
            ],
            [
                "id" => "106",
               
                "code" => "IQ",
                "iso3" => "IRQ",
                "population" => "31129225",
                "name" => "Iraq",
                "full_name" => "Republic of Iraq",
                "continent_id" => "3"
            ],
            [
                "id" => "107",
               
                "code" => "IR",
                "iso3" => "IRN",
                "population" => "75149669",
                "name" => "Iran",
                "full_name" => "Islamic Republic of Iran",
                "continent_id" => "3"
            ],
            [
                "id" => "108",
               
                "code" => "IS",
                "iso3" => "ISL",
                "population" => "318452",
                "name" => "Iceland",
                "full_name" => "Republic of Iceland",
                "continent_id" => "4"
            ],
            [
                "id" => "109",
               
                "code" => "IT",
                "iso3" => "ITA",
                "population" => "60681514",
                "name" => "Italy",
                "full_name" => "Italian Republic",
                "continent_id" => "4"
            ],
            [
                "id" => "110",
               
                "code" => "JE",
                "iso3" => "JEY",
                "population" => "97900",
                "name" => "Jersey",
                "full_name" => "Bailiwick of Jersey",
                "continent_id" => "4"
            ],
            [
                "id" => "111",
               
                "code" => "JM",
                "iso3" => "JAM",
                "population" => "2719000",
                "name" => "Jamaica",
                "full_name" => "Jamaica",
                "continent_id" => "5"
            ],
            [
                "id" => "112",
               
                "code" => "JO",
                "iso3" => "JOR",
                "population" => "6508271",
                "name" => "Jordan",
                "full_name" => "Hashemite Kingdom of Jordan",
                "continent_id" => "3"
            ],
            [
                "id" => "113",
               
                "code" => "JP",
                "iso3" => "JPN",
                "population" => "127950000",
                "name" => "Japan",
                "full_name" => "Japan",
                "continent_id" => "3"
            ],
            [
                "id" => "114",
               
                "code" => "KE",
                "iso3" => "KEN",
                "population" => "43013341",
                "name" => "Kenya",
                "full_name" => "Republic of Kenya",
                "continent_id" => "1"
            ],
            [
                "id" => "115",
               
                "code" => "KG",
                "iso3" => "KGZ",
                "population" => "5550239",
                "name" => "Kyrgyzstan",
                "full_name" => "Kyrgyz Republic",
                "continent_id" => "3"
            ],
            [
                "id" => "116",
               
                "code" => "KH",
                "iso3" => "KHM",
                "population" => "14952665",
                "name" => "Cambodia",
                "full_name" => "Kingdom of Cambodia",
                "continent_id" => "3"
            ],
            [
                "id" => "117",
               
                "code" => "KI",
                "iso3" => "KIR",
                "population" => "103500",
                "name" => "Kiribati",
                "full_name" => "Republic of Kiribati",
                "continent_id" => "6"
            ],
            [
                "id" => "118",
               
                "code" => "KM",
                "iso3" => "COM",
                "population" => "798000",
                "name" => "Comoros",
                "full_name" => "Union of the Comoros",
                "continent_id" => "1"
            ],
            [
                "id" => "119",
               
                "code" => "KN",
                "iso3" => "KNA",
                "population" => "51300",
                "name" => "Saint Kitts and Nevis",
                "full_name" => "Federation of Saint Kitts and Nevis",
                "continent_id" => "5"
            ],
            [
                "id" => "120",
               
                "code" => "KP",
                "iso3" => "",
                "population" => "",
                "name" => "Korea, North",
                "full_name" => "Democratic People\\\\'s Republic of Korea",
                "continent_id" => "3"
            ],
            [
                "id" => "121",
               
                "code" => "KR",
                "iso3" => "",
                "population" => "",
                "name" => "Korea, South",
                "full_name" => "Republic of Korea",
                "continent_id" => "3"
            ],
            [
                "id" => "122",
               
                "code" => "KW",
                "iso3" => "KWT",
                "population" => "3566437",
                "name" => "Kuwait",
                "full_name" => "State of Kuwait",
                "continent_id" => "3"
            ],
            [
                "id" => "123",
               
                "code" => "KY",
                "iso3" => "CYM",
                "population" => "54878",
                "name" => "Cayman Islands",
                "full_name" => "Cayman Islands",
                "continent_id" => "5"
            ],
            [
                "id" => "124",
               
                "code" => "KZ",
                "iso3" => "KAZ",
                "population" => "16600000",
                "name" => "Kazakhstan",
                "full_name" => "Republic of Kazakhstan",
                "continent_id" => "3"
            ],
            [
                "id" => "125",
               
                "code" => "LA",
                "iso3" => "LAO",
                "population" => "6500000",
                "name" => "Laos",
                "full_name" => "Lao People\\\\'s Democratic Republic",
                "continent_id" => "3"
            ],
            [
                "id" => "126",
               
                "code" => "LB",
                "iso3" => "LBN",
                "population" => "4224000",
                "name" => "Lebanon",
                "full_name" => "Lebanese Republic",
                "continent_id" => "3"
            ],
            [
                "id" => "127",
               
                "code" => "LC",
                "iso3" => "LCA",
                "population" => "173765",
                "name" => "Saint Lucia",
                "full_name" => "Saint Lucia",
                "continent_id" => "5"
            ],
            [
                "id" => "128",
               
                "code" => "LI",
                "iso3" => "LIE",
                "population" => "36281",
                "name" => "Liechtenstein",
                "full_name" => "Principality of Liechtenstein",
                "continent_id" => "4"
            ],
            [
                "id" => "129",
               
                "code" => "LK",
                "iso3" => "LKA",
                "population" => "20277597",
                "name" => "Sri Lanka",
                "full_name" => "Democratic Socialist Republic of Sri Lanka",
                "continent_id" => "3"
            ],
            [
                "id" => "130",
               
                "code" => "LR",
                "iso3" => "LBR",
                "population" => "3786764",
                "name" => "Liberia",
                "full_name" => "Republic of Liberia",
                "continent_id" => "1"
            ],
            [
                "id" => "131",
               
                "code" => "LS",
                "iso3" => "LSO",
                "population" => "2067000",
                "name" => "Lesotho",
                "full_name" => "Kingdom of Lesotho",
                "continent_id" => "1"
            ],
            [
                "id" => "132",
               
                "code" => "LT",
                "iso3" => "LTU",
                "population" => "3207060",
                "name" => "Lithuania",
                "full_name" => "Republic of Lithuania",
                "continent_id" => "4"
            ],
            [
                "id" => "133",
               
                "code" => "LU",
                "iso3" => "LUX",
                "population" => "511840",
                "name" => "Luxembourg",
                "full_name" => "Grand Duchy of Luxembourg",
                "continent_id" => "4"
            ],
            [
                "id" => "134",
               
                "code" => "LV",
                "iso3" => "",
                "population" => "",
                "name" => "Latvia",
                "full_name" => "Republic of Latvia",
                "continent_id" => "4"
            ],
            [
                "id" => "135",
               
                "code" => "LY",
                "iso3" => "LBY",
                "population" => "6310434",
                "name" => "Libya",
                "full_name" => "Libyan Arab Jamahiriya",
                "continent_id" => "1"
            ],
            [
                "id" => "136",
               
                "code" => "MA",
                "iso3" => "MAR",
                "population" => "34859364",
                "name" => "Morocco",
                "full_name" => "Kingdom of Morocco",
                "continent_id" => "1"
            ],
            [
                "id" => "137",
               
                "code" => "MC",
                "iso3" => "MCO",
                "population" => "36371",
                "name" => "Monaco",
                "full_name" => "Principality of Monaco",
                "continent_id" => "4"
            ],
            [
                "id" => "138",
               
                "code" => "MD",
                "iso3" => "MDA",
                "population" => "3559500",
                "name" => "Moldova",
                "full_name" => "Republic of Moldova",
                "continent_id" => "4"
            ],
            [
                "id" => "139",
               
                "code" => "ME",
                "iso3" => "MNE",
                "population" => "625266",
                "name" => "Montenegro",
                "full_name" => "Republic of Montenegro",
                "continent_id" => "4"
            ],
            [
                "id" => "140",
               
                "code" => "MF",
                "iso3" => "",
                "population" => "",
                "name" => "Saint Martin (French part)",
                "full_name" => "Saint Martin",
                "continent_id" => "5"
            ],
            [
                "id" => "141",
               
                "code" => "MG",
                "iso3" => "MDG",
                "population" => "22005222",
                "name" => "Madagascar",
                "full_name" => "Republic of Madagascar",
                "continent_id" => "1"
            ],
            [
                "id" => "142",
               
                "code" => "MH",
                "iso3" => "MHL",
                "population" => "68000",
                "name" => "Marshall Islands",
                "full_name" => "Republic of the Marshall Islands",
                "continent_id" => "6"
            ],
            [
                "id" => "143",
               
                "code" => "MK",
                "iso3" => "MKD",
                "population" => "2082370",
                "name" => "Macedonia",
                "full_name" => "Republic of Macedonia",
                "continent_id" => "4"
            ],
            [
                "id" => "144",
               
                "code" => "ML",
                "iso3" => "MLI",
                "population" => "14517176",
                "name" => "Mali",
                "full_name" => "Republic of Mali",
                "continent_id" => "1"
            ],
            [
                "id" => "145",
               
                "code" => "MM",
                "iso3" => "MMR",
                "population" => "60280000",
                "name" => "Myanmar",
                "full_name" => "Union of Myanmar",
                "continent_id" => "3"
            ],
            [
                "id" => "146",
               
                "code" => "MN",
                "iso3" => "MNG",
                "population" => "2754685",
                "name" => "Mongolia",
                "full_name" => "Mongolia",
                "continent_id" => "3"
            ],
            [
                "id" => "147",
               
                "code" => "MO",
                "iso3" => "MAC",
                "population" => "568700",
                "name" => "Macau",
                "full_name" => "Macao Special Administrative Region of China",
                "continent_id" => "3"
            ],
            [
                "id" => "148",
               
                "code" => "MP",
                "iso3" => "MNP",
                "population" => "53833",
                "name" => "Northern Mariana Islands",
                "full_name" => "Commonwealth of the Northern Mariana Islands",
                "continent_id" => "6"
            ],
            [
                "id" => "149",
               
                "code" => "MQ",
                "iso3" => "MTQ",
                "population" => "403795",
                "name" => "Martinique",
                "full_name" => "Martinique",
                "continent_id" => "5"
            ],
            [
                "id" => "150",
               
                "code" => "MR",
                "iso3" => "MRT",
                "population" => "3359185",
                "name" => "Mauritania",
                "full_name" => "Islamic Republic of Mauritania",
                "continent_id" => "1"
            ],
            [
                "id" => "151",
               
                "code" => "MS",
                "iso3" => "MSR",
                "population" => "5164",
                "name" => "Montserrat",
                "full_name" => "Montserrat",
                "continent_id" => "5"
            ],
            [
                "id" => "152",
               
                "code" => "MT",
                "iso3" => "MLT",
                "population" => "417608",
                "name" => "Malta",
                "full_name" => "Republic of Malta",
                "continent_id" => "4"
            ],
            [
                "id" => "153",
               
                "code" => "MU",
                "iso3" => "MUS",
                "population" => "1291456",
                "name" => "Mauritius",
                "full_name" => "Republic of Mauritius",
                "continent_id" => "1"
            ],
            [
                "id" => "154",
               
                "code" => "MV",
                "iso3" => "MDV",
                "population" => "328536",
                "name" => "Maldives",
                "full_name" => "Republic of Maldives",
                "continent_id" => "3"
            ],
            [
                "id" => "155",
               
                "code" => "MW",
                "iso3" => "MWI",
                "population" => "14901000",
                "name" => "Malawi",
                "full_name" => "Republic of Malawi",
                "continent_id" => "1"
            ],
            [
                "id" => "156",
               
                "code" => "MX",
                "iso3" => "",
                "population" => "",
                "name" => "Mexico",
                "full_name" => "United Mexican States",
                "continent_id" => "5"
            ],
            [
                "id" => "157",
               
                "code" => "MY",
                "iso3" => "MYS",
                "population" => "28731000",
                "name" => "Malaysia",
                "full_name" => "Malaysia",
                "continent_id" => "3"
            ],
            [
                "id" => "158",
               
                "code" => "MZ",
                "iso3" => "MOZ",
                "population" => "22894000",
                "name" => "Mozambique",
                "full_name" => "Republic of Mozambique",
                "continent_id" => "1"
            ],
            [
                "id" => "159",
               
                "code" => "NA",
                "iso3" => "NAM",
                "population" => "2100000",
                "name" => "Namibia",
                "full_name" => "Republic of Namibia",
                "continent_id" => "1"
            ],
            [
                "id" => "160",
               
                "code" => "NC",
                "iso3" => "NCL",
                "population" => "252000",
                "name" => "New Caledonia",
                "full_name" => "New Caledonia",
                "continent_id" => "6"
            ],
            [
                "id" => "161",
               
                "code" => "NE",
                "iso3" => "NER",
                "population" => "16274738",
                "name" => "Niger",
                "full_name" => "Republic of Niger",
                "continent_id" => "1"
            ],
            [
                "id" => "162",
               
                "code" => "NF",
                "iso3" => "NFK",
                "population" => "2302",
                "name" => "Norfolk Island",
                "full_name" => "Norfolk Island",
                "continent_id" => "6"
            ],
            [
                "id" => "163",
               
                "code" => "NG",
                "iso3" => "NGA",
                "population" => "166629000",
                "name" => "Nigeria",
                "full_name" => "Federal Republic of Nigeria",
                "continent_id" => "1"
            ],
            [
                "id" => "164",
               
                "code" => "NI",
                "iso3" => "NIC",
                "population" => "5743000",
                "name" => "Nicaragua",
                "full_name" => "Republic of Nicaragua",
                "continent_id" => "5"
            ],
            [
                "id" => "165",
               
                "code" => "NL",
                "iso3" => "NLD",
                "population" => "16703700",
                "name" => "Netherlands",
                "full_name" => "Kingdom of the Netherlands",
                "continent_id" => "4"
            ],
            [
                "id" => "166",
               
                "code" => "NO",
                "iso3" => "NOR",
                "population" => "4993300",
                "name" => "Norway",
                "full_name" => "Kingdom of Norway",
                "continent_id" => "4"
            ],
            [
                "id" => "167",
               
                "code" => "NP",
                "iso3" => "NPL",
                "population" => "26620080",
                "name" => "Nepal",
                "full_name" => "State of Nepal",
                "continent_id" => "3"
            ],
            [
                "id" => "168",
               
                "code" => "NR",
                "iso3" => "NRU",
                "population" => "9378",
                "name" => "Nauru",
                "full_name" => "Republic of Nauru",
                "continent_id" => "6"
            ],
            [
                "id" => "169",
               
                "code" => "NU",
                "iso3" => "NIU",
                "population" => "1398",
                "name" => "Niue",
                "full_name" => "Niue",
                "continent_id" => "6"
            ],
            [
                "id" => "170",
               
                "code" => "NZ",
                "iso3" => "NZL",
                "population" => "4108037",
                "name" => "New Zealand",
                "full_name" => "New Zealand",
                "continent_id" => "6"
            ],
            [
                "id" => "171",
               
                "code" => "OM",
                "iso3" => "OMN",
                "population" => "2773479",
                "name" => "Oman",
                "full_name" => "Sultanate of Oman",
                "continent_id" => "3"
            ],
            [
                "id" => "172",
               
                "code" => "PA",
                "iso3" => "",
                "population" => "",
                "name" => "Panama",
                "full_name" => "Republic of Panama",
                "continent_id" => "5"
            ],
            [
                "id" => "173",
               
                "code" => "PE",
                "iso3" => "PER",
                "population" => "29546963",
                "name" => "Peru",
                "full_name" => "Republic of Peru",
                "continent_id" => "7"
            ],
            [
                "id" => "174",
               
                "code" => "PF",
                "iso3" => "",
                "population" => "",
                "name" => "French Polynesia",
                "full_name" => "French Polynesia",
                "continent_id" => "6"
            ],
            [
                "id" => "175",
               
                "code" => "PG",
                "iso3" => "PNG",
                "population" => "5172033",
                "name" => "Papua New Guinea",
                "full_name" => "Independent State of Papua New Guinea",
                "continent_id" => "6"
            ],
            [
                "id" => "176",
               
                "code" => "PH",
                "iso3" => "PHL",
                "population" => "95856000",
                "name" => "Philippines",
                "full_name" => "Republic of the Philippines",
                "continent_id" => "3"
            ],
            [
                "id" => "177",
               
                "code" => "PK",
                "iso3" => "PAK",
                "population" => "180440000",
                "name" => "Pakistan",
                "full_name" => "Islamic Republic of Pakistan",
                "continent_id" => "3"
            ],
            [
                "id" => "178",
               
                "code" => "PL",
                "iso3" => "POL",
                "population" => "38186860",
                "name" => "Poland",
                "full_name" => "Republic of Poland",
                "continent_id" => "4"
            ],
            [
                "id" => "179",
               
                "code" => "PM",
                "iso3" => "SPM",
                "population" => "5888",
                "name" => "Saint Pierre and Miquelon",
                "full_name" => "Saint Pierre and Miquelon",
                "continent_id" => "5"
            ],
            [
                "id" => "180",
               
                "code" => "PN",
                "iso3" => "",
                "population" => "",
                "name" => "Pitcairn",
                "full_name" => "Pitcairn Islands",
                "continent_id" => "6"
            ],
            [
                "id" => "181",
               
                "code" => "PR",
                "iso3" => "PUR",
                "population" => "3982000",
                "name" => "Puerto Rico",
                "full_name" => "Commonwealth of Puerto Rico",
                "continent_id" => "5"
            ],
            [
                "id" => "182",
               
                "code" => "PS",
                "iso3" => "PSE",
                "population" => "4260636",
                "name" => "Palestine",
                "full_name" => "Occupied Palestinian Territory",
                "continent_id" => "3"
            ],
            [
                "id" => "183",
               
                "code" => "PT",
                "iso3" => "PRT",
                "population" => "10647763",
                "name" => "Portugal",
                "full_name" => "Portuguese Republic",
                "continent_id" => "4"
            ],
            [
                "id" => "184",
               
                "code" => "PW",
                "iso3" => "PLW",
                "population" => "20956",
                "name" => "Palau",
                "full_name" => "Republic of Palau",
                "continent_id" => "6"
            ],
            [
                "id" => "185",
               
                "code" => "PY",
                "iso3" => "PRY",
                "population" => "6375830",
                "name" => "Paraguay",
                "full_name" => "Republic of Paraguay",
                "continent_id" => "7"
            ],
            [
                "id" => "186",
               
                "code" => "QA",
                "iso3" => "QAT",
                "population" => "1853563",
                "name" => "Qatar",
                "full_name" => "State of Qatar",
                "continent_id" => "3"
            ],
            [
                "id" => "187",
               
                "code" => "RE",
                "iso3" => "",
                "population" => "",
                "name" => "Reunion",
                "full_name" => "Reunion",
                "continent_id" => "1"
            ],
            [
                "id" => "188",
               
                "code" => "RO",
                "iso3" => "ROU",
                "population" => "19042936",
                "name" => "Romania",
                "full_name" => "Romania",
                "continent_id" => "4"
            ],
            [
                "id" => "189",
               
                "code" => "RS",
                "iso3" => "SRB",
                "population" => "7276604",
                "name" => "Serbia",
                "full_name" => "Republic of Serbia",
                "continent_id" => "4"
            ],
            [
                "id" => "190",
               
                "code" => "RU",
                "iso3" => "",
                "population" => "",
                "name" => "Russian Federation",
                "full_name" => "Russian Federation",
                "continent_id" => "4"
            ],
            [
                "id" => "191",
               
                "code" => "RW",
                "iso3" => "RWA",
                "population" => "11689696",
                "name" => "Rwanda",
                "full_name" => "Republic of Rwanda",
                "continent_id" => "1"
            ],
            [
                "id" => "192",
               
                "code" => "SA",
                "iso3" => "SAU",
                "population" => "27136977",
                "name" => "Saudi Arabia",
                "full_name" => "Kingdom of Saudi Arabia",
                "continent_id" => "3"
            ],
            [
                "id" => "193",
               
                "code" => "SB",
                "iso3" => "SLB",
                "population" => "523000",
                "name" => "Solomon Islands",
                "full_name" => "Solomon Islands",
                "continent_id" => "6"
            ],
            [
                "id" => "194",
               
                "code" => "SC",
                "iso3" => "SYC",
                "population" => "84000",
                "name" => "Seychelles",
                "full_name" => "Republic of Seychelles",
                "continent_id" => "1"
            ],
            [
                "id" => "195",
               
                "code" => "SD",
                "iso3" => "SDN",
                "population" => "30894000",
                "name" => "Sudan",
                "full_name" => "Republic of Sudan",
                "continent_id" => "1"
            ],
            [
                "id" => "196",
               
                "code" => "SE",
                "iso3" => "SWE",
                "population" => "9415570",
                "name" => "Sweden",
                "full_name" => "Kingdom of Sweden",
                "continent_id" => "4"
            ],
            [
                "id" => "197",
               
                "code" => "SG",
                "iso3" => "SGB",
                "population" => "5274700",
                "name" => "Singapore",
                "full_name" => "Republic of Singapore",
                "continent_id" => "3"
            ],
            [
                "id" => "198",
               
                "code" => "SH",
                "iso3" => "SHN",
                "population" => "7728",
                "name" => "Saint Helena",
                "full_name" => "Saint Helena",
                "continent_id" => "1"
            ],
            [
                "id" => "199",
               
                "code" => "SI",
                "iso3" => "SVN",
                "population" => "2048951",
                "name" => "Slovenia",
                "full_name" => "Republic of Slovenia",
                "continent_id" => "4"
            ],
            [
                "id" => "200",
               
                "code" => "SJ",
                "iso3" => "",
                "population" => "",
                "name" => "Svalbard and Jan Mayen Islands",
                "full_name" => "Svalbard & Jan Mayen Islands",
                "continent_id" => "4"
            ],
            [
                "id" => "201",
               
                "code" => "SK",
                "iso3" => "SVK",
                "population" => "5440078",
                "name" => "Slovakia",
                "full_name" => "Slovakia (Slovak Republic)",
                "continent_id" => "4"
            ],
            [
                "id" => "202",
               
                "code" => "SL",
                "iso3" => "SLE",
                "population" => "5485998",
                "name" => "Sierra Leone",
                "full_name" => "Republic of Sierra Leone",
                "continent_id" => "1"
            ],
            [
                "id" => "203",
               
                "code" => "SM",
                "iso3" => "SMR",
                "population" => "32404",
                "name" => "San Marino",
                "full_name" => "Republic of San Marino",
                "continent_id" => "4"
            ],
            [
                "id" => "204",
               
                "code" => "SN",
                "iso3" => "SEN",
                "population" => "12855153",
                "name" => "Senegal",
                "full_name" => "Republic of Senegal",
                "continent_id" => "1"
            ],
            [
                "id" => "205",
               
                "code" => "SO",
                "iso3" => "SOM",
                "population" => "10085638",
                "name" => "Somalia",
                "full_name" => "Somali Republic",
                "continent_id" => "1"
            ],
            [
                "id" => "206",
               
                "code" => "SR",
                "iso3" => "SUR",
                "population" => "492829",
                "name" => "Suriname",
                "full_name" => "Republic of Suriname",
                "continent_id" => "7"
            ],
            [
                "id" => "207",
               
                "code" => "ST",
                "iso3" => "",
                "population" => "",
                "name" => "Sao Tome and Principe",
                "full_name" => "Democratic Republic of Sao Tome and Principe",
                "continent_id" => "1"
            ],
            [
                "id" => "208",
               
                "code" => "SV",
                "iso3" => "SLV",
                "population" => "6163000",
                "name" => "El Salvador",
                "full_name" => "Republic of El Salvador",
                "continent_id" => "5"
            ],
            [
                "id" => "209",
               
                "code" => "SY",
                "iso3" => "SYR",
                "population" => "22530746",
                "name" => "Syria",
                "full_name" => "Syrian Arab Republic",
                "continent_id" => "3"
            ],
            [
                "id" => "210",
               
                "code" => "SZ",
                "iso3" => "SWZ",
                "population" => "1185000",
                "name" => "Swaziland",
                "full_name" => "Kingdom of Swaziland",
                "continent_id" => "1"
            ],
            [
                "id" => "211",
               
                "code" => "TC",
                "iso3" => "TCA",
                "population" => "46400",
                "name" => "Turks and Caicos Islands",
                "full_name" => "Turks and Caicos Islands",
                "continent_id" => "5"
            ],
            [
                "id" => "212",
               
                "code" => "TD",
                "iso3" => "TCD",
                "population" => "10329208",
                "name" => "Chad",
                "full_name" => "Republic of Chad",
                "continent_id" => "1"
            ],
            [
                "id" => "213",
               
                "code" => "TF",
                "iso3" => "",
                "population" => "",
                "name" => "French Southern Lands",
                "full_name" => "French Southern Territories",
                "continent_id" => "2"
            ],
            [
                "id" => "214",
               
                "code" => "TG",
                "iso3" => "TGO",
                "population" => "6619000",
                "name" => "Togo",
                "full_name" => "Togolese Republic",
                "continent_id" => "1"
            ],
            [
                "id" => "215",
               
                "code" => "TH",
                "iso3" => "THA",
                "population" => "64076000",
                "name" => "Thailand",
                "full_name" => "Kingdom of Thailand",
                "continent_id" => "3"
            ],
            [
                "id" => "216",
               
                "code" => "TJ",
                "iso3" => "TJK",
                "population" => "7616000",
                "name" => "Tajikistan",
                "full_name" => "Republic of Tajikistan",
                "continent_id" => "3"
            ],
            [
                "id" => "217",
               
                "code" => "TK",
                "iso3" => "TKL",
                "population" => "1411",
                "name" => "Tokelau",
                "full_name" => "Tokelau",
                "continent_id" => "6"
            ],
            [
                "id" => "218",
               
                "code" => "TL",
                "iso3" => "",
                "population" => "",
                "name" => "Timor-Leste",
                "full_name" => "Democratic Republic of Timor-Leste",
                "continent_id" => "3"
            ],
            [
                "id" => "219",
               
                "code" => "TM",
                "iso3" => "TKM",
                "population" => "5125693",
                "name" => "Turkmenistan",
                "full_name" => "Turkmenistan",
                "continent_id" => "3"
            ],
            [
                "id" => "220",
               
                "code" => "TN",
                "iso3" => "TUN",
                "population" => "10486339",
                "name" => "Tunisia",
                "full_name" => "Tunisian Republic",
                "continent_id" => "1"
            ],
            [
                "id" => "221",
               
                "code" => "TO",
                "iso3" => "TON",
                "population" => "103036",
                "name" => "Tonga",
                "full_name" => "Kingdom of Tonga",
                "continent_id" => "6"
            ],
            [
                "id" => "222",
               
                "code" => "TR",
                "iso3" => "TUR",
                "population" => "79749461",
                "name" => "Turkey",
                "full_name" => "Republic of Turkey",
                "continent_id" => "3"
            ],
            [
                "id" => "223",
               
                "code" => "TT",
                "iso3" => "TTO",
                "population" => "1346350",
                "name" => "Trinidad and Tobago",
                "full_name" => "Republic of Trinidad and Tobago",
                "continent_id" => "5"
            ],
            [
                "id" => "224",
               
                "code" => "TV",
                "iso3" => "TUV",
                "population" => "10544",
                "name" => "Tuvalu",
                "full_name" => "Tuvalu",
                "continent_id" => "6"
            ],
            [
                "id" => "225",
               
                "code" => "TW",
                "iso3" => "TWN",
                "population" => "23174528",
                "name" => "Taiwan",
                "full_name" => "Taiwan",
                "continent_id" => "3"
            ],
            [
                "id" => "226",
               
                "code" => "TZ",
                "iso3" => "TZA",
                "population" => "43188000",
                "name" => "Tanzania",
                "full_name" => "United Republic of Tanzania",
                "continent_id" => "1"
            ],
            [
                "id" => "227",
               
                "code" => "UA",
                "iso3" => "UKR",
                "population" => "44854065",
                "name" => "Ukraine",
                "full_name" => "Ukraine",
                "continent_id" => "4"
            ],
            [
                "id" => "228",
               
                "code" => "UG",
                "iso3" => "UGA",
                "population" => "35873253",
                "name" => "Uganda",
                "full_name" => "Republic of Uganda",
                "continent_id" => "1"
            ],
            [
                "id" => "229",
               
                "code" => "UM",
                "iso3" => "",
                "population" => "",
                "name" => "United States Minor Outlying Islands",
                "full_name" => "United States Minor Outlying Islands",
                "continent_id" => "6"
            ],
            [
                "id" => "230",
               
                "code" => "US",
                "iso3" => "",
                "population" => "",
                "name" => "United States of America",
                "full_name" => "United States of America",
                "continent_id" => "5"
            ],
            [
                "id" => "231",
               
                "code" => "UY",
                "iso3" => "URY",
                "population" => "3510386",
                "name" => "Uruguay",
                "full_name" => "Eastern Republic of Uruguay",
                "continent_id" => "7"
            ],
            [
                "id" => "232",
               
                "code" => "UZ",
                "iso3" => "UZB",
                "population" => "29559100",
                "name" => "Uzbekistan",
                "full_name" => "Republic of Uzbekistan",
                "continent_id" => "3"
            ],
            [
                "id" => "233",
               
                "code" => "VA",
                "iso3" => "VAT",
                "population" => "836",
                "name" => "Vatican City",
                "full_name" => "Holy See (Vatican City State)",
                "continent_id" => "4"
            ],
            [
                "id" => "234",
               
                "code" => "VC",
                "iso3" => "VCT",
                "population" => "120000",
                "name" => "Saint Vincent and the Grenadines",
                "full_name" => "Saint Vincent and the Grenadines",
                "continent_id" => "5"
            ],
            [
                "id" => "235",
               
                "code" => "VE",
                "iso3" => "VEN",
                "population" => "28833845",
                "name" => "Venezuela",
                "full_name" => "Bolivarian Republic of Venezuela",
                "continent_id" => "7"
            ],
            [
                "id" => "236",
               
                "code" => "VG",
                "iso3" => "",
                "population" => "",
                "name" => "Virgin Islands, British",
                "full_name" => "British Virgin Islands",
                "continent_id" => "5"
            ],
            [
                "id" => "237",
               
                "code" => "VI",
                "iso3" => "",
                "population" => "",
                "name" => "Virgin Islands, U.S.",
                "full_name" => "United States Virgin Islands",
                "continent_id" => "5"
            ],
            [
                "id" => "238",
               
                "code" => "VN",
                "iso3" => "VNM",
                "population" => "89316000",
                "name" => "Vietnam",
                "full_name" => "Socialist Republic of Vietnam",
                "continent_id" => "3"
            ],
            [
                "id" => "239",
               
                "code" => "VU",
                "iso3" => "VUT",
                "population" => "224564",
                "name" => "Vanuatu",
                "full_name" => "Republic of Vanuatu",
                "continent_id" => "6"
            ],
            [
                "id" => "240",
               
                "code" => "WF",
                "iso3" => "",
                "population" => "",
                "name" => "Wallis and Futuna Islands",
                "full_name" => "Wallis and Futuna",
                "continent_id" => "6"
            ],
            [
                "id" => "241",
               
                "code" => "WS",
                "iso3" => "WSM",
                "population" => "194320",
                "name" => "Samoa",
                "full_name" => "Independent State of Samoa",
                "continent_id" => "6"
            ],
            [
                "id" => "242",
               
                "code" => "YE",
                "iso3" => "YEM",
                "population" => "25130000",
                "name" => "Yemen",
                "full_name" => "Yemen",
                "continent_id" => "3"
            ],
            [
                "id" => "243",
               
                "code" => "YT",
                "iso3" => "MYT",
                "population" => "194000",
                "name" => "Mayotte",
                "full_name" => "Mayotte",
                "continent_id" => "1"
            ],
            [
                "id" => "244",
               
                "code" => "ZA",
                "iso3" => "ZAF",
                "population" => "51770560",
                "name" => "South Africa",
                "full_name" => "Republic of South Africa",
                "continent_id" => "1"
            ],
            [
                "id" => "245",
               
                "code" => "ZM",
                "iso3" => "ZMB",
                "population" => "14309466",
                "name" => "Zambia",
                "full_name" => "Republic of Zambia",
                "continent_id" => "1"
            ],
            [
                "id" => "246",
               
                "code" => "ZW",
                "iso3" => "ZWE",
                "population" => "12619600",
                "name" => "Zimbabwe",
                "full_name" => "Republic of Zimbabwe",
                "continent_id" => "1"
            ],
            [
                "id" => "247",
               
                "code" => "Sco",
                "iso3" => "GB-SCT",
                "population" => "5,373,000",
                "name" => "Scotland",
                "full_name" => "Great Britain United Kingdom",
                "continent_id" => "1"
            ],
            
        ];


        foreach($entries as $entry){
            DB::table('countries')->insert($entry);
        }
    }
}