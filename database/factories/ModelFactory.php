<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'username' => $faker->username,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('123456'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Club::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'description' => $faker->sentence,
        'founded' => 2017,
        'website' => $faker->sentence,
        'slug' => str_slug($faker->name),
    ];
});

$factory->define(App\Models\Post::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'content' => $faker->sentence,
        'title' => $faker->title,
        'slug' => str_slug($faker->title),
        'model' => get_class(App\Models\Club::first()),
        'model_id' => App\Models\Club::first()->id,
        'user_id' => App\Models\User::first()->id
    ];
});
