<?php
Route::get('/mbo-value', ['uses' => 'HomesController@value', 'as' => 'app.home.value']);
Route::get('/membership-directory', ['uses' => 'HomesController@directory', 'as' => 'app.home.directory']);
Route::get('/faq', ['uses' => 'HomesController@faq', 'as' => 'app.home.faq']);
Route::get('/list-members', ['uses' => 'HomesController@list_members', 'as' => 'app.home.member']);

// Calendar
Route::get('calendar', ['uses' => 'CalendarsController@index', 'as' => 'app.calendar.index']);
Route::get('calendar/show', ['uses' => 'CalendarsController@show', 'as' => 'app.calendar.show']);
Route::post('calendar/store', ['uses' => 'CalendarsController@store', 'as' => 'app.calendar.store']);

// Settings
Route::get('/settings/{user}', ['uses' => 'SettingsController@show', 'as' => 'app.setting.index']);
Route::put('/settings/{user}', ['uses' => 'SettingsController@update', 'as' => 'app.setting.update']);

// Media
Route::resource('/media', 'AttachmentsController', ['as' => 'app']);
Route::resource('/member', 'MembersController', ['as' => 'app']);

// Events
Route::resource('event', 'EventsController', ['as' => 'app']);
Route::post('/attend/{event}', ['uses' => 'EventsController@attend', 'as' => 'app.event.attend']);
Route::get('/attend/change/{event}', ['uses' => 'EventsController@change', 'as' => 'app.event.change']);
Route::get('/attend/{event}/all-members', ['uses' => 'EventsController@allMembers', 'as' => 'app.event.all_members']);

// Clubs
Route::resource('club', 'ClubsController', ['as' => 'app']);
Route::get('/join/club', ['uses' => 'ClubsController@join', 'as' => 'app.club.join']);

// Club Posts
Route::resource('club.post', 'ClubsPostsController', ['as' => 'app']);

// Post Comments
Route::resource('post.comment', 'PostsCommentsController', ['as' => 'app']);

// Market Place
Route::resource('market', 'MarketPlacesController', ['as' => 'app']);
Route::resource('market.comment', 'MarketsCommentsController', ['as' => 'app']);

// User
Route::resource('user', 'UsersController', ['as' => 'app']);

// Gallery
Route::get('/category/{category}/year/{year}', ['uses' => 'GalleriesController@filterByYear', 'as' => 'app.gallery.filter']);
Route::resource('gallery', 'GalleriesController', ['as' => 'app']);

// Event Categories
Route::resource('category', 'EventCategoriesController', ['as' => 'app']);
Route::get('/category/{category}', ['uses' => 'EventCategoriesController@index', 'as' => 'app.category.index']);

// User Group (should be at end)
Route::group(['prefix' => '{user}'], function(){
	Route::get('/', ['uses' => 'UsersController@show', 'as' => 'app.user.show']);
	Route::post('/avatar', ['uses' => 'UsersController@uploadAvatar', 'as' => 'app.user.avatar']);
});
