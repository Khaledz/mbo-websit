<?php

// Auth
Auth::routes();

// Guest
Route::group(['namespace' => 'App'], function(){
	require(__DIR__ . '/app/guest.php');
});

// Auth access
Route::group(['middleware' => ['role:admin|champion|membership|board admin','auth'], 'namespace' => 'App'], function(){
	require(__DIR__ . '/app/auth.php');
});

// Admin
Route::group(['middleware' => ['role:admin|board admin', 'auth'], 'namespace' => 'Admin', 'prefix' => 'admin'], function(){
	require(__DIR__ . '/admin/auth.php');
});