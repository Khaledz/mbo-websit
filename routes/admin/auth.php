<?php

Route::get('/home', ['uses' => 'HomesController@index', 'as' => 'admin.home.index']);
Route::resource('/user', 'UsersController', ['as' => 'admin']);
Route::get('user/filter/{role?}', ['uses' => 'UsersController@filter', 'as' => 'admin.user.filter']);
Route::resource('/role', 'RolesController', ['as' => 'admin']);
Route::resource('/club', 'ClubsController', ['as' => 'admin']);
Route::resource('/event', 'EventsController', ['as' => 'admin']);
Route::resource('/setting', 'SettingsController', ['as' => 'admin']);
Route::resource('/market', 'MarketPlacesController', ['as' => 'admin']);
Route::resource('/category', 'EventCategoriesController', ['as' => 'admin']);
Route::resource('/announcement', 'AnnouncementsController', ['as' => 'admin']);
Route::resource('/slider', 'SlidersController', ['as' => 'admin']);
Route::resource('/renwal', 'RenwalsController', ['as' => 'admin']);
Route::resource('/board', 'BoardsController', ['as' => 'admin']);
Route::resource('/gallery', 'GalleriesController', ['as' => 'admin']);
Route::resource('/activity', 'ActivitiesController', ['as' => 'admin']);
// Route::resource('/setting', 'SettingsController', ['as' => 'admin']);
Route::resource('/info', 'SettingsController', ['as' => 'admin']);
Route::resource('/faq', 'FaqsController', ['as' => 'admin']);
Route::resource('/post', 'PostsController', ['as' => 'admin']);
Route::resource('/comment', 'CommentsController', ['as' => 'admin']);

Route::get('/attachment/{model}/{attachment}', ['uses' => 'AttachmentsController@destroy', 'as' => 'admin.attachment.destroy']);

Route::get('club/{club}/pending', ['uses' => 'ClubsController@getPending', 'as' => 'admin.club.pending']);
Route::post('club/{club}/pending', ['uses' => 'ClubsController@postPending', 'as' => 'admin.club.pending']);
